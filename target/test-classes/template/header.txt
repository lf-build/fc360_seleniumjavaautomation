<html ng-app="reportingApp">
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<title>I/O Testing Report</title>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css">
<style>
    /* Move down content because we have a fixed navbar that is 50px tall */
    body {
        padding-top: 30px;
    }
    .main {
        padding: 20px;
    }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>

<!-- Dialog Lib-->
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular-sanitize.min.js"></script>
<script src="https://rawgit.com/m-e-conroy/angular-dialog-service/v4.2.0/dialogs.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bower-angular-translate/2.0.1/angular-translate.min.js"></script>

<script src="http://cmaurer.github.io/angularjs-nvd3-directives/lib/d3/d3.min.js"></script>
<script src="http://cmaurer.github.io/angularjs-nvd3-directives/lib/nvd3/nv.d3.min.js"></script>
<link rel="stylesheet" href="http://cmaurer.github.io/angularjs-nvd3-directives/lib/nvd3/nv.d3.css">
<script src="http://cmaurer.github.io/angularjs-nvd3-directives/lib/angularjs-nvd3-directives/dist/angularjs-nvd3-directives.js"></script>
<script src="http://cmaurer.github.io/angularjs-nvd3-directives/javascripts/scale.fix.js"></script>
<script>
    var reportingApp = angular.module("reportingApp", ['ui.bootstrap', 'nvd3ChartDirectives','dialogs.main']); 
	
	reportingApp.config(function(dialogsProvider){
		dialogsProvider.useBackdrop(true);
		dialogsProvider.useEscClose(true);
		dialogsProvider.useCopy(false);
		dialogsProvider.setSize('lg');
	  });

	
	reportingApp.run(['$templateCache',function($templateCache){
		$templateCache.put('/dialogs/image.html','<style>.modal-dialog{width:1000px} .modal{overflow:hidden}</style><div style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 14px;line-height: 1.42857143;color: #333333;"> <div class="modal-header dialog-header-confirm"> <h4 class="modal-title"> <span class="text-left">Screenshot</span> </h4> </div> <div class="modal-body clearfix"> <img data-ng-src="{{imageURI}}" style="width:100%"/> </div>  <div class="modal-footer"> <button type="button" class="btn btn-default" data-ng-click="close()">Close</button> </div></div>');
	}]);

	reportingApp.controller('imageDialogController',
	['$scope','$modalInstance','data',function($scope,$modalInstance,data){
		if(data!=undefined && data.imageURI!=undefined){
			$scope.imageURI = data.imageURI;
		}
		
		$scope.close = function() {		
			$modalInstance.close();
		};
	}]);

    reportingApp.controller("ReportCtrl", ['$scope','dialogs', function ($scope,dialogs) {
         $scope.report = {
            project: {name: "Portal_Automation"},
            environment:  ENVT_DETAILS,
            signOffSuccessThreshold: 80,
            signOffCommentsNegative: "Build Failed. Either build is unstable or more than expected test cases failed.",
            signOffCommentsPositive: "Build Successful.",
            