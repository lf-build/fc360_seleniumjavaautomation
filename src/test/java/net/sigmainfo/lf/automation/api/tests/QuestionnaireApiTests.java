package net.sigmainfo.lf.automation.api.tests;

import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static net.sigmainfo.lf.automation.api.constant.ApiParam.executeTestcase;
import static org.testng.Assert.assertEquals;

/**
 * Created by shaishav.s on 08-03-2017.
 */
public class QuestionnaireApiTests  extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(QuestionnaireApiTests.class);

    @Autowired
    TestResults testResults;

    @Autowired
    ApiFuncUtils apiFuncUtils;

    @Autowired
    ApiParam apiParam;

    public static String funcMod="ProjectTest_API";

    @AfterClass(alwaysRun = true)
    private void endCasereport() throws IOException, JSONException {

        String funcModule = "ProjectTest_API";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  " + org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcMod);
    }

    /**
     * Description          : coborrowerSectionQuestionTest
     * Includes             : 1. Questionnaire flow  :
     *                          a. Create questionnaire
     *                          b. Answer the question in coborrowerSection
     *                          c. Verify the questionnaire in mongodb that answer is updated
     */


    @Test(priority=1,description = "", groups = {"docittapitests","questionnaireApiTests","coborrowerSectionQuestionTest"})
    public void VerifyCoborrowerSectionQuestion() throws Exception{
        String sTestID = "coborrowerSectionQuestionTest";
        String result = "Failed";
        String formId=null;
        String tempAppId=null;
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        if(executeTestcase) {
            logger.info("\n*****Execution of testcase: " + sTestID + "  starts. *****\n");
            try {

                result = "Passed";
            } finally {
                logger.info("\n*****Execution of testcase: " + sTestID + "  ends. *****\n");
                testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
                writeToReport(funcMod,sTestID, result);
            }
        }
    }
}
