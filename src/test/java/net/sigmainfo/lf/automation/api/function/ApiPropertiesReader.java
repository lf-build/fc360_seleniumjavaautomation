package net.sigmainfo.lf.automation.api.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiPropertiesReader.java
 * Description          : Reads api.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class ApiPropertiesReader {

    @Value(value = "${baseresturl}")
    private String baseresturl;

    @Value(value = "${custom_report_location}")
    private String custom_report_location;

    public String getBaseresturl() {
        return baseresturl;
    }

    public void setBaseresturl(String baseresturl) {
        this.baseresturl = baseresturl;
    }

    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }
}
