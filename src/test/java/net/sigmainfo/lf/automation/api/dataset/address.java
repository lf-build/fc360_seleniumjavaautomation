package net.sigmainfo.lf.automation.api.dataset;

/**
 * Created by           : Shaishav.s on 10-04-2017.
 * Test class           : address.java
 * Description          : Contains members making a complete address class within automation
 * Includes             : 1. Declares members within the class
 *                        2. Contains getter and setter methods
 */
import org.springframework.stereotype.Component;

/**
 *  DATABSETS TO BE DECLARED ON DEMAND BASIS WHICH SHOULD MATCH ACTUAL JSON STRUCTURE
 */

@Component
public class address {
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    private String port;


}
