package net.sigmainfo.lf.automation.portal.pages.Borrower;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 06-10-2017.
 */
public class BorrowerHomePage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerHomePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerHomePage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        //assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_businessNameTextBox)));
        //assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_newApplicationLabel)),"Page not loaded properly.");
        //assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.homepage_newApplicationLabel)).isDisplayed(),"Page not loaded properly.");
        logger.info("=========== BorrowerHomePage is loaded============");
    }
    public BorrowerHomePage(){}

    public void enterBasicDetails(WebDriver driver) throws Exception {
    	Thread.sleep(10000);
    	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_businessNameTextBox));        
    	PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_businessNameTextBox),PortalParam.businessName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_amountSeekingTextBox),PortalParam.amountSeeking);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_useOfFundsDropdown), PortalParam.useOfFunds);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_annualBusinessRevenueTextBox),PortalParam.annualBusinessRevenue);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_firstNameTextBox),PortalParam.firstName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_lastNameTextBox), PortalParam.lastName); //StringEncrypter.createNewEncrypter().decrypt(
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_phoneTextBox), PortalParam.phone);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_emailTextBox), PortalParam.username);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_passwordTextBox), PortalParam.password);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_confirmPasswordTextBox), PortalParam.password);
        }
    public void clickBasicContinue(WebDriver driver) throws Exception {
        try {
            PortalFuncUtils.scrollToElementandClick(driver, PortalFuncUtils.getLocator(UIObjParam.homepage_basicTab_continueButton),"Continue");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void enterYourBusinessDetails(WebDriver driver) throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//li[contains(@class,'profile dropdown')]//div[@class='detail']//strong[contains(text(),'"+PortalParam.firstName+"')]")));
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_businessLabel)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_dbaTextBox)));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_dbaTextBox),PortalParam.dba);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_businessAddressTextBox),PortalParam.businessAddress);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_cityTextBox),PortalParam.city);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_stateDropdown),PortalParam.state);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_zipCodeTextBox), PortalParam.zipCode);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_businessPhoneTextBox), PortalParam.businessPhone);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_bstMonthDropdown),PortalParam.bsMonth);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_bstDayDropdown),PortalParam.bsDay);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_bstYearDropdown),PortalParam.bsYear);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_businessWebsiteTextBox),PortalParam.website);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_legalEntityDropdown),PortalParam.legalEntity);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_businessLocationDropdown),PortalParam.location);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_federalSalesTaxTextBox), PortalParam.taxId);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_industryDropdown),PortalParam.industry);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_importanceDropdown),PortalParam.importance);

    }

    public void clickYourBusinessContinue(WebDriver driver) throws Exception {
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_yourBusinessTab_continueButton),"Continue");
    }

   
    
    public BorrowerDashboardPage submitApplication(WebDriver driver, String bank) throws Exception {
            PortalFuncUtils.waitForPageToLoad(driver);         
            try {
                enterBasicDetails(driver);
            }
            catch (Exception e){}
            try {
                clickBasicContinue(driver);
            }catch (Exception e){}
            try {
                enterYourBusinessDetails(driver);
            }catch (Exception e){}
            try {
                clickYourBusinessContinue(driver);
            }catch (Exception e){}
            try {
                enterAboutYouDetails(driver);
            }catch (Exception e){}
            try {
                clickAboutYouContinue(driver);
            }catch (Exception e){}
            try {
                enterFinancialReviewDetails(driver,bank);
            }catch (Exception e){}
            return new BorrowerDashboardPage(driver);
    }

    private void clickFinish(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_finishButton),"Finish");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_reviewDocusignDocLabel));
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_agreementCheckBox),"Agreement checkbox");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_continueButton),"Continue");
    }

    private void enterFinancialReviewDetails(WebDriver driver, String bank) throws Exception {
        PortalFuncUtils.scrollOnTopOfThePage(driver);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_selectOptionsLabel)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_linkBankAccountButton),"Link Bank Account");
        connectBank(driver,bank);
       /* PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_finishButton),"Finish");
        new WebDriverWait(driver,60).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[contains(@src,'demo.docusign.net')]")));
        System.out.println(driver.findElements(By.tagName("iframe")).size());
        driver.switchTo().frame(0);
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_agreementCheckBox));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_agreementCheckBox),"Agreement checkbox");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_continueButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_continueButton),"Continue");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_okButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_okButton),"OK");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_startDocusignButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_startDocusignButton),"Start");
        Thread.sleep(4000);
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_signHereButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_signHereButton),"Sign here");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_adoptSignButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_adoptSignButton),"Adopt and Sign");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_finishButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_docusign_finishButton),"Finish");
        driver.switchTo().defaultContent();*/
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_completeApplicationButton),"Complete Application");

    }

    public void connectBank(WebDriver driver, String bank) throws Exception {
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//iframe[contains(@src,'"+PortalParam.iframeSource+"')]")),"Plaid container is not visible");
            driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'"+PortalParam.iframeSource+"')]")));
           
            if(bank.equalsIgnoreCase("no"))            	
            {
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_searchBankTextBox), PortalParam.bankToConnect);
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_resultantBank), PortalParam.bankToConnect);
            }
            else
            {
            	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_searchBankTextBox), "Fidelity");
                 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_resultantBank), "Fidelity");               
            }
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.selectBank_submitPlaidButton)),"Submit button not visible.");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_plaidUsernameTextBox), portalParam.plaid_user);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_plaidPasswordTextBox), portalParam.plaid_password);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_submitPlaidButton), "Submit");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.selectBank_successfulConnectionLabel)),"Select account label not seen");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.selectBank_continueButton), "Continue");
            driver.switchTo().defaultContent();
            PortalFuncUtils.scrollOnTopOfThePage(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel)),"Success message not displayed after bank linking");
           // PortalFuncUtils.clickButton(driver,By.xpath("//div[@class='link-bank-action']//span[contains(text(),'Authorization to Apply for Business Funding')]/preceding-sibling::input[@type='checkbox']"),"Authorization checkbox");
           // PortalFuncUtils.clickButton(driver,By.xpath("//div[@class='link-bank-action']//span[contains(text(),'Agreement to Conduct Business Electronically')]/preceding-sibling::input[@type='checkbox']"),"Agreement checkbox");
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_financialReviewTab_completeApplicationButton),"Complete Application");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }

    private void clickAboutYouContinue(WebDriver driver) throws Exception {
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_continueButton),"Continue");
    }

    public void enterAboutYouDetails(WebDriver driver) throws Exception {
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_getToKnowLabel)));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_emailAddressTextBox),PortalParam.username);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_firstNameTextBox),PortalParam.firstName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_lastNameTextBox),PortalParam.lastName);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_addressTextBox),PortalParam.homeAddress);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_homeCityTextBox),PortalParam.homeCity);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_homeStateDropdown),PortalParam.homeState);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_zipCodeTextBox),PortalParam.homeZipcode);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_phoneNumberTextBox),PortalParam.homePhone);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_mobileNumberTextBox),PortalParam.homeMobile);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_ownershipTextBox),PortalParam.ownership);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_ssnTextBox),PortalParam.ssn);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_mobDropdown),PortalParam.doMonth);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_dobDropdown),PortalParam.doDay);
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_yobDropdown),PortalParam.doYear);
        PortalFuncUtils.selectCheckbox(driver,PortalFuncUtils.getLocator(UIObjParam.homepage_aboutYouTab_AcceptTC));
    }

    public BorrowerHomePage logout(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,By.xpath("//li[@class='profile dropdown ']/a/div//strong/strong"),"Profile");
        PortalFuncUtils.clickButton(driver,By.xpath("//a[@id='open_93907719']"),"Log out");
        return new BorrowerHomePage(driver);
    }

    public BorrowerLoginPage clickSignInButton() throws Exception {
        PortalFuncUtils.clickButton(driver,By.xpath("//button[@value='Sign In']"),"SignIn");
        return new BorrowerLoginPage(driver);
    }
	
	
}
