package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.UIObjPropertiesReader;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 19-01-2018.
 */
public class BackOfficeAppDetailsPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeAppDetailsPage.class);

    public BackOfficeAppDetailsPage(WebDriver driver) throws Exception {

        this.driver = driver;
        //driver.navigate().refresh();
        //assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_forgetPasswordLink)).isDisplayed());
        logger.info("=========== BackOfficeAppDetailsPage is loaded============");
    }
    public BackOfficeAppDetailsPage(){}
    
    public static void ApplicationDataVerification() throws Exception
    {
      Thread.sleep(10000);
      PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
      Thread.sleep(5000);
      PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_applicationDataVerification));
      PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_applicationDataVerification),"Click Application verification button");
      Thread.sleep(5000);
      PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_applicationDataCheck));
      PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_applicationDataCheck));
      
      //PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_verifyCheckbox));
      PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_applicationDataVerifyButton),"Click app verification button");
      Thread.sleep(5000);	
    }
    
    public static void BuisnessSocialVerification() throws Exception
    {
    	Thread.sleep(5000);
    	PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
    	Thread.sleep(5000);
    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessSocialVerification));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessSocialVerification),"Click Business Social verification button");
        Thread.sleep(5000);
        PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessSocialVerifyCheck));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_businessSocialVerifyButton),"Click business social verify button");
        Thread.sleep(5000);
    }
    
    
    public static void CashFlowUploadStep() throws Exception
    {
    	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton));
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton),"Click Action Icon");
    	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadBankStatementButton));
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_uploadBankStatementButton),"Click upload bank statement button");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankNameTextBox), "IDBICCI Bank");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankACNumTextBox), "123456789");
    	// PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankACTypeTextBox));
    	// PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankACTypeTextBox), "Click Acc type");
    	 PortalFuncUtils.selectSearchDropdown2(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankACTypeTextBox),"Savings");
     	
    	 Thread.sleep(2000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankStatementUpload),"Click upload statement button");
    	 Thread.sleep(2000);
    	 // below line execute the AutoIT script .
         Runtime.getRuntime().exec("C:\\Users\\TEMP\\Desktop\\BankStatementUpload.exe");	
    	 Thread.sleep(5000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankStatementSubmit),"Submit bank statement");
    	 //PortalFuncUtils.clickevent(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_bankStatementUpload),"Click upload statement button");
    	
       }
       public static void CashFundingAccountInitialize() throws Exception
       {
    	 Thread.sleep(15000);
    	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowTab));
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowTab),"Click cashflow tab");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowSelectAC),"Click select account");
     	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_SelectFundingAC),"Select funding account");
         Thread.sleep(5000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowTab),"Click cashflow tab");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowSelectAC),"Click select account");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_SelectCashFlowAC),"Select cashflow account");
    	 Thread.sleep(5000);    	      	
    	}
    	
        public static void CashFlowManualVerification() throws Exception
        {    	
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
     	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowDetailsButton));
     	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowDetailsButton),"Select cashflow details button");
     	 Thread.sleep(5000);
     	 PortalFuncUtils.selectSearchDropdown2(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowSelectProduct),"");
    	 Thread.sleep(5000);
     	// PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_verifyCheckbox));
    	// PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_verifyCheckbox));
     	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowVerifySubmit), "Verify Button");
     	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowCloseButton), "Close Button");
     	 Thread.sleep(2000);
        }
        
        public static void CashFlowManualVerificationSec(String flow) throws Exception
        {    	
         Thread.sleep(5000);
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
     	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowDetailsButton));
     	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowDetailsButton),"Select cashflow details button");
     	 Thread.sleep(5000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_verifyCheckbox),"Select list of flow info");
         
     	 if(flow.equalsIgnoreCase("StarterMCA")){
     	   	 PortalFuncUtils.selectFromVadinDropdownSec(1,"StarterMCA");
     	 }else if(flow.equalsIgnoreCase("PremiumLOC")){
         	 PortalFuncUtils.selectFromVadinDropdownSec(4,"PremiumLoc");
     	 }else{ }
     		 
    	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowVerifySubmit), "Verify Button");
     	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_CashFlowCloseButton), "Close Button");
     	 Thread.sleep(20000);
        }
        
        
        public static void DecideProduct(int i, String Product) throws Exception 
        {
        	 Thread.sleep(5000);
        	 PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton));
        	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton),"Click Action Icon");
             PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_decideProduct),"Select decide product");
             PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_selectListproduct),"Select list of flow info");
             PortalFuncUtils.selectFromVadinDropdownSec(i,Product);
             PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_moveProduct),"Select decide product");
             
        }
        
        
        //After Offer Gentration
        public static void FillOfferGenratioStep() throws Exception
        {
         	
         PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
         PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox), "Daily");
     	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_ProductTextBox), "Daily");
      	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_ProgramTextBox), "Low");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_FactorTextBox), "1000");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_TermsTextBox), "6 Months");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_PaymentTextBox), "30000");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox), "25000");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_SubmitedDateTextBox), "2018-02-17"); 
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox), "2018-02-17"); 
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_PSFpercentageTextBox), "3");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_CompPercentageTextBox), "10");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox), "8");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_CommentsTextBox), "Test comments");
    	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferGen_FunderFeeTextBox), "2");
     	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OfferSubmit),"Click offer submit button");
    	 Thread.sleep(20000);     
        }
        
        public static void UploadDocsStep() throws Exception
        {    	 
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Action Icon");
         PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocs));
      	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocs), "Upload Docs");
          
         Thread.sleep(8000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_PhotoIDUpload),"Click PhotoID upload button");
    	 Thread.sleep(2000);
    	 // below line execute the AutoIT script .
         Runtime.getRuntime().exec("C:\\Users\\thangaraj.c\\Desktop\\PhotoIDUpload.exe");	   	
     	 Thread.sleep(10000);
     	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnershipUpload),"Click ownership upload button");
     	 Thread.sleep(2000);
     	 // below line execute the AutoIT script .
          Runtime.getRuntime().exec("C:\\Users\\thangaraj.c\\Desktop\\PhotoIDUpload.exe");	 
      	 Thread.sleep(10000);
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ProofOfRentUpload),"Click Proof of rent upload button");
    	 Thread.sleep(2000);
    	 // below line execute the AutoIT script .
         Runtime.getRuntime().exec("C:\\Users\\thangaraj.c\\Desktop\\PhotoIDUpload.exe");	  
         Thread.sleep(10000);
   	     PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_VoidedCheckUpload),"Click voidedcheck upload button");
   	     Thread.sleep(2000);
   	     // below line execute the AutoIT script .
         Runtime.getRuntime().exec("C:\\Users\\thangaraj.c\\Desktop\\PhotoIDUpload.exe");	    	
    	 Thread.sleep(30000);    	 
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocsClose),"Click upload docs close button");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
      	 Thread.sleep(4000);
        }
        
        // Bank Verify 
        public static void BankMaualVerification() throws Exception
        {       	
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankDetails),"Click details button");
    	 PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankAccNumTextBox));
     	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankAccNumTextBox), "0000");
     	 PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankRoutingNumTextBox));      	
     	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankRoutingNumTextBox), "123456789");
    	 PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankverifyCheckbox));
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankVerifyButton),"Click Verify button");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankVerifyCloseButton),"Click bank close button");
     	 Thread.sleep(8000);
        }
        
        
    	// ID Verification
        public static void IDMaualVerification() throws Exception
        { 
          driver.navigate().refresh();
          Thread.sleep(30000);
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");          	
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyDetails),"Click details button");
    	  Thread.sleep(10000);
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox1));      	
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox1));
    	  System.out.println("----1----"); 
    	  PortalFuncUtils.isVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2),driver); 
     	 
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2)); 
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2));
    	  System.out.println("----2----"); 
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox3)); 
     	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox3));
     	  System.out.println("----3----");
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyButton),"Click Verify button");
    	  Thread.sleep(5000);
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyCloseButton),"Click ID verify close button");
        }
        
    	// Business owner Verification
        public static void BusinessOwnerMaualVerification() throws Exception
        { 
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetails),"Click details button");
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1)); 
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1));
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyButton),"Click Verify button");
    	  Thread.sleep(5000);
    	  //PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButton),"Click ID verify close button");
    	  Thread.sleep(10000);
        }
        
        
    	// Rent Verification 
        public static void RentMaualVerification() throws Exception
        { 
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_RentDetails),"Click details button");
    	   PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox));      	
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox), "2348975199");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerNameTextBox), "Steve");
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerVerifyButton),"Click Verify button");
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerVerifyCloseButton),"Click owner verify close button");
    	   Thread.sleep(10000);
        }
    	 
    	 // Generate Agreement
        public static void GeerateAgreementMaualVerification() throws Exception
        {  
        	Thread.sleep(10000);
           PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Click Action Icon");
    	   PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenerateAgreement), "Click generate agreement");
    	   Thread.sleep(10000);
    	   PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreePrepayCheck));      	
    	   PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreePrepayCheck));
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeFirstRepurchasePrice), "2500");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeSecondRepurchasePrice), "3500");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeThirdRepurchasePrice), "4500");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeFourthRepurchasePrice), "5500");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeFifthRepurchasePrice), "6500");
   	   	   PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_GenAgreeNoStackCheck));
   	       PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_GenAgreeButton), "Click generate loan button");
   	       Thread.sleep(30000);
        }
        
         // Send Agreement
        public static void SendAgreementSteps() throws Exception
        { 
   	       PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Click Action Icon");
 	       PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_SendAgreement), "Click Send agreement");
 	       PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_VerifySendAgreement));      	
 	       PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_VerifySendAgreement), "Click Send agreement button");
 	       Thread.sleep(30000);
        }
 	      
 	     
 	    // Upload Signed agreement document
        public static void UploadSignedAgreementDocs() throws Exception
        {  
        	Thread.sleep(10000);
          PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actionsButton), "Action Icon");
          PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocs));
     	  PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocs), "Upload Docs");        
          Thread.sleep(8000);
   	      PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_SignedAgreementUpload),"Click SignedAgreement upload button");
   	      Thread.sleep(2000);
   	      // below line execute the AutoIT script .
          Runtime.getRuntime().exec("C:\\Users\\thangaraj.c\\Desktop\\PhotoIDUpload.exe");	
          Thread.sleep(20000);
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_UploadDocsClose),"Click upload docs close button");
   	      PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");
        }
      
    	
    	//Contract Verification
        public static void ContractVerification() throws Exception
        { 
   	      Thread.sleep(20000);
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractVerifyDetails),"Click details button");
    	  Thread.sleep(10000);
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerverifyCheckbox1));
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerVerifyButton),"Click Verify button");
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerCloseButton),"Click close button");
        } 
    	 
    	 //Funding verification
        public static void FundingVerification() throws Exception
        { 
    	   Thread.sleep(20000);
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingVerifyDetails),"Click details button");
     	   Thread.sleep(10000);
     	   PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerverifyCheckbox1));
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerVerifyButton),"Click funding verify button");
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerCloseButton),"Click funding close button");
        }  
        
        
        // Loc Modifiation
        
        // Bank Verify 
        public static void BankMaualVerificationLoc() throws Exception
        {       	
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankDetailsLoc),"Click details button");
    	 PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankAccNumTextBox));
     	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankAccNumTextBox), "0000");
     	 PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankRoutingNumTextBox));      	
     	 PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankRoutingNumTextBox), "123456789");
    	 PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankverifyCheckbox));
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankVerifyButton),"Click Verify button");
    	 PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BankVerifyCloseButtonLoc),"Click bank close button");
     	 Thread.sleep(8000);
        }
        
        
    	// ID Verification
        public static void IDMaualVerificationLoc() throws Exception
        { 
          driver.navigate().refresh();
          Thread.sleep(30000);
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_DashBoardTab),"Select verification dashboard");          	
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyDetailsLoc),"Click details button");
    	  Thread.sleep(10000);
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox1));      	
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox1));
    	  System.out.println("----1----"); 
    	  PortalFuncUtils.isVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2),driver); 
     	 
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2)); 
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox2));
    	  System.out.println("----2----"); 
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox3)); 
     	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDverifyCheckbox3));
     	  System.out.println("----3----");
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyButton),"Click Verify button");
    	  Thread.sleep(5000);
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_IDVerifyCloseButtonLoc),"Click ID verify close button");
        }
        
    	// Business owner Verification
        public static void BusinessOwnerMaualVerificationLoc() throws Exception
        { 
          PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc),"Click details button");
    	  PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1)); 
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1));
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyButton),"Click Verify button");
    	  Thread.sleep(5000);
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc),"Click ID verify close button"); 
        }
        
        
    	// Rent Verification 
        public static void RentMaualVerificationLoc() throws Exception
        { 
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_RentDetailsLoc),"Click details button");
    	   PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox));      	
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox), "2348975199");
    	   PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerNameTextBox), "Steve");
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerVerifyButton),"Click Verify button");
    	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_OwnerVerifyCloseButtonLoc),"Click owner verify close button");
    	   Thread.sleep(10000);
        }
    	
      //Contract Verification
        public static void ContractVerificationLoc() throws Exception
        { 
   	      Thread.sleep(20000);
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractVerifyDetailsLoc),"Click details button");
    	  Thread.sleep(10000);
    	  PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerverifyCheckbox1));
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerVerifyButton),"Click Verify button");
    	  PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ContractOwnerCloseButtonLoc),"Click close button");
        } 
    	 
    	 //Funding verification
        public static void FundingVerificationLoc() throws Exception
        { 
    	   Thread.sleep(20000);
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingVerifyDetailsLoc),"Click details button");
     	   Thread.sleep(5000);
     	   //PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerInitiateButton),"Click Initiate button");
     	   //Thread.sleep(10000);
     	   PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerverifyCheckbox1));
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerVerifyButton),"Click funding verify button");
     	   Thread.sleep(10000);
     	   PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_FundingOwnerCloseButtonLoc),"Click funding close button");
        }  
        
        public static String BackOfficeGetStatus() throws Exception
        {
        	Thread.sleep(10000);
        	return PortalFuncUtils.getStatusText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_homepage_actions_ApplicationStatus));
        }
    
}
