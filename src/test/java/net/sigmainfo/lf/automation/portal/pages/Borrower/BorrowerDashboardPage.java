package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import javax.swing.*;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 28-11-2017.
 */
public class BorrowerDashboardPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerDashboardPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerDashboardPage(WebDriver driver) throws Exception {

        PortalFuncUtils.waitForPageToLoad(driver);
       // assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_accountLinkedMessageLabel)));
       // assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_purposeOfFundsTitle)).getText().contains(PortalParam.useOfFunds));
        logger.info("=========== BorrowerDashboardPage is loaded============");
    }
    public BorrowerDashboardPage(){}

    public BorrowerHomePage logout(WebDriver driver) throws Exception {
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown)),"Profile dropdown not visible");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown)),"Profile dropdown not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_profileDropdown), "Profile");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.dashboard_logoutLink)),"Logout link not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.dashboard_logoutLink), "Log out");
        	
        	
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
             return new BorrowerHomePage(driver);
    }
}
