package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalPropertiesReader.java
 * Description          : Reads portal.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class PortalPropertiesReader {

    @Value(value = "${borrowerUrl}")
    private String borrowerUrl;
    @Value(value = "${backOfficeUrl}")
    private String backOfficeUrl;
    @Value(value = "${custom_report_location}")
    private String custom_report_location;
    @Value(value = "${browser}")
    private String browser;
    @Value(value = "${encryptedPassword}")
    private String encryptedPassword;
    @Value(value = "${backOfficeUsername}")
    private String backOfficeUsername;
    @Value(value = "${backOfficePassword}")
    private String backOfficePassword;
    @Value(value = "${gmailId}")
    private String gmailId;
    @Value(value = "${gmailPassword}")
    private String gmailPassword;
    @Value(value = "${plaid_user}")
    private String plaid_user;
    @Value(value = "${plaid_password}")
    private String plaid_password;
    @Value(value = "${upload_file_location}")
    private String upload_file_location;

    @Value(value = "${iframeSource}")
    private String iframeSource;

    @Value(value = "${bankToConnect}")
    private String bankToConnect;

    public String getIframeSource() {
        return iframeSource;
    }

    public void setIframeSource(String iframeSource) {
        this.iframeSource = iframeSource;
    }

    public String getBankToConnect() {
        return bankToConnect;
    }

    public void setBankToConnect(String bankToConnect) {
        this.bankToConnect = bankToConnect;
    }

    public String getUpload_file_location() {
        return upload_file_location;
    }

    public void setUpload_file_location(String upload_file_location) {
        this.upload_file_location = upload_file_location;
    }

    public String getPlaid_user() {
        return plaid_user;
    }

    public void setPlaid_user(String plaid_user) {
        this.plaid_user = plaid_user;
    }

    public String getPlaid_password() {
        return plaid_password;
    }

    public void setPlaid_password(String plaid_password) {
        this.plaid_password = plaid_password;
    }

    public String getGmailId() {
        return gmailId;
    }

    public void setGmailId(String gmailId) {
        this.gmailId = gmailId;
    }

    public String getGmailPassword() {
        return gmailPassword;
    }

    public void setGmailPassword(String gmailPassword) {
        this.gmailPassword = gmailPassword;
    }

    public String getBackOfficeUsername() {
        return backOfficeUsername;
    }

    public void setBackOfficeUsername(String backOfficeUsername) {
        this.backOfficeUsername = backOfficeUsername;
    }

    public String getBackOfficePassword() {
        return backOfficePassword;
    }

    public void setBackOfficePassword(String backOfficePassword) {
        this.backOfficePassword = backOfficePassword;
    }

    public String getBorrowerUrl() {
        return borrowerUrl;
    }

    public String getBackOfficeUrl() {
        return backOfficeUrl;
    }

    public void setBackOfficeUrl(String backOfficeUrl) {
        this.backOfficeUrl = backOfficeUrl;
    }

    public void setBorrowerUrl(String borrowerUrl) {
        this.borrowerUrl = borrowerUrl;
    }

    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
