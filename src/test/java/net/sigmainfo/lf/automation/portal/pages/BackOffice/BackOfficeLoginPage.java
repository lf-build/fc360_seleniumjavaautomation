package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 19-01-2018.
 */
public class BackOfficeLoginPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerHomePage.class);

    public BackOfficeLoginPage(WebDriver driver) throws Exception {

        this.driver = driver;
        assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_forgetPasswordLink)).isDisplayed());
        logger.info("=========== BackOfficeLoginPage is loaded============");
    }
    public BackOfficeLoginPage(){}

    public static BackOfficeSearchPage loginToBackOffice(WebDriver driver) throws Exception {
        try {
            //wait.until(ExpectedConditions.visibilityOfElementLocated(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_forgetPasswordLink)));
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_forgetPasswordLink)));
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), PortalParam.backofficeUsername);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.backofficePassword));
            PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_agreementCheckbox));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        return new BackOfficeSearchPage(driver);
    }
}
