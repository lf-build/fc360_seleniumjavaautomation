package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by shaishav.s on 09-05-2017.
 */
@Component
public class UIObjPropertiesReader {

    @Value(value = "${borrower.loginpage.loginLabel}")
    private String borrower_loginpage_loginLabel;

    @Value(value = "${borrower.loginpage.emailTextBox}")
    private String borrower_loginpage_emailTextBox;

    @Value(value = "${borrower.loginpage.passwordTextBox}")
    private String borrower_loginpage_passwordTextBox;

    @Value(value = "${borrower.loginpage.submitButton}")
    private String borrower_loginpage_submitButton;

    @Value(value = "${homepage.newApplicationLabel}")
    private String homepage_newApplicationLabel;

    @Value(value = "${homepage.signinButton}")
    private String homepage_signinButton;

    @Value(value = "${homepage.basicTab}")
    private String homepage_basicTab;

    @Value(value = "${homepage.yourBusinessTab}")
    private String homepage_yourBusinessTab;

    @Value(value = "${homepage.aboutYouTab}")
    private String homepage_aboutYouTab;

    @Value(value = "${homepage.financialReviewTab}")
    private String homepage_financialReviewTab;

    @Value(value = "${homepage.basicTab.businessNameTextBox}")
    private String homepage_basicTab_businessNameTextBox;

    @Value(value = "${homepage.basicTab.amountSeekingTextBox}")
    private String homepage_basicTab_amountSeekingTextBox;

    @Value(value = "${homepage.basicTab.useOfFundsDropdown}")
    private String homepage_basicTab_useOfFundsDropdown;

    @Value(value = "${homepage.basicTab.annualBusinessRevenueTextBox}")
    private String homepage_basicTab_annualBusinessRevenueTextBox;

    @Value(value = "${homepage.basicTab.firstNameTextBox}")
    private String homepage_basicTab_firstNameTextBox;

    @Value(value = "${homepage.basicTab.lastNameTextBox}")
    private String homepage_basicTab_lastNameTextBox;

    @Value(value = "${homepage.basicTab.phoneTextBox}")
    private String homepage_basicTab_phoneTextBox;

    @Value(value = "${homepage.basicTab.emailTextBox}")
    private String homepage_basicTab_emailTextBox;

    @Value(value = "${homepage.basicTab.confirmPasswordTextBox}")
    private String homepage_basicTab_confirmPasswordTextBox;

    @Value(value = "${homepage.basicTab.passwordTextBox}")
    private String homepage_basicTab_passwordTextBox;

    @Value(value = "${homepage.basicTab.signInButton}")
    private String homepage_basicTab_signInButton;

    @Value(value = "${homepage.basicTab.continueButton}")
    private String homepage_basicTab_continueButton;

    @Value(value = "${homepage.basicTab.startedApplicationLabel}")
    private String homepage_basicTab_startedApplicationLabel;

    @Value(value = "${homepage.yourBusinessTab.businessLabel}")
    private String homepage_yourBusinessTab_businessLabel;

    @Value(value = "${homepage.yourBusinessTab.businessAddressTextBox}")
    private String homepage_yourBusinessTab_businessAddressTextBox;

    @Value(value = "${homepage.yourBusinessTab.dbaTextBox}")
    private String homepage_yourBusinessTab_dbaTextBox;

    @Value(value = "${homepage.yourBusinessTab.cityTextBox}")
    private String homepage_yourBusinessTab_cityTextBox;

    @Value(value = "${homepage.yourBusinessTab.stateDropdown}")
    private String homepage_yourBusinessTab_stateDropdown;

    @Value(value = "${homepage.yourBusinessTab.zipCodeTextBox}")
    private String homepage_yourBusinessTab_zipCodeTextBox;

    @Value(value = "${homepage.yourBusinessTab.businessPhoneTextBox}")
    private String homepage_yourBusinessTab_businessPhoneTextBox;

    @Value(value = "${homepage.yourBusinessTab.bstMonthDropdown}")
    private String homepage_yourBusinessTab_bstMonthDropdown;

    @Value(value = "${homepage.yourBusinessTab.bstDayDropdown}")
    private String homepage_yourBusinessTab_bstDayDropdown;

    @Value(value = "${homepage.financialReviewTab.docusign.okButton}")
    private String homepage_financialReviewTab_docusign_okButton;

    @Value(value = "${homepage.financialReviewTab.docusign.startDocusignButton}")
    private String homepage_financialReviewTab_docusign_startDocusignButton;

    @Value(value = "${homepage.financialReviewTab.docusign.signHereButton}")
    private String homepage_financialReviewTab_docusign_signHereButton;

    @Value(value = "${homepage.financialReviewTab.docusign.adoptSignButton}")
    private String homepage_financialReviewTab_docusign_adoptSignButton;

    @Value(value = "${homepage.financialReviewTab.docusign.finishButton}")
    private String homepage_financialReviewTab_docusign_finishButton;

    @Value(value = "${borrower.accountLinkedMessageLabel}")
    private String borrower_accountLinkedMessageLabel;

    @Value(value = "${borrower.purposeOfFundsTitle}")
    private String borrower_purposeOfFundsTitle;

    @Value(value = "${borrower.dashboard.appIdLabel}")
    private String borrower_dashboard_appIdLabel;
    
    @Value(value = "${backoffice.searchpage.SelectApp}")
    private String backoffice_searchpage_SelectApp;
    
    @Value(value="${backoffice.homepage.actions.uploadBankStatementButton}")
    private String backoffice_homepage_actions_uploadBankStatementButton;
    
    @Value(value="${backoffice.homepage.actions.bankNameTextBox}")
    private String backoffice_homepage_actions_bankNameTextBox;
    
    @Value(value="${backoffice.homepage.actions.bankACNumTextBox}")
    private String backoffice_homepage_actions_bankACNumTextBox;
   
   	@Value(value="${backoffice.homepage.actions.bankACTypeTextBox}")
    private String backoffice_homepage_actions_bankACTypeTextBox;
   
   	@Value(value="${backoffice.homepage.actions.bankStatementUpload}")
    private String backoffice_homepage_actions_bankStatementUpload;
   
   	@Value(value="${backoffice.homepage.actions.bankStatementSubmit}")
   	private String backoffice_homepage_actions_bankStatementSubmit;
    
   	@Value(value="${backoffice.homepage.actions.CashFlowTab}")
   	private String backoffice_homepage_actions_CashFlowTab;
	
    @Value(value="${backoffice.homepage.actions.CashFlowSelectAC}")
    private String backoffice_homepage_actions_CashFlowSelectAC;
	
    @Value(value="${backoffice.homepage.actions.SelectCashFlowAC}")
    private String backoffice_homepage_actions_SelectCashFlowAC;
    

	@Value(value="${backoffice.homepage.actions.SelectFundingAC}")
    private String backoffice_homepage_actions_SelectFundingAC;
	
	@Value(value="${backoffice.homepage.actions.DashBoardTab}")
    private String backoffice_homepage_actions_DashBoardTab;
	
	@Value(value="${backoffice.homepage.actions.CashFlowDetailsButton}")
    private String backoffice_homepage_actions_CashFlowDetailsButton;
	
	@Value(value="${backoffice.homepage.actions.CashFlowSelectProduct}")
    private String backoffice_homepage_actions_CashFlowSelectProduct;
	
	@Value(value="${backoffice.homepage.actions.verifyCheckbox}")
    private String backoffice_homepage_actions_verifyCheckbox;
	
	@Value(value="${backoffice.homepage.actions.CashFlowVerifySubmit}")
    private String backoffice_homepage_actions_CashFlowVerifySubmit;
	
	@Value(value="${backoffice.homepage.actions.CashFlowCloseButton}")
    private String backoffice_homepage_actions_CashFlowCloseButton;	
	
	@Value(value="${backoffice.homepage.actions.decideProduct}")
    private String backoffice_homepage_actions_decideProduct;	
	
	@Value(value="${backoffice.homepage.actions.moveProduct}")
    private String backoffice_homepage_actions_moveProduct;	
	
	@Value(value="${backoffice.homepage.actions.selectListproduct}")
    private String backoffice_homepage_actions_selectListproduct;	

	@Value(value="${backoffice.homepage.actions.OfferGen.PaymentFrequencyTextBox}")
    private String backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox;	

	@Value(value="${backoffice.homepage.actions.OfferGen.ProgramTextBox}")
    private String backoffice_homepage_actions_OfferGen_ProgramTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.FactorTextBox}")
    private String backoffice_homepage_actions_OfferGen_FactorTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.TermsTextBox}")
    private String backoffice_homepage_actions_OfferGen_TermsTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.PaymentTextBox}")
    private String backoffice_homepage_actions_OfferGen_PaymentTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.ApprovedAmtTextBox}")
    private String backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.ApprovedDateTimeTextBox}")
    private String backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.SubmitedDateTextBox}")
    private String backoffice_homepage_actions_OfferGen_SubmitedDateTextBox;
	
	@Value(value="${backoffice.homepage.actions.OfferGen.PSFpercentageTextBox}")
    private String backoffice_homepage_actions_OfferGen_PSFpercentageTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.FunderFeeTextBox}")
    private String backoffice_homepage_actions_OfferGen_FunderFeeTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.CompPercentageTextBox}")
    private String backoffice_homepage_actions_OfferGen_CompPercentageTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.MaxCrossPersentageTextBox}")
    private String backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox;

	@Value(value="${backoffice.homepage.actions.OfferSubmit}")
    private String backoffice_homepage_actions_OfferSubmit;

	@Value(value="${backoffice.homepage.actions.OfferGen.ProductTextBox}")
    private String backoffice_homepage_actions_OfferGen_ProductTextBox;

	@Value(value="${backoffice.homepage.actions.OfferGen.CommentsTextBox}")
    private String backoffice_homepage_actions_OfferGen_CommentsTextBox;

	@Value(value="${backoffice.homepage.actions.UploadDocs}")
    private String backoffice_homepage_actions_UploadDocs;

	@Value(value="${backoffice.homepage.actions.PhotoIDUpload}")
    private String backoffice_homepage_actions_PhotoIDUpload;

	@Value(value="${backoffice.homepage.actions.OwnershipUpload}")
    private String backoffice_homepage_actions_OwnershipUpload;

	@Value(value="${backoffice.homepage.actions.ProofOfRentUpload}")
    private String backoffice_homepage_actions_ProofOfRentUpload;

	@Value(value="${backoffice.homepage.actions.VoidedCheckUpload}")
    private String backoffice_homepage_actions_VoidedCheckUpload;

	@Value(value="${backoffice.homepage.actions.UploadDocsClose}")
    private String backoffice_homepage_actions_UploadDocsClose;

	@Value(value="${backoffice.homepage.actions.BankDetails}")
    private String backoffice_homepage_actions_BankDetails;
	
	@Value(value="${backoffice.homepage.actions.BankAccNumTextBox}")
    private String backoffice_homepage_actions_BankAccNumTextBox;
	
	@Value(value="${backoffice.homepage.actions.BankverifyCheckbox}")
    private String backoffice_homepage_actions_BankverifyCheckbox;
	
	@Value(value="${backoffice.homepage.actions.BankVerifyButton}")
    private String backoffice_homepage_actions_BankVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.BankRoutingNumTextBox}")
    private String backoffice_homepage_actions_BankRoutingNumTextBox;
	
	@Value(value="${backoffice.homepage.actions.BankVerifyCloseButton}")
    private String backoffice_homepage_actions_BankVerifyCloseButton;
	
	@Value(value="${backoffice.homepage.actions.IDVerifyDetails}")
    private String backoffice_homepage_actions_IDVerifyDetails;
	
	@Value(value="${backoffice.homepage.actions.IDverifyCheckbox1}")
    private String backoffice_homepage_actions_IDverifyCheckbox1;
	
	@Value(value="${backoffice.homepage.actions.IDverifyCheckbox2}")
    private String backoffice_homepage_actions_IDverifyCheckbox2;
	
	@Value(value="${backoffice.homepage.actions.IDverifyCheckbox3}")
    private String backoffice_homepage_actions_IDverifyCheckbox3;
	
	@Value(value="${backoffice.homepage.actions.IDVerifyButton}")
    private String backoffice_homepage_actions_IDVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.IDVerifyCloseButton}")
    private String backoffice_homepage_actions_IDVerifyCloseButton;
	
	@Value(value="${backoffice.homepage.actions.BusinessOwnerVerifyDetails}")
    private String backoffice_homepage_actions_BusinessOwnerVerifyDetails;
	
	@Value(value="${backoffice.homepage.actions.BusinessOwnerverifyCheckbox1}")
    private String backoffice_homepage_actions_BusinessOwnerverifyCheckbox1;
	
	@Value(value="${backoffice.homepage.actions.BusinessOwnerVerifyButton}")
    private String backoffice_homepage_actions_BusinessOwnerVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.BusinessOwnerVerifyCloseButton}")
    private String backoffice_homepage_actions_BusinessOwnerVerifyCloseButton;
	
	@Value(value="${backoffice.homepage.actions.RentDetails}")
    private String backoffice_homepage_actions_RentDetails;
	
	@Value(value="${backoffice.homepage.actions.OwnerPhoneNumTextBox}")
    private String backoffice_homepage_actions_OwnerPhoneNumTextBox;
	
	@Value(value="${backoffice.homepage.actions.OwnerNameTextBox}")
    private String backoffice_homepage_actions_OwnerNameTextBox;
	
	@Value(value="${backoffice.homepage.actions.OwnerVerifyButton}")
    private String backoffice_homepage_actions_OwnerVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.OwnerVerifyCloseButton}")
    private String backoffice_homepage_actions_OwnerVerifyCloseButton;
	
	@Value(value="${backoffice.homepage.actions.GenerateAgreement}")
    private String backoffice_homepage_actions_GenerateAgreement;
	
	@Value(value="${backoffice.homepage.actions.GenAgreePrepayCheck}")
    private String backoffice_homepage_actions_GenAgreePrepayCheck;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeFirstRepurchasePrice}")
    private String backoffice_homepage_actions_GenAgreeFirstRepurchasePrice;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeSecondRepurchasePrice}")
    private String backoffice_homepage_actions_GenAgreeSecondRepurchasePrice;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeThirdRepurchasePrice}")
    private String backoffice_homepage_actions_GenAgreeThirdRepurchasePrice;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeFourthRepurchasePrice}")
    private String backoffice_homepage_actions_GenAgreeFourthRepurchasePrice;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeFifthRepurchasePrice}")
    private String backoffice_homepage_actions_GenAgreeFifthRepurchasePrice;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeNoStackCheck}")
    private String backoffice_homepage_actions_GenAgreeNoStackCheck;
	
	@Value(value="${backoffice.homepage.actions.GenAgreeButton}")
    private String backoffice_homepage_GenAgreeButton;
	
	@Value(value="${backoffice.homepage.actions.SendAgreement}")
    private String backoffice_homepage_actions_SendAgreement;
	
	@Value(value="${backoffice.homepage.actions.VerifySendAgreement}")
    private String backoffice_homepage_actions_VerifySendAgreement;
	
	@Value(value="${backoffice.homepage.actions.SignedAgreementUpload}")
    private String backoffice_homepage_actions_SignedAgreementUpload;
	
	@Value(value="${backoffice.homepage.actions.ContractVerifyDetails}")
    private String backoffice_homepage_actions_ContractVerifyDetails;
	
	@Value(value="${backoffice.homepage.actions.ContractOwnerverifyCheckbox1}")
    private String backoffice_homepage_actions_ContractOwnerverifyCheckbox1;
	
	@Value(value="${backoffice.homepage.actions.ContractOwnerVerifyButton}")
    private String backoffice_homepage_actions_ContractOwnerVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.ContractOwnerCloseButton}")
    private String backoffice_homepage_actions_ContractOwnerCloseButton;
	
	
	@Value(value="${backoffice.homepage.actions.FundingVerifyDetails}")
    private String backoffice_homepage_actions_FundingVerifyDetails;
	
	@Value(value="${backoffice.homepage.actions.FundingOwnerverifyCheckbox1}")
    private String backoffice_homepage_actions_FundingOwnerverifyCheckbox1;
	
	@Value(value="${backoffice.homepage.actions.FundingOwnerVerifyButton}")
    private String backoffice_homepage_actions_FundingOwnerVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.FundingOwnerCloseButton}")
    private String backoffice_homepage_actions_FundingOwnerCloseButton;
	
	@Value(value="${backoffice.searchpage.searchMenu}")
    private String backoffice_searchpage_searchMenu;
	
	@Value(value="${backoffice.homepage.actions.IDVerifyDetailsLoc}")
    private String backoffice_homepage_actions_IDVerifyDetailsLoc;
	@Value(value="${backoffice.homepage.actions.BusinessOwnerVerifyDetailsLoc}")
    private String backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc;
	@Value(value="${backoffice.homepage.actions.RentDetailsLoc}")
    private String backoffice_homepage_actions_RentDetailsLoc;
	@Value(value="${backoffice.homepage.actions.BankDetailsLoc}")
    private String backoffice_homepage_actions_BankDetailsLoc;
	@Value(value="${backoffice.homepage.actions.FundingVerifyDetailsLoc}")
    private String backoffice_homepage_actions_FundingVerifyDetailsLoc;
	@Value(value="${backoffice.homepage.actions.ContractVerifyDetailsLoc}")
    private String backoffice_homepage_actions_ContractVerifyDetailsLoc;
	
	@Value(value="${backoffice.homepage.actions.BankVerifyCloseButtonLoc}")
    private String backoffice_homepage_actions_BankVerifyCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.IDVerifyCloseButtonLoc}")
    private String backoffice_homepage_actions_IDVerifyCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.BusinessOwnerVerifyCloseButtonLoc}")
    private String backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.OwnerVerifyCloseButtonLoc}")
    private String backoffice_homepage_actions_OwnerVerifyCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.ContractOwnerCloseButtonLoc}")
    private String backoffice_homepage_actions_ContractOwnerCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.FundingOwnerCloseButtonLoc}")
    private String backoffice_homepage_actions_FundingOwnerCloseButtonLoc;
	@Value(value="${backoffice.homepage.actions.FundingOwnerInitiateButton}")
    private String backoffice_homepage_actions_FundingOwnerInitiateButton;
	
	@Value(value="${backoffice.homepage.actions.applicationDataVerification}")
    private String backoffice_homepage_actions_applicationDataVerification;
	@Value(value="${backoffice.homepage.actions.applicationDataCheck}")
    private String backoffice_homepage_actions_applicationDataCheck;
	@Value(value="${backoffice.homepage.actions.applicationDataVerifyButton}")
    private String backoffice_homepage_actions_applicationDataVerifyButton;
	
	
	@Value(value="${backoffice.homepage.actions.businessSocialVerification}")
    private String backoffice_homepage_actions_businessSocialVerification;
	@Value(value="${backoffice.homepage.actions.businessSocialVerifyCheck}")
    private String backoffice_homepage_actions_businessSocialVerifyCheck;
	@Value(value="${backoffice.homepage.actions.businessSocialVerifyButton}")
    private String backoffice_homepage_actions_businessSocialVerifyButton;
	
	@Value(value="${backoffice.homepage.actions.ApplicationStatus}")
    private String backoffice_homepage_actions_ApplicationStatus;

	

	public String getBorrower_dashboard_appIdLabel() {
        return borrower_dashboard_appIdLabel;
    }

    public void setBorrower_dashboard_appIdLabel(String borrower_dashboard_appIdLabel) {
        this.borrower_dashboard_appIdLabel = borrower_dashboard_appIdLabel;
    }

    public String getBorrower_purposeOfFundsTitle() {
        return borrower_purposeOfFundsTitle;
    }

    public void setBorrower_purposeOfFundsTitle(String borrower_purposeOfFundsTitle) {
        this.borrower_purposeOfFundsTitle = borrower_purposeOfFundsTitle;
    }

    public String getBorrower_accountLinkedMessageLabel() {
        return borrower_accountLinkedMessageLabel;
    }

    public void setBorrower_accountLinkedMessageLabel(String borrower_accountLinkedMessageLabel) {
        this.borrower_accountLinkedMessageLabel = borrower_accountLinkedMessageLabel;
    }

    public String getHomepage_financialReviewTab_docusign_finishButton() {
        return homepage_financialReviewTab_docusign_finishButton;
    }

    public void setHomepage_financialReviewTab_docusign_finishButton(String homepage_financialReviewTab_docusign_finishButton) {
        this.homepage_financialReviewTab_docusign_finishButton = homepage_financialReviewTab_docusign_finishButton;
    }

    public String getHomepage_financialReviewTab_docusign_startDocusignButton() {
        return homepage_financialReviewTab_docusign_startDocusignButton;
    }

    public void setHomepage_financialReviewTab_docusign_startDocusignButton(String homepage_financialReviewTab_docusign_startDocusignButton) {
        this.homepage_financialReviewTab_docusign_startDocusignButton = homepage_financialReviewTab_docusign_startDocusignButton;
    }

    public String getHomepage_financialReviewTab_docusign_signHereButton() {
        return homepage_financialReviewTab_docusign_signHereButton;
    }

    public void setHomepage_financialReviewTab_docusign_signHereButton(String homepage_financialReviewTab_docusign_signHereButton) {
        this.homepage_financialReviewTab_docusign_signHereButton = homepage_financialReviewTab_docusign_signHereButton;
    }

    public String getHomepage_financialReviewTab_docusign_adoptSignButton() {
        return homepage_financialReviewTab_docusign_adoptSignButton;
    }

    public void setHomepage_financialReviewTab_docusign_adoptSignButton(String homepage_financialReviewTab_docusign_adoptSignButton) {
        this.homepage_financialReviewTab_docusign_adoptSignButton = homepage_financialReviewTab_docusign_adoptSignButton;
    }

    public String getHomepage_financialReviewTab_docusign_okButton() {
        return homepage_financialReviewTab_docusign_okButton;
    }

    public void setHomepage_financialReviewTab_docusign_okButton(String homepage_financialReviewTab_docusign_okButton) {
        this.homepage_financialReviewTab_docusign_okButton = homepage_financialReviewTab_docusign_okButton;
    }
    
    public String getBackoffice_homepage_actions_bankNameTextBox() {
		return backoffice_homepage_actions_bankNameTextBox;
	}

	public void setBackoffice_homepage_actions_bankNameTextBox(String backoffice_homepage_actions_bankNameTextBox) {
		this.backoffice_homepage_actions_bankNameTextBox = backoffice_homepage_actions_bankNameTextBox;
	}
	
	 public String getBackoffice_homepage_actions_bankACNumTextBox() {
			return backoffice_homepage_actions_bankACNumTextBox;
		}

		public void setBackoffice_homepage_actions_bankACNumTextBox(String backoffice_homepage_actions_bankACNumTextBox) {
			this.backoffice_homepage_actions_bankACNumTextBox = backoffice_homepage_actions_bankACNumTextBox;
		}

		public String getBackoffice_homepage_actions_bankACTypeTextBox() {
			return backoffice_homepage_actions_bankACTypeTextBox;
		}

		public void setBackoffice_homepage_actions_bankACTypeTextBox(String backoffice_homepage_actions_bankACTypeTextBox) {
			this.backoffice_homepage_actions_bankACTypeTextBox = backoffice_homepage_actions_bankACTypeTextBox;
		}

		public String getBackoffice_homepage_actions_bankStatementUpload() {
			return backoffice_homepage_actions_bankStatementUpload;
		}

		public void setBackoffice_homepage_actions_bankStatementUpload(String backoffice_homepage_actions_bankStatementUpload) {
			this.backoffice_homepage_actions_bankStatementUpload = backoffice_homepage_actions_bankStatementUpload;
		}
		

		public String getBackoffice_homepage_actions_bankStatementSubmit() {
			return backoffice_homepage_actions_bankStatementSubmit;
		}

		public void setBackoffice_homepage_actions_bankStatementSubmit(String backoffice_homepage_actions_bankStatementSubmit) {
			this.backoffice_homepage_actions_bankStatementSubmit = backoffice_homepage_actions_bankStatementSubmit;
		}
		
		public String getBackoffice_homepage_actions_CashFlowTab() {
			return backoffice_homepage_actions_CashFlowTab;
		}

		public void setBackoffice_homepage_actions_CashFlowTab(String backoffice_homepage_actions_CashFlowTab) {
			this.backoffice_homepage_actions_CashFlowTab = backoffice_homepage_actions_CashFlowTab;
		}
		
		public String getBackoffice_homepage_actions_CashFlowSelectAC() {
			return backoffice_homepage_actions_CashFlowSelectAC;
		}

		public void setBackoffice_homepage_actions_CashFlowSelectAC(String backoffice_homepage_actions_CashFlowSelectAC) {
			this.backoffice_homepage_actions_CashFlowSelectAC = backoffice_homepage_actions_CashFlowSelectAC;
		}

		public String getBackoffice_homepage_actions_SelectCashFlowAC() {
			return backoffice_homepage_actions_SelectCashFlowAC;
		}

		public void setBackoffice_homepage_actions_SelectCashFlowAC(String backoffice_homepage_actions_SelectCashFlowAC) {
			this.backoffice_homepage_actions_SelectCashFlowAC = backoffice_homepage_actions_SelectCashFlowAC;
		}

		public String getBackoffice_homepage_actions_SelectFundingAC() {
			return backoffice_homepage_actions_SelectFundingAC;
		}

		public void setBackoffice_homepage_actions_SelectFundingAC(String backoffice_homepage_actions_SelectFundingAC) {
			this.backoffice_homepage_actions_SelectFundingAC = backoffice_homepage_actions_SelectFundingAC;
		}


		public String getBackoffice_homepage_actions_DashBoardTab() {
			return backoffice_homepage_actions_DashBoardTab;
		}

		public void setBackoffice_homepage_actions_DashBoardTab(String backoffice_homepage_actions_DashBoardTab) {
			this.backoffice_homepage_actions_DashBoardTab = backoffice_homepage_actions_DashBoardTab;
		}
		
		public String getBackoffice_homepage_actions_CashFlowDetailsButton() {
			return backoffice_homepage_actions_CashFlowDetailsButton;
		}

		public void setBackoffice_homepage_actions_CashFlowDetailsButton(
				String backoffice_homepage_actions_CashFlowDetailsButton) {
			this.backoffice_homepage_actions_CashFlowDetailsButton = backoffice_homepage_actions_CashFlowDetailsButton;
		}
		
		public String getBackoffice_homepage_actions_CashFlowSelectProduct() {
			return backoffice_homepage_actions_CashFlowSelectProduct;
		}

		public void setBackoffice_homepage_actions_CashFlowSelectProduct(
				String backoffice_homepage_actions_CashFlowSelectProduct) {
			this.backoffice_homepage_actions_CashFlowSelectProduct = backoffice_homepage_actions_CashFlowSelectProduct;
		}

		public String getBackoffice_homepage_actions_verifyCheckbox() {
			return backoffice_homepage_actions_verifyCheckbox;
		}

		public void setBackoffice_homepage_actions_verifyCheckbox(String backoffice_homepage_actions_verifyCheckbox) {
			this.backoffice_homepage_actions_verifyCheckbox = backoffice_homepage_actions_verifyCheckbox;
		}
		
		public String getBackoffice_homepage_actions_CashFlowVerifySubmit() {
			return backoffice_homepage_actions_CashFlowVerifySubmit;
		}

		public void setBackoffice_homepage_actions_CashFlowVerifySubmit(
				String backoffice_homepage_actions_CashFlowVerifySubmit) {
			this.backoffice_homepage_actions_CashFlowVerifySubmit = backoffice_homepage_actions_CashFlowVerifySubmit;
		}

		public String getBackoffice_homepage_actions_CashFlowCloseButton() {
			return backoffice_homepage_actions_CashFlowCloseButton;
		}

		public void setBackoffice_homepage_actions_CashFlowCloseButton(String backoffice_homepage_actions_CashFlowCloseButton) {
			this.backoffice_homepage_actions_CashFlowCloseButton = backoffice_homepage_actions_CashFlowCloseButton;
		}

		public String getBackoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox() {
			return backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox(
				String backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox) {
			this.backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox = backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_ProgramTextBox() {
			return backoffice_homepage_actions_OfferGen_ProgramTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_ProgramTextBox(
				String backoffice_homepage_actions_OfferGen_ProgramTextBox) {
			this.backoffice_homepage_actions_OfferGen_ProgramTextBox = backoffice_homepage_actions_OfferGen_ProgramTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_FactorTextBox() {
			return backoffice_homepage_actions_OfferGen_FactorTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_FactorTextBox(
				String backoffice_homepage_actions_OfferGen_FactorTextBox) {
			this.backoffice_homepage_actions_OfferGen_FactorTextBox = backoffice_homepage_actions_OfferGen_FactorTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_TermsTextBox() {
			return backoffice_homepage_actions_OfferGen_TermsTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_TermsTextBox(
				String backoffice_homepage_actions_OfferGen_TermsTextBox) {
			this.backoffice_homepage_actions_OfferGen_TermsTextBox = backoffice_homepage_actions_OfferGen_TermsTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_PaymentTextBox() {
			return backoffice_homepage_actions_OfferGen_PaymentTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_PaymentTextBox(
				String backoffice_homepage_actions_OfferGen_PaymentTextBox) {
			this.backoffice_homepage_actions_OfferGen_PaymentTextBox = backoffice_homepage_actions_OfferGen_PaymentTextBox;
		}		
		

		public String getBackoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox() {
			return backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox(
				String backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox) {
			this.backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox = backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_SubmitedDateTextBox() {
			return backoffice_homepage_actions_OfferGen_SubmitedDateTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_SubmitedDateTextBox(
				String backoffice_homepage_actions_OfferGen_SubmitedDateTextBox) {
			this.backoffice_homepage_actions_OfferGen_SubmitedDateTextBox = backoffice_homepage_actions_OfferGen_SubmitedDateTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_ApprovedAmtTextBox() {
			return backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_ApprovedAmtTextBox(
				String backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox) {
			this.backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox = backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_PSFpercentageTextBox() {
			return backoffice_homepage_actions_OfferGen_PSFpercentageTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_PSFpercentageTextBox(
				String backoffice_homepage_actions_OfferGen_PSFpercentageTextBox) {
			this.backoffice_homepage_actions_OfferGen_PSFpercentageTextBox = backoffice_homepage_actions_OfferGen_PSFpercentageTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_FunderFeeTextBox() {
			return backoffice_homepage_actions_OfferGen_FunderFeeTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_FunderFeeTextBox(
				String backoffice_homepage_actions_OfferGen_FunderFeeTextBox) {
			this.backoffice_homepage_actions_OfferGen_FunderFeeTextBox = backoffice_homepage_actions_OfferGen_FunderFeeTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_CompPercentageTextBox() {
			return backoffice_homepage_actions_OfferGen_CompPercentageTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_CompPercentageTextBox(
				String backoffice_homepage_actions_OfferGen_CompPercentageTextBox) {
			this.backoffice_homepage_actions_OfferGen_CompPercentageTextBox = backoffice_homepage_actions_OfferGen_CompPercentageTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox() {
			return backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox(
				String backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox) {
			this.backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox = backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox;
		}

		public String getBackoffice_homepage_actions_OfferSubmit() {
			return backoffice_homepage_actions_OfferSubmit;
		}

		public void setBackoffice_homepage_actions_OfferSubmit(String backoffice_homepage_actions_OfferSubmit) {
			this.backoffice_homepage_actions_OfferSubmit = backoffice_homepage_actions_OfferSubmit;
		}

		public String getBackoffice_homepage_actions_OfferGen_ProductTextBox() {
			return backoffice_homepage_actions_OfferGen_ProductTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_ProductTextBox(
				String backoffice_homepage_actions_OfferGen_ProductTextBox) {
			this.backoffice_homepage_actions_OfferGen_ProductTextBox = backoffice_homepage_actions_OfferGen_ProductTextBox;
		}

		public String getBackoffice_homepage_actions_OfferGen_CommentsTextBox() {
			return backoffice_homepage_actions_OfferGen_CommentsTextBox;
		}

		public void setBackoffice_homepage_actions_OfferGen_CommentsTextBox(
				String backoffice_homepage_actions_OfferGen_CommentsTextBox) {
			this.backoffice_homepage_actions_OfferGen_CommentsTextBox = backoffice_homepage_actions_OfferGen_CommentsTextBox;
		}
	
       public String getBackoffice_homepage_actions_UploadDocs() {
			return backoffice_homepage_actions_UploadDocs;
		}

		public void setBackoffice_homepage_actions_UploadDocs(String backoffice_homepage_actions_UploadDocs) {
			this.backoffice_homepage_actions_UploadDocs = backoffice_homepage_actions_UploadDocs;
		}

		public String getBackoffice_homepage_actions_PhotoIDUpload() {
			return backoffice_homepage_actions_PhotoIDUpload;
		}

		public void setBackoffice_homepage_actions_PhotoIDUpload(String backoffice_homepage_actions_PhotoIDUpload) {
			this.backoffice_homepage_actions_PhotoIDUpload = backoffice_homepage_actions_PhotoIDUpload;
		}

		public String getBackoffice_homepage_actions_OwnershipUpload() {
			return backoffice_homepage_actions_OwnershipUpload;
		}

		public void setBackoffice_homepage_actions_OwnershipUpload(String backoffice_homepage_actions_OwnershipUpload) {
			this.backoffice_homepage_actions_OwnershipUpload = backoffice_homepage_actions_OwnershipUpload;
		}

		public String getBackoffice_homepage_actions_ProofOfRentUpload() {
			return backoffice_homepage_actions_ProofOfRentUpload;
		}

		public void setBackoffice_homepage_actions_ProofOfRentUpload(String backoffice_homepage_actions_ProofOfRentUpload) {
			this.backoffice_homepage_actions_ProofOfRentUpload = backoffice_homepage_actions_ProofOfRentUpload;
		}

		public String getBackoffice_homepage_actions_VoidedCheckUpload() {
			return backoffice_homepage_actions_VoidedCheckUpload;
		}

		public void setBackoffice_homepage_actions_VoidedCheckUpload(String backoffice_homepage_actions_VoidedCheckUpload) {
			this.backoffice_homepage_actions_VoidedCheckUpload = backoffice_homepage_actions_VoidedCheckUpload;
		}
		
	   public String getBackoffice_homepage_actions_UploadDocsClose() {
	 		return backoffice_homepage_actions_UploadDocsClose;
		}

		public void setBackoffice_homepage_actions_UploadDocsClose(String backoffice_homepage_actions_UploadDocsClose) {
			this.backoffice_homepage_actions_UploadDocsClose = backoffice_homepage_actions_UploadDocsClose;
		}

	    public String getBackoffice_homepage_actions_BankDetails() {
			return backoffice_homepage_actions_BankDetails;
		}

		public void setBackoffice_homepage_actions_BankDetails(String backoffice_homepage_actions_BankDetails) {
			this.backoffice_homepage_actions_BankDetails = backoffice_homepage_actions_BankDetails;
		}

		public String getBackoffice_homepage_actions_BankAccNumTextBox() {
			return backoffice_homepage_actions_BankAccNumTextBox;
		}

		public void setBackoffice_homepage_actions_BankAccNumTextBox(String backoffice_homepage_actions_BankAccNumTextBox) {
			this.backoffice_homepage_actions_BankAccNumTextBox = backoffice_homepage_actions_BankAccNumTextBox;
		}

		public String getBackoffice_homepage_actions_BankverifyCheckbox() {
			return backoffice_homepage_actions_BankverifyCheckbox;
		}

		public void setBackoffice_homepage_actions_BankverifyCheckbox(String backoffice_homepage_actions_BankverifyCheckbox) {
			this.backoffice_homepage_actions_BankverifyCheckbox = backoffice_homepage_actions_BankverifyCheckbox;
		}

		public String getBackoffice_homepage_actions_BankVerifyButton() {
			return backoffice_homepage_actions_BankVerifyButton;
		}

		public void setBackoffice_homepage_actions_BankVerifyButton(String backoffice_homepage_actions_BankVerifyButton) {
			this.backoffice_homepage_actions_BankVerifyButton = backoffice_homepage_actions_BankVerifyButton;
		}
		
	    public String getBackoffice_homepage_actions_BankRoutingNumTextBox() {
			return backoffice_homepage_actions_BankRoutingNumTextBox;
		}

		public void setBackoffice_homepage_actions_BankRoutingNumTextBox(
				String backoffice_homepage_actions_BankRoutingNumTextBox) {
			this.backoffice_homepage_actions_BankRoutingNumTextBox = backoffice_homepage_actions_BankRoutingNumTextBox;
		}

	    public String getBackoffice_homepage_actions_BankVerifyCloseButton() {
			return backoffice_homepage_actions_BankVerifyCloseButton;
		}

		public void setBackoffice_homepage_actions_BankVerifyCloseButton(
				String backoffice_homepage_actions_BankVerifyCloseButton) {
			this.backoffice_homepage_actions_BankVerifyCloseButton = backoffice_homepage_actions_BankVerifyCloseButton;
		}

	    public String getBackoffice_homepage_actions_IDVerifyDetails() {
			return backoffice_homepage_actions_IDVerifyDetails;
		}

		public void setBackoffice_homepage_actions_IDVerifyDetails(String backoffice_homepage_actions_IDVerifyDetails) {
			this.backoffice_homepage_actions_IDVerifyDetails = backoffice_homepage_actions_IDVerifyDetails;
		}

		public String getBackoffice_homepage_actions_IDverifyCheckbox1() {
			return backoffice_homepage_actions_IDverifyCheckbox1;
		}

		public void setBackoffice_homepage_actions_IDverifyCheckbox1(String backoffice_homepage_actions_IDverifyCheckbox1) {
			this.backoffice_homepage_actions_IDverifyCheckbox1 = backoffice_homepage_actions_IDverifyCheckbox1;
		}

		public String getBackoffice_homepage_actions_IDverifyCheckbox2() {
			return backoffice_homepage_actions_IDverifyCheckbox2;
		}

		public void setBackoffice_homepage_actions_IDverifyCheckbox2(String backoffice_homepage_actions_IDverifyCheckbox2) {
			this.backoffice_homepage_actions_IDverifyCheckbox2 = backoffice_homepage_actions_IDverifyCheckbox2;
		}

		public String getBackoffice_homepage_actions_IDverifyCheckbox3() {
			return backoffice_homepage_actions_IDverifyCheckbox3;
		}

		public void setBackoffice_homepage_actions_IDverifyCheckbox3(String backoffice_homepage_actions_IDverifyCheckbox3) {
			this.backoffice_homepage_actions_IDverifyCheckbox3 = backoffice_homepage_actions_IDverifyCheckbox3;
		}

		public String getBackoffice_homepage_actions_IDVerifyButton() {
			return backoffice_homepage_actions_IDVerifyButton;
		}

		public void setBackoffice_homepage_actions_IDVerifyButton(String backoffice_homepage_actions_IDVerifyButton) {
			this.backoffice_homepage_actions_IDVerifyButton = backoffice_homepage_actions_IDVerifyButton;
		}

		public String getBackoffice_homepage_actions_IDVerifyCloseButton() {
			return backoffice_homepage_actions_IDVerifyCloseButton;
		}

		public void setBackoffice_homepage_actions_IDVerifyCloseButton(String backoffice_homepage_actions_IDVerifyCloseButton) {
			this.backoffice_homepage_actions_IDVerifyCloseButton = backoffice_homepage_actions_IDVerifyCloseButton;
		}
		
	   public String getBackoffice_homepage_actions_BusinessOwnerVerifyDetails() {
			return backoffice_homepage_actions_BusinessOwnerVerifyDetails;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerVerifyDetails(
				String backoffice_homepage_actions_BusinessOwnerVerifyDetails) {
			this.backoffice_homepage_actions_BusinessOwnerVerifyDetails = backoffice_homepage_actions_BusinessOwnerVerifyDetails;
		}

		public String getBackoffice_homepage_actions_BusinessOwnerverifyCheckbox1() {
			return backoffice_homepage_actions_BusinessOwnerverifyCheckbox1;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerverifyCheckbox1(
				String backoffice_homepage_actions_BusinessOwnerverifyCheckbox1) {
			this.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1 = backoffice_homepage_actions_BusinessOwnerverifyCheckbox1;
		}

		public String getBackoffice_homepage_actions_BusinessOwnerVerifyButton() {
			return backoffice_homepage_actions_BusinessOwnerVerifyButton;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerVerifyButton(
				String backoffice_homepage_actions_BusinessOwnerVerifyButton) {
			this.backoffice_homepage_actions_BusinessOwnerVerifyButton = backoffice_homepage_actions_BusinessOwnerVerifyButton;
		}

		public String getBackoffice_homepage_actions_BusinessOwnerVerifyCloseButton() {
			return backoffice_homepage_actions_BusinessOwnerVerifyCloseButton;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerVerifyCloseButton(
				String backoffice_homepage_actions_BusinessOwnerVerifyCloseButton) {
			this.backoffice_homepage_actions_BusinessOwnerVerifyCloseButton = backoffice_homepage_actions_BusinessOwnerVerifyCloseButton;
		}
		
	    public String getBackoffice_homepage_actions_RentDetails() {
			return backoffice_homepage_actions_RentDetails;
		}

		public void setBackoffice_homepage_actions_RentDetails(String backoffice_homepage_actions_RentDetails) {
			this.backoffice_homepage_actions_RentDetails = backoffice_homepage_actions_RentDetails;
		}

		public String getBackoffice_homepage_actions_OwnerPhoneNumTextBox() {
			return backoffice_homepage_actions_OwnerPhoneNumTextBox;
		}

		public void setBackoffice_homepage_actions_OwnerPhoneNumTextBox(
				String backoffice_homepage_actions_OwnerPhoneNumTextBox) {
			this.backoffice_homepage_actions_OwnerPhoneNumTextBox = backoffice_homepage_actions_OwnerPhoneNumTextBox;
		}

		public String getBackoffice_homepage_actions_OwnerNameTextBox() {
			return backoffice_homepage_actions_OwnerNameTextBox;
		}

		public void setBackoffice_homepage_actions_OwnerNameTextBox(String backoffice_homepage_actions_OwnerNameTextBox) {
			this.backoffice_homepage_actions_OwnerNameTextBox = backoffice_homepage_actions_OwnerNameTextBox;
		}

		public String getBackoffice_homepage_actions_OwnerVerifyButton() {
			return backoffice_homepage_actions_OwnerVerifyButton;
		}

		public void setBackoffice_homepage_actions_OwnerVerifyButton(String backoffice_homepage_actions_OwnerVerifyButton) {
			this.backoffice_homepage_actions_OwnerVerifyButton = backoffice_homepage_actions_OwnerVerifyButton;
		}

		public String getBackoffice_homepage_actions_OwnerVerifyCloseButton() {
			return backoffice_homepage_actions_OwnerVerifyCloseButton;
		}

		public void setBackoffice_homepage_actions_OwnerVerifyCloseButton(
				String backoffice_homepage_actions_OwnerVerifyCloseButton) {
			this.backoffice_homepage_actions_OwnerVerifyCloseButton = backoffice_homepage_actions_OwnerVerifyCloseButton;
		}

	    public String getBackoffice_homepage_actions_GenerateAgreement() {
			return backoffice_homepage_actions_GenerateAgreement;
		}

		public void setBackoffice_homepage_actions_GenerateAgreement(String backoffice_homepage_actions_GenerateAgreement) {
			this.backoffice_homepage_actions_GenerateAgreement = backoffice_homepage_actions_GenerateAgreement;
		}

		public String getBackoffice_homepage_actions_GenAgreePrepayCheck() {
			return backoffice_homepage_actions_GenAgreePrepayCheck;
		}

		public void setBackoffice_homepage_actions_GenAgreePrepayCheck(String backoffice_homepage_actions_GenAgreePrepayCheck) {
			this.backoffice_homepage_actions_GenAgreePrepayCheck = backoffice_homepage_actions_GenAgreePrepayCheck;
		}

		public String getBackoffice_homepage_actions_GenAgreeFirstRepurchasePrice() {
			return backoffice_homepage_actions_GenAgreeFirstRepurchasePrice;
		}

		public void setBackoffice_homepage_actions_GenAgreeFirstRepurchasePrice(
				String backoffice_homepage_actions_GenAgreeFirstRepurchasePrice) {
			this.backoffice_homepage_actions_GenAgreeFirstRepurchasePrice = backoffice_homepage_actions_GenAgreeFirstRepurchasePrice;
		}

		public String getBackoffice_homepage_actions_GenAgreeSecondRepurchasePrice() {
			return backoffice_homepage_actions_GenAgreeSecondRepurchasePrice;
		}

		public void setBackoffice_homepage_actions_GenAgreeSecondRepurchasePrice(
				String backoffice_homepage_actions_GenAgreeSecondRepurchasePrice) {
			this.backoffice_homepage_actions_GenAgreeSecondRepurchasePrice = backoffice_homepage_actions_GenAgreeSecondRepurchasePrice;
		}

		public String getBackoffice_homepage_actions_GenAgreeThirdRepurchasePrice() {
			return backoffice_homepage_actions_GenAgreeThirdRepurchasePrice;
		}

		public void setBackoffice_homepage_actions_GenAgreeThirdRepurchasePrice(
				String backoffice_homepage_actions_GenAgreeThirdRepurchasePrice) {
			this.backoffice_homepage_actions_GenAgreeThirdRepurchasePrice = backoffice_homepage_actions_GenAgreeThirdRepurchasePrice;
		}

		public String getBackoffice_homepage_actions_GenAgreeFourthRepurchasePrice() {
			return backoffice_homepage_actions_GenAgreeFourthRepurchasePrice;
		}

		public void setBackoffice_homepage_actions_GenAgreeFourthRepurchasePrice(
				String backoffice_homepage_actions_GenAgreeFourthRepurchasePrice) {
			this.backoffice_homepage_actions_GenAgreeFourthRepurchasePrice = backoffice_homepage_actions_GenAgreeFourthRepurchasePrice;
		}

		public String getBackoffice_homepage_actions_GenAgreeFifthRepurchasePrice() {
			return backoffice_homepage_actions_GenAgreeFifthRepurchasePrice;
		}

		public void setBackoffice_homepage_actions_GenAgreeFifthRepurchasePrice(
				String backoffice_homepage_actions_GenAgreeFifthRepurchasePrice) {
			this.backoffice_homepage_actions_GenAgreeFifthRepurchasePrice = backoffice_homepage_actions_GenAgreeFifthRepurchasePrice;
		}

		public String getBackoffice_homepage_actions_GenAgreeNoStackCheck() {
			return backoffice_homepage_actions_GenAgreeNoStackCheck;
		}

		public void setBackoffice_homepage_actions_GenAgreeNoStackCheck(
				String backoffice_homepage_actions_GenAgreeNoStackCheck) {
			this.backoffice_homepage_actions_GenAgreeNoStackCheck = backoffice_homepage_actions_GenAgreeNoStackCheck;
		}

		public String getBackoffice_homepage_GenAgreeButton() {
			return backoffice_homepage_GenAgreeButton;
		}

		public void setBackoffice_homepage_GenAgreeButton(String backoffice_homepage_GenAgreeButton) {
			this.backoffice_homepage_GenAgreeButton = backoffice_homepage_GenAgreeButton;
		}
		
	    public String getBackoffice_homepage_actions_SendAgreement() {
			return backoffice_homepage_actions_SendAgreement;
		}

		public void setBackoffice_homepage_actions_SendAgreement(String backoffice_homepage_actions_SendAgreement) {
			this.backoffice_homepage_actions_SendAgreement = backoffice_homepage_actions_SendAgreement;
		}

		public String getBackoffice_homepage_actions_VerifySendAgreement() {
			return backoffice_homepage_actions_VerifySendAgreement;
		}

		public void setBackoffice_homepage_actions_VerifySendAgreement(String backoffice_homepage_actions_VerifySendAgreement) {
			this.backoffice_homepage_actions_VerifySendAgreement = backoffice_homepage_actions_VerifySendAgreement;
		}
	   public String getBackoffice_homepage_actions_SignedAgreementUpload() {
			return backoffice_homepage_actions_SignedAgreementUpload;
		}

		public void setBackoffice_homepage_actions_SignedAgreementUpload(
				String backoffice_homepage_actions_SignedAgreementUpload) {
			this.backoffice_homepage_actions_SignedAgreementUpload = backoffice_homepage_actions_SignedAgreementUpload;
		}		

	    public String getBackoffice_homepage_actions_ContractVerifyDetails() {
			return backoffice_homepage_actions_ContractVerifyDetails;
		}

		public void setBackoffice_homepage_actions_ContractVerifyDetails(
				String backoffice_homepage_actions_ContractVerifyDetails) {
			this.backoffice_homepage_actions_ContractVerifyDetails = backoffice_homepage_actions_ContractVerifyDetails;
		}

		public String getBackoffice_homepage_actions_ContractOwnerverifyCheckbox1() {
			return backoffice_homepage_actions_ContractOwnerverifyCheckbox1;
		}

		public void setBackoffice_homepage_actions_ContractOwnerverifyCheckbox1(
				String backoffice_homepage_actions_ContractOwnerverifyCheckbox1) {
			this.backoffice_homepage_actions_ContractOwnerverifyCheckbox1 = backoffice_homepage_actions_ContractOwnerverifyCheckbox1;
		}

		public String getBackoffice_homepage_actions_ContractOwnerVerifyButton() {
			return backoffice_homepage_actions_ContractOwnerVerifyButton;
		}

		public void setBackoffice_homepage_actions_ContractOwnerVerifyButton(
				String backoffice_homepage_actions_ContractOwnerVerifyButton) {
			this.backoffice_homepage_actions_ContractOwnerVerifyButton = backoffice_homepage_actions_ContractOwnerVerifyButton;
		}

		public String getBackoffice_homepage_actions_ContractOwnerCloseButton() {
			return backoffice_homepage_actions_ContractOwnerCloseButton;
		}

		public void setBackoffice_homepage_actions_ContractOwnerCloseButton(
				String backoffice_homepage_actions_ContractOwnerCloseButton) {
			this.backoffice_homepage_actions_ContractOwnerCloseButton = backoffice_homepage_actions_ContractOwnerCloseButton;
		}
		
	    public String getBackoffice_homepage_actions_FundingVerifyDetails() {
			return backoffice_homepage_actions_FundingVerifyDetails;
		}

		public void setBackoffice_homepage_actions_FundingVerifyDetails(
				String backoffice_homepage_actions_FundingVerifyDetails) {
			this.backoffice_homepage_actions_FundingVerifyDetails = backoffice_homepage_actions_FundingVerifyDetails;
		}

		public String getBackoffice_homepage_actions_FundingOwnerverifyCheckbox1() {
			return backoffice_homepage_actions_FundingOwnerverifyCheckbox1;
		}

		public void setBackoffice_homepage_actions_FundingOwnerverifyCheckbox1(
				String backoffice_homepage_actions_FundingOwnerverifyCheckbox1) {
			this.backoffice_homepage_actions_FundingOwnerverifyCheckbox1 = backoffice_homepage_actions_FundingOwnerverifyCheckbox1;
		}

		public String getBackoffice_homepage_actions_FundingOwnerVerifyButton() {
			return backoffice_homepage_actions_FundingOwnerVerifyButton;
		}

		public void setBackoffice_homepage_actions_FundingOwnerVerifyButton(
				String backoffice_homepage_actions_FundingOwnerVerifyButton) {
			this.backoffice_homepage_actions_FundingOwnerVerifyButton = backoffice_homepage_actions_FundingOwnerVerifyButton;
		}

		public String getBackoffice_homepage_actions_FundingOwnerCloseButton() {
			return backoffice_homepage_actions_FundingOwnerCloseButton;
		}

		public void setBackoffice_homepage_actions_FundingOwnerCloseButton(
				String backoffice_homepage_actions_FundingOwnerCloseButton) {
			this.backoffice_homepage_actions_FundingOwnerCloseButton = backoffice_homepage_actions_FundingOwnerCloseButton;
		}

	    public String getBackoffice_searchpage_searchMenu() {
			return backoffice_searchpage_searchMenu;
		}

		public void setBackoffice_searchpage_searchMenu(String backoffice_searchpage_searchMenu) {
			this.backoffice_searchpage_searchMenu = backoffice_searchpage_searchMenu;
		}

	    public String getBackoffice_homepage_actions_IDVerifyDetailsLoc() {
			return backoffice_homepage_actions_IDVerifyDetailsLoc;
		}

		public void setBackoffice_homepage_actions_IDVerifyDetailsLoc(String backoffice_homepage_actions_IDVerifyDetailsLoc) {
			this.backoffice_homepage_actions_IDVerifyDetailsLoc = backoffice_homepage_actions_IDVerifyDetailsLoc;
		}

		public String getBackoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc() {
			return backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc(
				String backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc) {
			this.backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc = backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc;
		}

		public String getBackoffice_homepage_actions_RentDetailsLoc() {
			return backoffice_homepage_actions_RentDetailsLoc;
		}

		public void setBackoffice_homepage_actions_RentDetailsLoc(String backoffice_homepage_actions_RentDetailsLoc) {
			this.backoffice_homepage_actions_RentDetailsLoc = backoffice_homepage_actions_RentDetailsLoc;
		}

		public String getBackoffice_homepage_actions_BankDetailsLoc() {
			return backoffice_homepage_actions_BankDetailsLoc;
		}

		public void setBackoffice_homepage_actions_BankDetailsLoc(String backoffice_homepage_actions_BankDetailsLoc) {
			this.backoffice_homepage_actions_BankDetailsLoc = backoffice_homepage_actions_BankDetailsLoc;
		}

		public String getBackoffice_homepage_actions_FundingVerifyDetailsLoc() {
			return backoffice_homepage_actions_FundingVerifyDetailsLoc;
		}

		public void setBackoffice_homepage_actions_FundingVerifyDetailsLoc(
				String backoffice_homepage_actions_FundingVerifyDetailsLoc) {
			this.backoffice_homepage_actions_FundingVerifyDetailsLoc = backoffice_homepage_actions_FundingVerifyDetailsLoc;
		}

		public String getBackoffice_homepage_actions_ContractVerifyDetailsLoc() {
			return backoffice_homepage_actions_ContractVerifyDetailsLoc;
		}

		public void setBackoffice_homepage_actions_ContractVerifyDetailsLoc(
				String backoffice_homepage_actions_ContractVerifyDetailsLoc) {
			this.backoffice_homepage_actions_ContractVerifyDetailsLoc = backoffice_homepage_actions_ContractVerifyDetailsLoc;
		}

	    public String getBackoffice_homepage_actions_BankVerifyCloseButtonLoc() {
			return backoffice_homepage_actions_BankVerifyCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_BankVerifyCloseButtonLoc(
				String backoffice_homepage_actions_BankVerifyCloseButtonLoc) {
			this.backoffice_homepage_actions_BankVerifyCloseButtonLoc = backoffice_homepage_actions_BankVerifyCloseButtonLoc;
		}

		public String getBackoffice_homepage_actions_IDVerifyCloseButtonLoc() {
			return backoffice_homepage_actions_IDVerifyCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_IDVerifyCloseButtonLoc(
				String backoffice_homepage_actions_IDVerifyCloseButtonLoc) {
			this.backoffice_homepage_actions_IDVerifyCloseButtonLoc = backoffice_homepage_actions_IDVerifyCloseButtonLoc;
		}

		public String getBackoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc() {
			return backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc(
				String backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc) {
			this.backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc = backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc;
		}

		public String getBackoffice_homepage_actions_OwnerVerifyCloseButtonLoc() {
			return backoffice_homepage_actions_OwnerVerifyCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_OwnerVerifyCloseButtonLoc(
				String backoffice_homepage_actions_OwnerVerifyCloseButtonLoc) {
			this.backoffice_homepage_actions_OwnerVerifyCloseButtonLoc = backoffice_homepage_actions_OwnerVerifyCloseButtonLoc;
		}

		public String getBackoffice_homepage_actions_ContractOwnerCloseButtonLoc() {
			return backoffice_homepage_actions_ContractOwnerCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_ContractOwnerCloseButtonLoc(
				String backoffice_homepage_actions_ContractOwnerCloseButtonLoc) {
			this.backoffice_homepage_actions_ContractOwnerCloseButtonLoc = backoffice_homepage_actions_ContractOwnerCloseButtonLoc;
		}

		public String getBackoffice_homepage_actions_FundingOwnerCloseButtonLoc() {
			return backoffice_homepage_actions_FundingOwnerCloseButtonLoc;
		}

		public void setBackoffice_homepage_actions_FundingOwnerCloseButtonLoc(
				String backoffice_homepage_actions_FundingOwnerCloseButtonLoc) {
			this.backoffice_homepage_actions_FundingOwnerCloseButtonLoc = backoffice_homepage_actions_FundingOwnerCloseButtonLoc;
		}
		
	    public String getBackoffice_homepage_actions_FundingOwnerInitiateButton() {
			return backoffice_homepage_actions_FundingOwnerInitiateButton;
		}

		public void setBackoffice_homepage_actions_FundingOwnerInitiateButton(
				String backoffice_homepage_actions_FundingOwnerInitiateButton) {
			this.backoffice_homepage_actions_FundingOwnerInitiateButton = backoffice_homepage_actions_FundingOwnerInitiateButton;
		}
		
	    public String getBackoffice_homepage_actions_applicationDataVerification() {
			return backoffice_homepage_actions_applicationDataVerification;
		}

		public void setBackoffice_homepage_actions_applicationDataVerification(
				String backoffice_homepage_actions_applicationDataVerification) {
			this.backoffice_homepage_actions_applicationDataVerification = backoffice_homepage_actions_applicationDataVerification;
		}

		public String getBackoffice_homepage_actions_applicationDataCheck() {
			return backoffice_homepage_actions_applicationDataCheck;
		}

		public void setBackoffice_homepage_actions_applicationDataCheck(
				String backoffice_homepage_actions_applicationDataCheck) {
			this.backoffice_homepage_actions_applicationDataCheck = backoffice_homepage_actions_applicationDataCheck;
		}

		public String getBackoffice_homepage_actions_applicationDataVerifyButton() {
			return backoffice_homepage_actions_applicationDataVerifyButton;
		}

		public void setBackoffice_homepage_actions_applicationDataVerifyButton(
				String backoffice_homepage_actions_applicationDataVerifyButton) {
			this.backoffice_homepage_actions_applicationDataVerifyButton = backoffice_homepage_actions_applicationDataVerifyButton;
		}
        
	    public String getBackoffice_homepage_actions_businessSocialVerification() {
			return backoffice_homepage_actions_businessSocialVerification;
		}

		public void setBackoffice_homepage_actions_businessSocialVerification(
				String backoffice_homepage_actions_businessSocialVerification) {
			this.backoffice_homepage_actions_businessSocialVerification = backoffice_homepage_actions_businessSocialVerification;
		}

		public String getBackoffice_homepage_actions_businessSocialVerifyCheck() {
			return backoffice_homepage_actions_businessSocialVerifyCheck;
		}

		public void setBackoffice_homepage_actions_businessSocialVerifyCheck(
				String backoffice_homepage_actions_businessSocialVerifyCheck) {
			this.backoffice_homepage_actions_businessSocialVerifyCheck = backoffice_homepage_actions_businessSocialVerifyCheck;
		}

		public String getBackoffice_homepage_actions_businessSocialVerifyButton() {
			return backoffice_homepage_actions_businessSocialVerifyButton;
		}

		public void setBackoffice_homepage_actions_businessSocialVerifyButton(
				String backoffice_homepage_actions_businessSocialVerifyButton) {
			this.backoffice_homepage_actions_businessSocialVerifyButton = backoffice_homepage_actions_businessSocialVerifyButton;
		}
		
		public String getBackoffice_homepage_actions_ApplicationStatus() {
			return backoffice_homepage_actions_ApplicationStatus;
		}

		public void setBackoffice_homepage_actions_ApplicationStatus(String backoffice_homepage_actions_ApplicationStatus) {
			this.backoffice_homepage_actions_ApplicationStatus = backoffice_homepage_actions_ApplicationStatus;
		}

    	public String getBackoffice_homepage_actions_decideProduct() {
			return backoffice_homepage_actions_decideProduct;
		}

		public void setBackoffice_homepage_actions_decideProduct(String backoffice_homepage_actions_decideProduct) {
			this.backoffice_homepage_actions_decideProduct = backoffice_homepage_actions_decideProduct;
		}

		public String getBackoffice_homepage_actions_moveProduct() {
			return backoffice_homepage_actions_moveProduct;
		}

		public void setBackoffice_homepage_actions_moveProduct(String backoffice_homepage_actions_moveProduct) {
			this.backoffice_homepage_actions_moveProduct = backoffice_homepage_actions_moveProduct;
		}

		public String getBackoffice_homepage_actions_selectListproduct() {
			return backoffice_homepage_actions_selectListproduct;
		}

		public void setBackoffice_homepage_actions_selectListproduct(String backoffice_homepage_actions_selectListproduct) {
			this.backoffice_homepage_actions_selectListproduct = backoffice_homepage_actions_selectListproduct;
		}


		
		


	@Value(value = "${homepage.yourBusinessTab.bstYearDropdown}")
    private String homepage_yourBusinessTab_bstYearDropdown;

    public String getHomepage_yourBusinessTab_bstYearDropdown() {
        return homepage_yourBusinessTab_bstYearDropdown;
    }

    public void setHomepage_yourBusinessTab_bstYearDropdown(String homepage_yourBusinessTab_bstYearDropdown) {
        this.homepage_yourBusinessTab_bstYearDropdown = homepage_yourBusinessTab_bstYearDropdown;
    }
    
    public String getBackoffice_homepage_actions_uploadBankStatementButton() {
		return backoffice_homepage_actions_uploadBankStatementButton;
	}

	public void setBackoffice_homepage_actions_uploadBankStatementButton(
			String backoffice_homepage_actions_uploadBankStatementButton) {
		this.backoffice_homepage_actions_uploadBankStatementButton = backoffice_homepage_actions_uploadBankStatementButton;
	}
    

    @Value(value = "${homepage.yourBusinessTab.businessWebsiteTextBox}")
    private String homepage_yourBusinessTab_businessWebsiteTextBox;

    @Value(value = "${homepage.yourBusinessTab.legalEntityDropdown}")
    private String homepage_yourBusinessTab_legalEntityDropdown;

    @Value(value = "${homepage.yourBusinessTab.businessLocationDropdown}")
    private String homepage_yourBusinessTab_businessLocationDropdown;

    @Value(value = "${homepage.yourBusinessTab.federalSalesTaxTextBox}")
    private String homepage_yourBusinessTab_federalSalesTaxTextBox;

    @Value(value = "${homepage.yourBusinessTab.industryDropdown}")
    private String homepage_yourBusinessTab_industryDropdown;

    @Value(value = "${homepage.yourBusinessTab.importanceDropdown}")
    private String homepage_yourBusinessTab_importanceDropdown;

    @Value(value = "${homepage.yourBusinessTab.continueButton}")
    private String homepage_yourBusinessTab_continueButton;

    @Value(value = "${homepage.aboutYouTab.getToKnowLabel}")
    private String homepage_aboutYouTab_getToKnowLabel;

    @Value(value = "${homepage.aboutYouTab.emailAddressTextBox}")
    private String homepage_aboutYouTab_emailAddressTextBox;

    @Value(value = "${homepage.aboutYouTab.firstNameTextBox}")
    private String homepage_aboutYouTab_firstNameTextBox;

    @Value(value = "${homepage.aboutYouTab.lastNameTextBox}")
    private String homepage_aboutYouTab_lastNameTextBox;

    @Value(value = "${homepage.aboutYouTab.addressTextBox}")
    private String homepage_aboutYouTab_addressTextBox;

    @Value(value = "${homepage.aboutYouTab.homeCityTextBox}")
    private String homepage_aboutYouTab_homeCityTextBox;

    @Value(value = "${homepage.aboutYouTab.homeStateDropdown}")
    private String homepage_aboutYouTab_homeStateDropdown;

    @Value(value = "${homepage.aboutYouTab.zipCodeTextBox}")
    private String homepage_aboutYouTab_zipCodeTextBox;

    @Value(value = "${homepage.aboutYouTab.phoneNumberTextBox}")
    private String homepage_aboutYouTab_phoneNumberTextBox;

    @Value(value = "${homepage.aboutYouTab.mobileNumberTextBox}")
    private String homepage_aboutYouTab_mobileNumberTextBox;

    @Value(value = "${homepage.aboutYouTab.ownershipTextBox}")
    private String homepage_aboutYouTab_ownershipTextBox;

    @Value(value = "${homepage.aboutYouTab.ssnTextBox}")
    private String homepage_aboutYouTab_ssnTextBox;

    @Value(value = "${homepage.aboutYouTab.mobDropdown}")
    private String homepage_aboutYouTab_mobDropdown;

    @Value(value = "${homepage.aboutYouTab.dobDropdown}")
    private String homepage_aboutYouTab_dobDropdown;

    @Value(value = "${homepage.aboutYouTab.yobDropdown}")
    private String homepage_aboutYouTab_yobDropdown;

    @Value(value = "${homepage.aboutYouTab.AcceptTC}")
    private String homepage_aboutYouTab_AcceptTC;
    
    @Value(value = "${homepage.aboutYouTab.continueButton}")
    private String homepage_aboutYouTab_continueButton;

    @Value(value = "${homepage.financialReviewTab.selectOptionsLabel}")
    private String homepage_financialReviewTab_selectOptionsLabel;

    @Value(value = "${homepage.financialReviewTab.linkBankAccountButton}")
    private String homepage_financialReviewTab_linkBankAccountButton;

    @Value(value = "${homepage.financialReviewTab.skipButton}")
    private String homepage_financialReviewTab_skipButton;

    @Value(value = "${selectBank.selectBankLabel}")
    private String selectBank_selectBankLabel;

    @Value(value = "${selectBank.searchBankTextBox}")
    private String selectBank_searchBankTextBox;

    @Value(value = "${selectBank.selectedBank}")
    private String selectBank_selectedBank;

    @Value(value = "${selectBank.plaidUsernameTextBox}")
    private String selectBank_plaidUsernameTextBox;

    @Value(value = "${selectBank.plaidPasswordTextBox}")
    private String selectBank_plaidPasswordTextBox;

    @Value(value = "${selectBank.submitPlaidButton}")
    private String selectBank_submitPlaidButton;

    @Value(value = "${selectBank.resultantBank}")
    private String selectBank_resultantBank;

    @Value(value = "${selectBank.selectYourAccountLabel}")
    private String selectBank_selectYourAccountLabel;

    @Value(value = "${selectBank.continueButton}")
    private String selectBank_continueButton;

    @Value(value = "${selectBank.successfulConnectionLabel}")
    private String selectBank_successfulConnectionLabel;

    @Value(value = "${homepage.financialReviewTab.bankLinkingSuccessfulMessageLabel}")
    private String homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel;

    @Value(value = "${homepage.financialReviewTab.finishButton}")
    private String homepage_financialReviewTab_finishButton;

    @Value(value = "${homepage.financialReviewTab.reviewDocusignDocLabel}")
    private String homepage_financialReviewTab_reviewDocusignDocLabel;

    @Value(value = "${homepage.financialReviewTab.agreementCheckBox}")
    private String homepage_financialReviewTab_agreementCheckBox;

    @Value(value = "${homepage.financialReviewTab.continueButton}")
    private String homepage_financialReviewTab_continueButton;

    @Value(value = "${dashboard.profileDropdown}")
    private String dashboard_profileDropdown;

    @Value(value = "${dashboard.logoutLink}")
    private String dashboard_logoutLink;

    @Value(value = "${backoffice.loginPage.usernameTextBox}")
    private String backoffice_loginPage_usernameTextBox;

    @Value(value = "${backoffice.loginPage.passwordTextBox}")
    private String backoffice_loginPage_passwordTextBox;

    @Value(value = "${backoffice.loginPage.agreementCheckbox}")
    private String backoffice_loginPage_agreementCheckbox;

    @Value(value = "${backoffice.loginPage.forgetPasswordLink}")
    private String backoffice_loginPage_forgetPasswordLink;

    @Value(value = "${backoffice.loginPage.loginButton}")
    private String backoffice_loginPage_loginButton;

    @Value(value = "${backoffice.header.profileDropdown}")
    private String backoffice_header_profileDropdown;

    @Value(value = "${backoffice.header.logoutLink}")
    private String backoffice_header_logoutLink;

    @Value(value = "${backoffice.searchpage.productIdTextBox}")
    private String backoffice_searchpage_productIdTextBox;

    @Value(value = "${backoffice.searchpage.applicationNumberTextBox}")
    private String backoffice_searchpage_applicationNumberTextBox;

    @Value(value = "${backoffice.searchpage.searchButton}")
    private String backoffice_searchpage_searchButton;
    
    @Value(value = "${backoffice.homepage.actionsButton}")
    private String backoffice_homepage_actionsButton;

   	@Value(value = "${homepage.financialReviewTab.completeApplicationButton}")
    private String homepage_financialReviewTab_completeApplicationButton;

    
   	
    public String getBackoffice_homepage_actionsButton() {
		return backoffice_homepage_actionsButton;
	}

	public void setBackoffice_homepage_actionsButton(String backoffice_homepage_actionsButton) {
		this.backoffice_homepage_actionsButton = backoffice_homepage_actionsButton;
	}
   	
   	public String getHomepage_financialReviewTab_completeApplicationButton() {
        return homepage_financialReviewTab_completeApplicationButton;
    }

    public void setHomepage_financialReviewTab_completeApplicationButton(String homepage_financialReviewTab_completeApplicationButton) {
        this.homepage_financialReviewTab_completeApplicationButton = homepage_financialReviewTab_completeApplicationButton;
    }

    public String getBackoffice_searchpage_searchButton() {
        return backoffice_searchpage_searchButton;
    }

    public void setBackoffice_searchpage_searchButton(String backoffice_searchpage_searchButton) {
        this.backoffice_searchpage_searchButton = backoffice_searchpage_searchButton;
    }

    public String getBackoffice_searchpage_productIdTextBox() {
        return backoffice_searchpage_productIdTextBox;
    }

    public void setBackoffice_searchpage_productIdTextBox(String backoffice_searchpage_productIdTextBox) {
        this.backoffice_searchpage_productIdTextBox = backoffice_searchpage_productIdTextBox;
    }

    public String getBackoffice_searchpage_applicationNumberTextBox() {
        return backoffice_searchpage_applicationNumberTextBox;
    }

    public void setBackoffice_searchpage_applicationNumberTextBox(String backoffice_searchpage_applicationNumberTextBox) {
        this.backoffice_searchpage_applicationNumberTextBox = backoffice_searchpage_applicationNumberTextBox;
    }

    public String getBackoffice_header_profileDropdown() {
        return backoffice_header_profileDropdown;
    }

    public void setBackoffice_header_profileDropdown(String backoffice_header_profileDropdown) {
        this.backoffice_header_profileDropdown = backoffice_header_profileDropdown;
    }

    public String getBackoffice_header_logoutLink() {
        return backoffice_header_logoutLink;
    }

    public void setBackoffice_header_logoutLink(String backoffice_header_logoutLink) {
        this.backoffice_header_logoutLink = backoffice_header_logoutLink;
    }

    public String getBackoffice_loginPage_usernameTextBox() {
        return backoffice_loginPage_usernameTextBox;
    }

    public void setBackoffice_loginPage_usernameTextBox(String backoffice_loginPage_usernameTextBox) {
        this.backoffice_loginPage_usernameTextBox = backoffice_loginPage_usernameTextBox;
    }

    public String getBackoffice_loginPage_passwordTextBox() {
        return backoffice_loginPage_passwordTextBox;
    }

    public void setBackoffice_loginPage_passwordTextBox(String backoffice_loginPage_passwordTextBox) {
        this.backoffice_loginPage_passwordTextBox = backoffice_loginPage_passwordTextBox;
    }

    public String getBackoffice_loginPage_agreementCheckbox() {
        return backoffice_loginPage_agreementCheckbox;
    }

    public void setBackoffice_loginPage_agreementCheckbox(String backoffice_loginPage_agreementCheckbox) {
        this.backoffice_loginPage_agreementCheckbox = backoffice_loginPage_agreementCheckbox;
    }

    public String getBackoffice_loginPage_forgetPasswordLink() {
        return backoffice_loginPage_forgetPasswordLink;
    }

    public void setBackoffice_loginPage_forgetPasswordLink(String backoffice_loginPage_forgetPasswordLink) {
        this.backoffice_loginPage_forgetPasswordLink = backoffice_loginPage_forgetPasswordLink;
    }

    public String getBackoffice_loginPage_loginButton() {
        return backoffice_loginPage_loginButton;
    }

    public void setBackoffice_loginPage_loginButton(String backoffice_loginPage_loginButton) {
        this.backoffice_loginPage_loginButton = backoffice_loginPage_loginButton;
    }

    public String getDashboard_profileDropdown() {
        return dashboard_profileDropdown;
    }

    public void setDashboard_profileDropdown(String dashboard_profileDropdown) {
        this.dashboard_profileDropdown = dashboard_profileDropdown;
    }

    public String getDashboard_logoutLink() {
        return dashboard_logoutLink;
    }

    public void setDashboard_logoutLink(String dashboard_logoutLink) {
        this.dashboard_logoutLink = dashboard_logoutLink;
    }

    public String getHomepage_financialReviewTab_reviewDocusignDocLabel() {
        return homepage_financialReviewTab_reviewDocusignDocLabel;
    }

    public void setHomepage_financialReviewTab_reviewDocusignDocLabel(String homepage_financialReviewTab_reviewDocusignDocLabel) {
        this.homepage_financialReviewTab_reviewDocusignDocLabel = homepage_financialReviewTab_reviewDocusignDocLabel;
    }

    public String getHomepage_financialReviewTab_agreementCheckBox() {
        return homepage_financialReviewTab_agreementCheckBox;
    }

    public void setHomepage_financialReviewTab_agreementCheckBox(String homepage_financialReviewTab_agreementCheckBox) {
        this.homepage_financialReviewTab_agreementCheckBox = homepage_financialReviewTab_agreementCheckBox;
    }

    public String getHomepage_financialReviewTab_continueButton() {
        return homepage_financialReviewTab_continueButton;
    }

    public void setHomepage_financialReviewTab_continueButton(String homepage_financialReviewTab_continueButton) {
        this.homepage_financialReviewTab_continueButton = homepage_financialReviewTab_continueButton;
    }

    public String getHomepage_financialReviewTab_finishButton() {
        return homepage_financialReviewTab_finishButton;
    }

    public void setHomepage_financialReviewTab_finishButton(String homepage_financialReviewTab_finishButton) {
        this.homepage_financialReviewTab_finishButton = homepage_financialReviewTab_finishButton;
    }

    public String getHomepage_financialReviewTab_bankLinkingSuccessfulMessageLabel() {
        return homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel;
    }

    public void setHomepage_financialReviewTab_bankLinkingSuccessfulMessageLabel(String homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel) {
        this.homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel = homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel;
    }

    public String getSelectBank_successfulConnectionLabel() {
        return selectBank_successfulConnectionLabel;
    }

    public void setSelectBank_successfulConnectionLabel(String selectBank_successfulConnectionLabel) {
        this.selectBank_successfulConnectionLabel = selectBank_successfulConnectionLabel;
    }

    public String getSelectBank_selectBankLabel() {
        return selectBank_selectBankLabel;
    }

    public void setSelectBank_selectBankLabel(String selectBank_selectBankLabel) {
        this.selectBank_selectBankLabel = selectBank_selectBankLabel;
    }

    public String getSelectBank_searchBankTextBox() {
        return selectBank_searchBankTextBox;
    }

    public void setSelectBank_searchBankTextBox(String selectBank_searchBankTextBox) {
        this.selectBank_searchBankTextBox = selectBank_searchBankTextBox;
    }

    public String getSelectBank_selectedBank() {
        return selectBank_selectedBank;
    }

    public void setSelectBank_selectedBank(String selectBank_selectedBank) {
        this.selectBank_selectedBank = selectBank_selectedBank;
    }

    public String getSelectBank_plaidUsernameTextBox() {
        return selectBank_plaidUsernameTextBox;
    }

    public void setSelectBank_plaidUsernameTextBox(String selectBank_plaidUsernameTextBox) {
        this.selectBank_plaidUsernameTextBox = selectBank_plaidUsernameTextBox;
    }

    public String getSelectBank_plaidPasswordTextBox() {
        return selectBank_plaidPasswordTextBox;
    }

    public void setSelectBank_plaidPasswordTextBox(String selectBank_plaidPasswordTextBox) {
        this.selectBank_plaidPasswordTextBox = selectBank_plaidPasswordTextBox;
    }

    public String getSelectBank_submitPlaidButton() {
        return selectBank_submitPlaidButton;
    }

    public void setSelectBank_submitPlaidButton(String selectBank_submitPlaidButton) {
        this.selectBank_submitPlaidButton = selectBank_submitPlaidButton;
    }

    public String getSelectBank_resultantBank() {
        return selectBank_resultantBank;
    }

    public void setSelectBank_resultantBank(String selectBank_resultantBank) {
        this.selectBank_resultantBank = selectBank_resultantBank;
    }

    public String getSelectBank_selectYourAccountLabel() {
        return selectBank_selectYourAccountLabel;
    }

    public void setSelectBank_selectYourAccountLabel(String selectBank_selectYourAccountLabel) {
        this.selectBank_selectYourAccountLabel = selectBank_selectYourAccountLabel;
    }

    public String getSelectBank_continueButton() {
        return selectBank_continueButton;
    }

    public void setSelectBank_continueButton(String selectBank_continueButton) {
        this.selectBank_continueButton = selectBank_continueButton;
    }

    public String getBorrower_loginpage_loginLabel() {
        return borrower_loginpage_loginLabel;
    }

    public void setBorrower_loginpage_loginLabel(String borrower_loginpage_loginLabel) {
        this.borrower_loginpage_loginLabel = borrower_loginpage_loginLabel;
    }

    public String getBorrower_loginpage_emailTextBox() {
        return borrower_loginpage_emailTextBox;
    }

    public void setBorrower_loginpage_emailTextBox(String borrower_loginpage_emailTextBox) {
        this.borrower_loginpage_emailTextBox = borrower_loginpage_emailTextBox;
    }

    public String getBorrower_loginpage_passwordTextBox() {
        return borrower_loginpage_passwordTextBox;
    }

    public void setBorrower_loginpage_passwordTextBox(String borrower_loginpage_passwordTextBox) {
        this.borrower_loginpage_passwordTextBox = borrower_loginpage_passwordTextBox;
    }

    public String getBorrower_loginpage_submitButton() {
        return borrower_loginpage_submitButton;
    }

    public void setBorrower_loginpage_submitButton(String borrower_loginpage_submitButton) {
        this.borrower_loginpage_submitButton = borrower_loginpage_submitButton;
    }

    public String getHomepage_newApplicationLabel() {
        return homepage_newApplicationLabel;
    }

    public void setHomepage_newApplicationLabel(String homepage_newApplicationLabel) {
        this.homepage_newApplicationLabel = homepage_newApplicationLabel;
    }

    public String getHomepage_signinButton() {
        return homepage_signinButton;
    }

    public void setHomepage_signinButton(String homepage_signinButton) {
        this.homepage_signinButton = homepage_signinButton;
    }

    public String getHomepage_basicTab() {
        return homepage_basicTab;
    }

    public void setHomepage_basicTab(String homepage_basicTab) {
        this.homepage_basicTab = homepage_basicTab;
    }

    public String getHomepage_yourBusinessTab() {
        return homepage_yourBusinessTab;
    }

    public void setHomepage_yourBusinessTab(String homepage_yourBusinessTab) {
        this.homepage_yourBusinessTab = homepage_yourBusinessTab;
    }

    public String getHomepage_aboutYouTab() {
        return homepage_aboutYouTab;
    }

    public void setHomepage_aboutYouTab(String homepage_aboutYouTab) {
        this.homepage_aboutYouTab = homepage_aboutYouTab;
    }

    public String getHomepage_financialReviewTab() {
        return homepage_financialReviewTab;
    }

    public void setHomepage_financialReviewTab(String homepage_financialReviewTab) {
        this.homepage_financialReviewTab = homepage_financialReviewTab;
    }

    public String getHomepage_basicTab_businessNameTextBox() {
        return homepage_basicTab_businessNameTextBox;
    }

    public void setHomepage_basicTab_businessNameTextBox(String homepage_basicTab_businessNameTextBox) {
        this.homepage_basicTab_businessNameTextBox = homepage_basicTab_businessNameTextBox;
    }

    public String getHomepage_basicTab_amountSeekingTextBox() {
        return homepage_basicTab_amountSeekingTextBox;
    }

    public void setHomepage_basicTab_amountSeekingTextBox(String homepage_basicTab_amountSeekingTextBox) {
        this.homepage_basicTab_amountSeekingTextBox = homepage_basicTab_amountSeekingTextBox;
    }

    public String getHomepage_basicTab_useOfFundsDropdown() {
        return homepage_basicTab_useOfFundsDropdown;
    }

    public void setHomepage_basicTab_useOfFundsDropdown(String homepage_basicTab_useOfFundsDropdown) {
        this.homepage_basicTab_useOfFundsDropdown = homepage_basicTab_useOfFundsDropdown;
    }

    public String getHomepage_basicTab_annualBusinessRevenueTextBox() {
        return homepage_basicTab_annualBusinessRevenueTextBox;
    }

    public void setHomepage_basicTab_annualBusinessRevenueTextBox(String homepage_basicTab_annualBusinessRevenueTextBox) {
        this.homepage_basicTab_annualBusinessRevenueTextBox = homepage_basicTab_annualBusinessRevenueTextBox;
    }

    public String getHomepage_basicTab_firstNameTextBox() {
        return homepage_basicTab_firstNameTextBox;
    }

    public void setHomepage_basicTab_firstNameTextBox(String homepage_basicTab_firstNameTextBox) {
        this.homepage_basicTab_firstNameTextBox = homepage_basicTab_firstNameTextBox;
    }

    public String getHomepage_basicTab_lastNameTextBox() {
        return homepage_basicTab_lastNameTextBox;
    }

    public void setHomepage_basicTab_lastNameTextBox(String homepage_basicTab_lastNameTextBox) {
        this.homepage_basicTab_lastNameTextBox = homepage_basicTab_lastNameTextBox;
    }

    public String getHomepage_basicTab_phoneTextBox() {
        return homepage_basicTab_phoneTextBox;
    }

    public void setHomepage_basicTab_phoneTextBox(String homepage_basicTab_phoneTextBox) {
        this.homepage_basicTab_phoneTextBox = homepage_basicTab_phoneTextBox;
    }

    public String getHomepage_basicTab_emailTextBox() {
        return homepage_basicTab_emailTextBox;
    }

    public void setHomepage_basicTab_emailTextBox(String homepage_basicTab_emailTextBox) {
        this.homepage_basicTab_emailTextBox = homepage_basicTab_emailTextBox;
    }

    public String getHomepage_basicTab_confirmPasswordTextBox() {
        return homepage_basicTab_confirmPasswordTextBox;
    }

    public void setHomepage_basicTab_confirmPasswordTextBox(String homepage_basicTab_confirmPasswordTextBox) {
        this.homepage_basicTab_confirmPasswordTextBox = homepage_basicTab_confirmPasswordTextBox;
    }

    public String getHomepage_basicTab_passwordTextBox() {
        return homepage_basicTab_passwordTextBox;
    }

    public void setHomepage_basicTab_passwordTextBox(String homepage_basicTab_passwordTextBox) {
        this.homepage_basicTab_passwordTextBox = homepage_basicTab_passwordTextBox;
    }

    public String getHomepage_basicTab_signInButton() {
        return homepage_basicTab_signInButton;
    }

    public void setHomepage_basicTab_signInButton(String homepage_basicTab_signInButton) {
        this.homepage_basicTab_signInButton = homepage_basicTab_signInButton;
    }

    public String getHomepage_basicTab_startedApplicationLabel() {
        return homepage_basicTab_startedApplicationLabel;
    }

    public void setHomepage_basicTab_startedApplicationLabel(String homepage_basicTab_startedApplicationLabel) {
        this.homepage_basicTab_startedApplicationLabel = homepage_basicTab_startedApplicationLabel;
    }

    public String getHomepage_yourBusinessTab_businessLabel() {
        return homepage_yourBusinessTab_businessLabel;
    }

    public void setHomepage_yourBusinessTab_businessLabel(String homepage_yourBusinessTab_businessLabel) {
        this.homepage_yourBusinessTab_businessLabel = homepage_yourBusinessTab_businessLabel;
    }

    public String getHomepage_yourBusinessTab_businessAddressTextBox() {
        return homepage_yourBusinessTab_businessAddressTextBox;
    }

    public void setHomepage_yourBusinessTab_businessAddressTextBox(String homepage_yourBusinessTab_businessAddressTextBox) {
        this.homepage_yourBusinessTab_businessAddressTextBox = homepage_yourBusinessTab_businessAddressTextBox;
    }

    public String getHomepage_yourBusinessTab_dbaTextBox() {
        return homepage_yourBusinessTab_dbaTextBox;
    }

    public void setHomepage_yourBusinessTab_dbaTextBox(String homepage_yourBusinessTab_dbaTextBox) {
        this.homepage_yourBusinessTab_dbaTextBox = homepage_yourBusinessTab_dbaTextBox;
    }

    public String getHomepage_yourBusinessTab_cityTextBox() {
        return homepage_yourBusinessTab_cityTextBox;
    }

    public void setHomepage_yourBusinessTab_cityTextBox(String homepage_yourBusinessTab_cityTextBox) {
        this.homepage_yourBusinessTab_cityTextBox = homepage_yourBusinessTab_cityTextBox;
    }

    public String getHomepage_yourBusinessTab_stateDropdown() {
        return homepage_yourBusinessTab_stateDropdown;
    }

    public void setHomepage_yourBusinessTab_stateDropdown(String homepage_yourBusinessTab_stateDropdown) {
        this.homepage_yourBusinessTab_stateDropdown = homepage_yourBusinessTab_stateDropdown;
    }

    public String getHomepage_yourBusinessTab_zipCodeTextBox() {
        return homepage_yourBusinessTab_zipCodeTextBox;
    }

    public void setHomepage_yourBusinessTab_zipCodeTextBox(String homepage_yourBusinessTab_zipCodeTextBox) {
        this.homepage_yourBusinessTab_zipCodeTextBox = homepage_yourBusinessTab_zipCodeTextBox;
    }

    public String getHomepage_yourBusinessTab_businessPhoneTextBox() {
        return homepage_yourBusinessTab_businessPhoneTextBox;
    }

    public void setHomepage_yourBusinessTab_businessPhoneTextBox(String homepage_yourBusinessTab_businessPhoneTextBox) {
        this.homepage_yourBusinessTab_businessPhoneTextBox = homepage_yourBusinessTab_businessPhoneTextBox;
    }

    public String getHomepage_yourBusinessTab_bstMonthDropdown() {
        return homepage_yourBusinessTab_bstMonthDropdown;
    }

    public void setHomepage_yourBusinessTab_bstMonthDropdown(String homepage_yourBusinessTab_bstMonthDropdown) {
        this.homepage_yourBusinessTab_bstMonthDropdown = homepage_yourBusinessTab_bstMonthDropdown;
    }

    public String getHomepage_yourBusinessTab_bstDayDropdown() {
        return homepage_yourBusinessTab_bstDayDropdown;
    }

    public void setHomepage_yourBusinessTab_bstDayDropdown(String homepage_yourBusinessTab_bstDayDropdown) {
        this.homepage_yourBusinessTab_bstDayDropdown = homepage_yourBusinessTab_bstDayDropdown;
    }

    public String getHomepage_yourBusinessTab_businessWebsiteTextBox() {
        return homepage_yourBusinessTab_businessWebsiteTextBox;
    }

    public void setHomepage_yourBusinessTab_businessWebsiteTextBox(String homepage_yourBusinessTab_businessWebsiteTextBox) {
        this.homepage_yourBusinessTab_businessWebsiteTextBox = homepage_yourBusinessTab_businessWebsiteTextBox;
    }

    public String getHomepage_yourBusinessTab_legalEntityDropdown() {
        return homepage_yourBusinessTab_legalEntityDropdown;
    }

    public void setHomepage_yourBusinessTab_legalEntityDropdown(String homepage_yourBusinessTab_legalEntityDropdown) {
        this.homepage_yourBusinessTab_legalEntityDropdown = homepage_yourBusinessTab_legalEntityDropdown;
    }

    public String getHomepage_yourBusinessTab_businessLocationDropdown() {
        return homepage_yourBusinessTab_businessLocationDropdown;
    }

    public void setHomepage_yourBusinessTab_businessLocationDropdown(String homepage_yourBusinessTab_businessLocationDropdown) {
        this.homepage_yourBusinessTab_businessLocationDropdown = homepage_yourBusinessTab_businessLocationDropdown;
    }

    public String getHomepage_yourBusinessTab_federalSalesTaxTextBox() {
        return homepage_yourBusinessTab_federalSalesTaxTextBox;
    }

    public void setHomepage_yourBusinessTab_federalSalesTaxTextBox(String homepage_yourBusinessTab_federalSalesTaxTextBox) {
        this.homepage_yourBusinessTab_federalSalesTaxTextBox = homepage_yourBusinessTab_federalSalesTaxTextBox;
    }

    public String getHomepage_yourBusinessTab_industryDropdown() {
        return homepage_yourBusinessTab_industryDropdown;
    }

    public void setHomepage_yourBusinessTab_industryDropdown(String homepage_yourBusinessTab_industryDropdown) {
        this.homepage_yourBusinessTab_industryDropdown = homepage_yourBusinessTab_industryDropdown;
    }

    public String getHomepage_yourBusinessTab_importanceDropdown() {
        return homepage_yourBusinessTab_importanceDropdown;
    }

    public void setHomepage_yourBusinessTab_importanceDropdown(String homepage_yourBusinessTab_importanceDropdown) {
        this.homepage_yourBusinessTab_importanceDropdown = homepage_yourBusinessTab_importanceDropdown;
    }

    public String getHomepage_yourBusinessTab_continueButton() {
        return homepage_yourBusinessTab_continueButton;
    }

    public void setHomepage_yourBusinessTab_continueButton(String homepage_yourBusinessTab_continueButton) {
        this.homepage_yourBusinessTab_continueButton = homepage_yourBusinessTab_continueButton;
    }

    public String getHomepage_aboutYouTab_getToKnowLabel() {
        return homepage_aboutYouTab_getToKnowLabel;
    }

    public void setHomepage_aboutYouTab_getToKnowLabel(String homepage_aboutYouTab_getToKnowLabel) {
        this.homepage_aboutYouTab_getToKnowLabel = homepage_aboutYouTab_getToKnowLabel;
    }

    public String getHomepage_aboutYouTab_emailAddressTextBox() {
        return homepage_aboutYouTab_emailAddressTextBox;
    }

    public void setHomepage_aboutYouTab_emailAddressTextBox(String homepage_aboutYouTab_emailAddressTextBox) {
        this.homepage_aboutYouTab_emailAddressTextBox = homepage_aboutYouTab_emailAddressTextBox;
    }

    public String getHomepage_aboutYouTab_firstNameTextBox() {
        return homepage_aboutYouTab_firstNameTextBox;
    }

    public void setHomepage_aboutYouTab_firstNameTextBox(String homepage_aboutYouTab_firstNameTextBox) {
        this.homepage_aboutYouTab_firstNameTextBox = homepage_aboutYouTab_firstNameTextBox;
    }

    public String getHomepage_aboutYouTab_lastNameTextBox() {
        return homepage_aboutYouTab_lastNameTextBox;
    }

    public void setHomepage_aboutYouTab_lastNameTextBox(String homepage_aboutYouTab_lastNameTextBox) {
        this.homepage_aboutYouTab_lastNameTextBox = homepage_aboutYouTab_lastNameTextBox;
    }

    public String getHomepage_aboutYouTab_addressTextBox() {
        return homepage_aboutYouTab_addressTextBox;
    }

    public void setHomepage_aboutYouTab_addressTextBox(String homepage_aboutYouTab_addressTextBox) {
        this.homepage_aboutYouTab_addressTextBox = homepage_aboutYouTab_addressTextBox;
    }

    public String getHomepage_aboutYouTab_homeCityTextBox() {
        return homepage_aboutYouTab_homeCityTextBox;
    }

    public void setHomepage_aboutYouTab_homeCityTextBox(String homepage_aboutYouTab_homeCityTextBox) {
        this.homepage_aboutYouTab_homeCityTextBox = homepage_aboutYouTab_homeCityTextBox;
    }

    public String getHomepage_aboutYouTab_homeStateDropdown() {
        return homepage_aboutYouTab_homeStateDropdown;
    }

    public void setHomepage_aboutYouTab_homeStateDropdown(String homepage_aboutYouTab_homeStateDropdown) {
        this.homepage_aboutYouTab_homeStateDropdown = homepage_aboutYouTab_homeStateDropdown;
    }

    public String getHomepage_aboutYouTab_zipCodeTextBox() {
        return homepage_aboutYouTab_zipCodeTextBox;
    }

    public void setHomepage_aboutYouTab_zipCodeTextBox(String homepage_aboutYouTab_zipCodeTextBox) {
        this.homepage_aboutYouTab_zipCodeTextBox = homepage_aboutYouTab_zipCodeTextBox;
    }

    public String getHomepage_aboutYouTab_phoneNumberTextBox() {
        return homepage_aboutYouTab_phoneNumberTextBox;
    }

    public void setHomepage_aboutYouTab_phoneNumberTextBox(String homepage_aboutYouTab_phoneNumberTextBox) {
        this.homepage_aboutYouTab_phoneNumberTextBox = homepage_aboutYouTab_phoneNumberTextBox;
    }

    public String getHomepage_aboutYouTab_mobileNumberTextBox() {
        return homepage_aboutYouTab_mobileNumberTextBox;
    }

    public void setHomepage_aboutYouTab_mobileNumberTextBox(String homepage_aboutYouTab_mobileNumberTextBox) {
        this.homepage_aboutYouTab_mobileNumberTextBox = homepage_aboutYouTab_mobileNumberTextBox;
    }

    public String getHomepage_aboutYouTab_ownershipTextBox() {
        return homepage_aboutYouTab_ownershipTextBox;
    }

    public void setHomepage_aboutYouTab_ownershipTextBox(String homepage_aboutYouTab_ownershipTextBox) {
        this.homepage_aboutYouTab_ownershipTextBox = homepage_aboutYouTab_ownershipTextBox;
    }

    public String getHomepage_aboutYouTab_ssnTextBox() {
        return homepage_aboutYouTab_ssnTextBox;
    }

    public void setHomepage_aboutYouTab_ssnTextBox(String homepage_aboutYouTab_ssnTextBox) {
        this.homepage_aboutYouTab_ssnTextBox = homepage_aboutYouTab_ssnTextBox;
    }

    public String getHomepage_aboutYouTab_mobDropdown() {
        return homepage_aboutYouTab_mobDropdown;
    }

    public void setHomepage_aboutYouTab_mobDropdown(String homepage_aboutYouTab_mobDropdown) {
        this.homepage_aboutYouTab_mobDropdown = homepage_aboutYouTab_mobDropdown;
    }

    public String getHomepage_aboutYouTab_dobDropdown() {
        return homepage_aboutYouTab_dobDropdown;
    }

    public void setHomepage_aboutYouTab_dobDropdown(String homepage_aboutYouTab_dobDropdown) {
        this.homepage_aboutYouTab_dobDropdown = homepage_aboutYouTab_dobDropdown;
    }

    public String getHomepage_aboutYouTab_yobDropdown() {
        return homepage_aboutYouTab_yobDropdown;
    }

    public void setHomepage_aboutYouTab_yobDropdown(String homepage_aboutYouTab_yobDropdown) {
        this.homepage_aboutYouTab_yobDropdown = homepage_aboutYouTab_yobDropdown;
    }    
    

    public String getHomepage_aboutYouTab_AcceptTC() {
		return homepage_aboutYouTab_AcceptTC;
	}

	public void setHomepage_aboutYouTab_AcceptTC(String homepage_aboutYouTab_AcceptTC) {
		this.homepage_aboutYouTab_AcceptTC = homepage_aboutYouTab_AcceptTC;
	}

	public String getHomepage_aboutYouTab_continueButton() {
        return homepage_aboutYouTab_continueButton;
    }

    public void setHomepage_aboutYouTab_continueButton(String homepage_aboutYouTab_continueButton) {
        this.homepage_aboutYouTab_continueButton = homepage_aboutYouTab_continueButton;
    }
    

    public String getHomepage_financialReviewTab_selectOptionsLabel() {
        return homepage_financialReviewTab_selectOptionsLabel;
    }

    public void setHomepage_financialReviewTab_selectOptionsLabel(String homepage_financialReviewTab_selectOptionsLabel) {
        this.homepage_financialReviewTab_selectOptionsLabel = homepage_financialReviewTab_selectOptionsLabel;
    }

    public String getHomepage_financialReviewTab_linkBankAccountButton() {
        return homepage_financialReviewTab_linkBankAccountButton;
    }

    public void setHomepage_financialReviewTab_linkBankAccountButton(String homepage_financialReviewTab_linkBankAccountButton) {
        this.homepage_financialReviewTab_linkBankAccountButton = homepage_financialReviewTab_linkBankAccountButton;
    }

    public String getHomepage_financialReviewTab_skipButton() {
        return homepage_financialReviewTab_skipButton;
    }

    public void setHomepage_financialReviewTab_skipButton(String homepage_financialReviewTab_skipButton) {
        this.homepage_financialReviewTab_skipButton = homepage_financialReviewTab_skipButton;
    }


    public String getHomepage_basicTab_continueButton() {
        return homepage_basicTab_continueButton;
    }

    public void setHomepage_basicTab_continueButton(String homepage_basicTab_continueButton) {
        this.homepage_basicTab_continueButton = homepage_basicTab_continueButton;
    }

	public String getBackoffice_searchpage_SelectApp() {		
		return backoffice_searchpage_SelectApp;
	}
	
	

    /*@Value(value = "${dashboard.overviewTab}")
    private String dashboard_overviewTab;

    @Value(value = "${dashboard.todoTab}")
    private String dashboard_todoTab;

    @Value(value = "${dashboard.profileTab}")
    private String dashboard_profileTab;

    @Value(value = "${dashboard.changePasswordTab}")
    private String dashboard_changePasswordTab;

    @Value(value = "${dashboard.borrowerNameLabel}")
    private String dashboard_borrowerNameLabel;

    @Value(value = "${dashboard.appIdLabel}")
    private String dashboard_appIdLabel;

    @Value(value = "${dashboard.phoneNumberLabel}")
    private String dashboard_phoneNumberLabel;

    @Value(value = "${dashboard.emailIdLabel}")
    private String dashboard_emailIdLabel;

    @Value(value = "${dashboard.loanAmountAppliedLabel}")
    private String dashboard_loanAmountAppliedLabel;

    @Value(value = "${dashboard.purposeOfFundsLabel}")
    private String dashboard_purposeOfFundsLabel;

    @Value(value = "${dashboard.bankVerificationRequestMsg}")
    private String dashboard_bankVerificationRequestMsg;

    @Value(value = "${dashboard.linkBankAccountButton}")
    private String dashboard_linkBankAccountButton;






    @Value(value = "${backoffice.searchpage.applicationNumberTextBox}")
    private String backoffice_searchpage_applicationNumberTextBox;

    @Value(value = "${backoffice.searchpage.businessPhoneTextBox}")
    private String backoffice_searchpage_businessPhoneTextBox;

    @Value(value = "${backoffice.searchpage.firstNameTextBox}")
    private String backoffice_searchpage_firstNameTextBox;

    @Value(value = "${backoffice.searchpage.lastNameTextBox}")
    private String backoffice_searchpage_lastNameTextBox;

    @Value(value = "${backoffice.searchpage.emailTextBox}")
    private String backoffice_searchpage_emailTextBox;

    @Value(value = "${backoffice.searchpage.businessNameTextBox}")
    private String backoffice_searchpage_businessNameTextBox;

    @Value(value = "${backoffice.searchpage.statusDropdown}")
    private String backoffice_searchpage_statusDropdown;

    @Value(value = "${backoffice.searchpage.appDateDropdown}")
    private String backoffice_searchpage_appDateDropdown;

    @Value(value = "${backoffice.searchpage.expDateDropdown}")
    private String backoffice_searchpage_expDateDropdown;

    @Value(value = "${backoffice.searchpage.searchButton}")
    private String backoffice_searchpage_searchButton;

    @Value(value = "${backoffice.searchpage.foundAppIdLabel}")
    private String backoffice_searchpage_foundAppIdLabel;

    @Value(value = "${backoffice.searchpage.foundAppDateLabl}")
    private String backoffice_searchpage_foundAppDateLabl;

    @Value(value = "${backoffice.searchpage.foundBusinessNameLabel}")
    private String backoffice_searchpage_foundBusinessNameLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessPhoneLabel}")
    private String backoffice_searchpage_foundBusinessPhoneLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessLoanAmountLabel}")
    private String backoffice_searchpage_foundBusinessLoanAmountLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessExpiryDateLabel}")
    private String backoffice_searchpage_foundBusinessExpiryDateLabel;

    @Value(value = "${backoffice.searchpage.foundBusinessStatusLabel}")
    private String backoffice_searchpage_foundBusinessStatusLabel;

    @Value(value = "${backoffice.searchpage.viewDetailsButton}")
    private String backoffice_searchpage_viewDetailsButton;

    @Value(value = "${backoffice.appDetailspage.headerStatus}")
    private String backoffice_appDetailspage_headerStatus;

    @Value(value = "${backoffice.appDetailspage.activityLog.email}")
    private String backoffice_appDetailspage_activityLog_email;

    @Value(value = "${backoffice.appDetailspage.activityLog.factVerification}")
    private String backoffice_appDetailspage_activityLog_factVerification;

    @Value(value = "${backoffice.appDetailspage.verificationDashboardTab}")
    private String backoffice_appDetailspage_verificationDashboardTab;

    @Value(value = "${backoffice.appDetailspage.cashflowTab}")
    private String backoffice_appDetailspage_cashflowTab;

    @Value(value = "${selectBank.selectBankLabel}")
    private String selectBank_selectBankLabel;

    @Value(value = "${selectBank.searchBankTextBox}")
    private String selectBank_searchBankTextBox;

    @Value(value = "${selectBank.sunTrustBank}")
    private String selectBank_sunTrustBank;

    @Value(value = "${selectBank.plaidUsernameTextBox}")
    private String selectBank_plaidUsernameTextBox;

    @Value(value = "${selectBank.plaidPasswordTextBox}")
    private String selectBank_plaidPasswordTextBox;

    @Value(value = "${selectBank.submitPlaidButton}")
    private String selectBank_submitPlaidButton;

    @Value(value = "${selectBank.sunTrustResultantBank}")
    private String selectBank_sunTrustResultantBank;

    @Value(value = "${selectBank.selectYourAccountLabel}")
    private String selectBank_selectYourAccountLabel;

    @Value(value = "${selectBank.plaidSavingAccount}")
    private String selectBank_plaidSavingAccount;

    @Value(value = "${selectBank.continueButton}")
    private String selectBank_continueButton;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflowDropdown}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflow.submitButton}")
    private String backoffice_appDetailspage_cashflowTab_editCashflow_submitButton;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflowLink}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowLink;

    @Value(value = "${backoffice.appDetailspage.cashflowTab.editCashflow.editCashflowSummaryTitle}")
    private String backoffice_appDetailspage_cashflowTab_editCashflow_editCashflowSummaryTitle;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_notinitiatedLabel;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.initiateButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_initiateButton;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.weblinkTextBox}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_weblinkTextBox;

    @Value(value = "${backoffice.appDetailspage.verificationdashboard.searchVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.searchVerification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_searchVerification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_notinitiatedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.initiateButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_initiateButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.passVerificationCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_passVerificationCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bakruptcyVerification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_bakruptcyVerification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.activityLog.cashFlowUpdate}")
    private String backoffice_appDetailspage_activityLog_cashFlowUpdate;

    @Value(value="${backoffice.appDetailspage.activityLog.refreshButton}")
    private String backoffice_appDetailspage_activityLog_refreshButton;

    @Value(value="${backoffice.homepage.actionsButton}")
    private String backoffice_homepage_actionsButton;

    @Value(value="${backoffice.homepage.actions.computeOffer}")
    private String backoffice_homepage_actions_computeOffer;

    @Value(value="${backoffice.homepage.actions.computerOfferButton}")
    private String backoffice_homepage_actions_computerOfferButton;

    @Value(value="${backoffice.searchpage.offerTab}")
    private String backoffice_searchpage_offerTab;

    @Value(value="${backoffice.searchpage.offerTab.dealGenerationLabel}")
    private String backoffice_searchpage_offerTab_dealGenerationLabel;

    @Value(value="${backoffice.searchpage.offerTab.typeOfPaymentDropdown}")
    private String backoffice_searchpage_offerTab_typeOfPaymentDropdown;

    @Value(value="${backoffice.searchpage.offerTab.generateDealButton}")
    private String backoffice_searchpage_offerTab_generateDealButton;

    @Value(value="${backoffice.searchpage.offerTab.dealSelectedLabel}")
    private String backoffice_searchpage_offerTab_dealSelectedLabel;

    @Value(value="${backoffice.homepage.actions.uploadDocsButton}")
    private String backoffice_homepage_actions_uploadDocsButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.idverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.licenceCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_licenceCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.nameCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_nameCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.dobCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_dobCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.idverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.idverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_idverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.accountNumberTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_accountNumberTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.routingNumberTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_routingNumberTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.nameOfBusinessCheckBox}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_nameOfBusinessCheckBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.bankverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.bankverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_bankverification_closeButton;

    @Value(value="${backoffice.homepage.actions.sendLoanAgreement}")
    private String backoffice_homepage_actions_sendLoanAgreement;

    @Value(value="${backoffice.homepage.actions.sendMCAAgreementButton}")
    private String backoffice_homepage_actions_sendMCAAgreementButton;

    @Value(value="${docusign.welcomeLabel}")
    private String docusign_welcomeLabel;

    @Value(value="${docusign.agreementCheckbox}")
    private String docusign_agreementCheckbox;

    @Value(value="${docusign.continueButton}")
    private String docusign_continueButton;

    @Value(value="${docusign.startNavigationButton}")
    private String docusign_startNavigationButton;

    @Value(value="${docusign.finishSignButton}")
    private String docusign_finishSignButton;

    @Value(value="${docusign.adopotSignatureLabel}")
    private String docusign_adopotSignatureLabel;

    @Value(value="${docusign.adoptAndSignButton}")
    private String docusign_adoptAndSignButton;

    @Value(value="${docusign.postSignThanksNoteLabel}")
    private String docusign_postSignThanksNoteLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.idVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_idVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.allSignatureCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_allSignatureCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashbaord.signedDocverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.welcomeVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_welcomeVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.acceptTermsCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_acceptTermsCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.welcomeverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_welcomeverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.merchantVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_merchantVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.addressCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_addressCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.merchantverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_merchantverification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.propertyVerificationLabel}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_propertyVerificationLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.idScreenshot}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_idScreenshot;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.pgorrentCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_pgorrentCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.goodstandingCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_goodstandingCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.renewLeaseCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_renewLeaseCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.submitMortgageCheckbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_submitMortgageCheckbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.landlordPhoneTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordPhoneTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.landlordNameTextbox}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_landlordNameTextbox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.verifiedStatus}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_verifiedStatus;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.propertyverification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_propertyverification_closeButton;

    @Value(value="${backoffice.homepage.closeUploadDocsButton}")
    private String backoffice_homepage_closeUploadDocsButton;

    @Value(value="${backoffice.homepage.actions.addFundingRequestMenuButton}")
    private String backoffice_homepage_actions_addFundingRequestMenuButton;

    @Value(value="${backoffice.homepage.actions.addFundingRequestButton}")
    private String backoffice_homepage_actions_addFundingRequestButton;

    @Value(value="${backoffice.homepage.actions.editMenuButton}")
    private String backoffice_homepage_actions_editMenuButton;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessName}")
    private String backoffice_appDetailspage_businessinfo_businessName;

    @Value(value="${backoffice.appDetailspage.businessinfo.email}")
    private String backoffice_appDetailspage_businessinfo_email;

    @Value(value="${backoffice.appDetailspage.businessinfo.dba}")
    private String backoffice_appDetailspage_businessinfo_dba;

    @Value(value="${backoffice.appDetailspage.businessinfo.address}")
    private String backoffice_appDetailspage_businessinfo_address;

    @Value(value="${backoffice.appDetailspage.businessinfo.phone}")
    private String backoffice_appDetailspage_businessinfo_phone;

    @Value(value="${backoffice.appDetailspage.businessinfo.reqAmount}")
    private String backoffice_appDetailspage_businessinfo_reqAmount;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessTaxId}")
    private String backoffice_appDetailspage_businessinfo_businessTaxId;

    @Value(value="${backoffice.appDetailspage.businessinfo.annualRevenue}")
    private String backoffice_appDetailspage_businessinfo_annualRevenue;

    @Value(value="${backoffice.appDetailspage.businessinfo.estDate}")
    private String backoffice_appDetailspage_businessinfo_estDate;

    @Value(value="${backoffice.appDetailspage.businessinfo.businessOwnership}")
    private String backoffice_appDetailspage_businessinfo_businessOwnership;

    @Value(value="${backoffice.appDetailspage.businessinfo.avgBankBalance}")
    private String backoffice_appDetailspage_businessinfo_avgBankBalance;

    @Value(value="${backoffice.appDetailspage.businessinfo.existingLoan}")
    private String backoffice_appDetailspage_businessinfo_existingLoan;

    @Value(value="${backoffice.appDetailspage.businessinfo.industry}")
    private String backoffice_appDetailspage_businessinfo_industry;

    @Value(value="${backoffice.appDetailspage.businessinfo.purposeOfFunds}")
    private String backoffice_appDetailspage_businessinfo_purposeOfFunds;

    @Value(value="${backoffice.appDetailspage.businessinfo.entityType}")
    private String backoffice_appDetailspage_businessinfo_entityType;

    @Value(value="${backoffice.appDetailspage.businessinfo.alertsTab}")
    private String backoffice_appDetailspage_businessinfo_alertsTab;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertName}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertName;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertDescription}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertDescription;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertStatus}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertStatus;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.alertTag}")
    private String backoffice_appDetailspage_businessinfo_alerts_alertTag;

    @Value(value="${backoffice.appDetailspage.businessinfo.alerts.dismissButton}")
    private String backoffice_appDetailspage_businessinfo_alerts_dismissButton;

    @Value(value="${backoffice.appDetailspage.activityLog.declineUpdate}")
    private String backoffice_appDetailspage_activityLog_declineUpdate;

    @Value(value="${backoffice.appDetailspage.scoringTab}")
    private String backoffice_appDetailspage_scoringTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.notinitiatedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_notinitiatedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.pscoreTab}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.pscoreValueLabel}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_pscoreValueLabel;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.intermediateTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.sourceTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.intermediateTab.pScoreLabel}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_intermediateTab_pScoreLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.creditScoreVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_creditScoreVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.scoringTab.ruleExecutionTab.pScoreTab.sourceTab.modelTab}")
    private String backoffice_appDetailspage_scoringTab_ruleExecutionTab_pScoreTab_sourceTab_modelTab;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.notVerifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_notVerifiedLabel;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.passCheckBox}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_passCheckBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.verifyButton}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifyButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.closeButton}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_closeButton;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.restrictedIndustryVerification.verifiedLabel}")
    private String backoffice_appDetailspage_verificationdashboard_restrictedIndustryVerification_verifiedLabel;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.bankNameTextBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_bankNameTextBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.accountNumberTextBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountNumberTextBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.accountTypeComboBox}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_accountTypeComboBox;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.selectFileButton}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_selectFileButton;

    @Value(value="${backoffice.appDetailspage.cashflowTab.uploadBankStatement.submitFileButton}")
    private String backoffice_appDetailspage_cashflowTab_uploadBankStatement_submitFileButton;

    @Value(value="${backoffice.homepage.actions.uploadBankStatementButton}")
    private String backoffice_homepage_actions_uploadBankStatementButton;

    @Value(value="${backoffice.appDetailspage.activityLog.bankLinkedUpdate}")
    private String backoffice_appDetailspage_activityLog_bankLinkedUpdate;

    @Value(value="${backoffice.appDetailspage.cashflowTab.editCashflowDropdown.setAsFundingAccount}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsFundingAccount;

    @Value(value="${backoffice.appDetailspage.cashflowTab.editCashflowDropdown.setAsCashflowAccount}")
    private String backoffice_appDetailspage_cashflowTab_editCashflowDropdown_setAsCashflowAccount;

    @Value(value="${backoffice.searchpage.productIdTextBox}")
    private String backoffice_searchpage_productIdTextBox;

    @Value(value="${backoffice.appDetailspage.verificationdashboard.signedDocverification.signedAgreementLabel}")
    private String backoffice_appDetailspage_verificationdashboard_signedDocverification_signedAgreementLabel;
*/

}
