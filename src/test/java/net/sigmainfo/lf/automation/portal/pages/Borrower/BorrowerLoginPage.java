package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 30-11-2017.
 */
public class BorrowerLoginPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BorrowerLoginPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerLoginPage(WebDriver driver) throws Exception {

        this.driver = driver;
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_loginLabel)));
        logger.info("=========== BorrowerLoginPage is loaded============");
    }
    public BorrowerLoginPage(){}

    public BorrowerDashboardPage login() throws Exception {
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_emailTextBox),"shaishav.sigma+3011141318@gmail.com");
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_passwordTextBox),"Jan@2017");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginpage_submitButton),"Login");
        return new BorrowerDashboardPage(driver);
    }
}
