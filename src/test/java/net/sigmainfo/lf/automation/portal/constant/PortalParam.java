package net.sigmainfo.lf.automation.portal.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */
@Component
public class PortalParam {


    public static String testId;
    public static String backOfficeUrl;
    public static String borrowerUrl;
    public static String custom_report_location;
    public static String businessName;
    public static String amountSeeking;
    public static String useOfFunds;
    public static String annualBusinessRevenue;
    public static String firstName;
    public static String lastName;
    public static String phone;
    public static String username;
    public static String password;
    public static String dba;
    public static String businessAddress;
    public static String city;
    public static String state;
    public static String zipCode;
    public static String businessPhone;
    public static String bsMonth;
    public static String bsDay;
    public static String bsYear;
    public static String website;
    public static String legalEntity;
    public static String location;
    public static String taxId;
    public static String industry;
    public static String importance;
    public static String homeAddress;
    public static String homeCity;
    public static String homeState;
    public static String homeZipcode;
    public static String homePhone;
    public static String homeMobile;
    public static String ownership;
    public static String ssn;
    public static String doMonth;
    public static String doDay;
    public static String doYear;
    public static String backofficeUsername;
    public static String backofficePassword;
    public static String gmailId;
    public static String gmailPassword;
    public static String plaid_user;
    public static String plaid_password;
    public static String upload_file_location;
    public static String browser;
    public static String bankToConnect;
    public static String iframeSource;

}
