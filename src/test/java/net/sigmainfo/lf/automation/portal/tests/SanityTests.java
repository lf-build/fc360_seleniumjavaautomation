package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeAppDetailsPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeLoginPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeSearchPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerLoginPage;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 08-02-2017.
 */
@Listeners(CaptureScreenshot.class)
public class SanityTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(SanityTests.class);
    public static String funcMod="FC360";
    // public WebDriver driver;

    @Autowired
    TestResults testResults;

    @Autowired
    PortalFuncUtils portalFuncUtils;

    public SanityTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "FC360";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void StartedMCAFlow() throws Exception {
        String sTestID = "StartedMCAFlow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
           // backOfficeAppDetailsPage.CashFlowUploadStep();
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
           // backOfficeAppDetailsPage.CashFlowManualVerification();
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerification();
            backOfficeAppDetailsPage.IDMaualVerification();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerification();
            backOfficeAppDetailsPage.RentMaualVerification();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerification();
            backOfficeAppDetailsPage.FundingVerification();
            Assert.assertEquals("Approved For Funding ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void Traditional_MCA_Flow() throws Exception {
        String sTestID = "Traditional_MCA_Flow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerification();
            backOfficeAppDetailsPage.IDMaualVerification();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerification();
            backOfficeAppDetailsPage.RentMaualVerification();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerification();
            backOfficeAppDetailsPage.FundingVerification();
            Assert.assertEquals("Approved For Funding ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void Streamline_LOC_Flow() throws Exception {
        String sTestID = "Streamline_LOC_Flow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);           
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage.searchAppLOCApp(appId); 
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();           
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerificationLoc();
            backOfficeAppDetailsPage.IDMaualVerificationLoc();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerificationLoc();
            backOfficeAppDetailsPage.RentMaualVerificationLoc();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerificationLoc();
            backOfficeAppDetailsPage.FundingVerificationLoc();
            Assert.assertEquals("Open ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void Premium_LOC_Flow() throws Exception {
        String sTestID = "Premium_LOC_Flow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);           
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage.searchAppLOCApp(appId); 
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();           
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerificationLoc();
            backOfficeAppDetailsPage.IDMaualVerificationLoc();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerificationLoc();
            backOfficeAppDetailsPage.RentMaualVerificationLoc();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerificationLoc();
            backOfficeAppDetailsPage.FundingVerificationLoc();
            Assert.assertEquals("Open ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void Platinum_LOC_Flow() throws Exception {
        String sTestID = "Platinum_LOC_Flow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);           
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage.searchAppLOCApp(appId); 
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();           
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerificationLoc();
            backOfficeAppDetailsPage.IDMaualVerificationLoc();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerificationLoc();
            backOfficeAppDetailsPage.RentMaualVerificationLoc();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerificationLoc();
            backOfficeAppDetailsPage.FundingVerificationLoc();
            Assert.assertEquals("Open ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void VerificationFlow_AmtLess2500() throws Exception {
        String sTestID = "VerificationFlow_AmtLess2500";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.ApplicationDataVerification();          
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    } 
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void VerificationFlow_BusiLess9Mnth() throws Exception {
        String sTestID = "VerificationFlow_BusiLess9Mnth";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.ApplicationDataVerification();
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    } 
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void VerificationFlow_BusinessLess2Yrs() throws Exception {
        String sTestID = "VerificationFlow_BusinessLess2Yrs";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.ApplicationDataVerification();
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    } 
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void VerificationFlow_CaliforniaBusiness() throws Exception {
        String sTestID = "VerificationFlow_CaliforniaBusiness";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.ApplicationDataVerification();
           /* backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerification();
            backOfficeAppDetailsPage.IDMaualVerification();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerification();
            backOfficeAppDetailsPage.RentMaualVerification();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerification();
            backOfficeAppDetailsPage.FundingVerification();*/
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
    
    
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void BSV_BusinessClosed_1Rating() throws Exception {
        String sTestID = "BSV_BusinessClosed_1Rating";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.BuisnessSocialVerification();
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void BSV_BusinessOpen_1Rating() throws Exception {
        String sTestID = "BSV_BusinessOpen_1Rating";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.BuisnessSocialVerification();
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"},enabled = true)
    public void BSV_BusinessClosed_2Rating() throws Exception {
        String sTestID = "BSV_BusinessClosed_2Rating";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.BuisnessSocialVerification();
           /* backOfficeAppDetailsPage.CashFundingAccountInitialize();         
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerification();
            backOfficeAppDetailsPage.IDMaualVerification();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerification();
            backOfficeAppDetailsPage.RentMaualVerification();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerification();
            backOfficeAppDetailsPage.FundingVerification();*/
            Assert.assertEquals("Verification ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void OtherLender_cashFlowFailed() throws Exception {
        String sTestID = "OtherLender_cashFlowFailed";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;       
        String appId;
        String bankModification = "yes";
        String flow = "null";
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);           
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.CashFlowUploadStep();
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.CashFlowManualVerificationSec(flow);          
            Assert.assertEquals("Other Lenders ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void StarterMCA_cashFlowFailed() throws Exception {
        String sTestID = "StarterMCA_cashFlowFailed";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;       
        String appId;
        String bankModification = "yes";
        String flow = "StarterMCA";
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);           
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.CashFlowUploadStep();
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.CashFlowManualVerificationSec(flow);          
            Assert.assertEquals("Offer Generation ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void PremiumLOC_cashFlowFailed() throws Exception {
        String sTestID = "PremiumLOC_cashFlowFailed";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;       
        String appId;
        String bankModification = "yes";
        String flow = "PremiumLOC";
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);           
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.CashFlowUploadStep();
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.CashFlowManualVerificationSec(flow);   
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage.searchAppLOCApp(appId); 
            Assert.assertEquals("Offer Generation ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    } 
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void changeFlowMCA2LOC() throws Exception {
        String sTestID = "changeFlowMCA2LOC";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);   
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.DecideProduct(4,"PremiumLOC");
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();           
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerificationLoc();
            backOfficeAppDetailsPage.IDMaualVerificationLoc();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerificationLoc();
            backOfficeAppDetailsPage.RentMaualVerificationLoc();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerificationLoc();
            backOfficeAppDetailsPage.FundingVerificationLoc();
            Assert.assertEquals("Open ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void changeFlowLOC2MCA() throws Exception {
        String sTestID = "changeFlowLOC2MCA";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);;
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);           
            backOfficeAppDetailsPage.CashFundingAccountInitialize();
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage.searchAppLOCApp(appId); 
            backOfficeAppDetailsPage.FillOfferGenratioStep();
            backOfficeAppDetailsPage.DecideProduct(1,"StarterMCA");
            backOfficeAppDetailsPage.UploadDocsStep();
            backOfficeAppDetailsPage.BankMaualVerification();
            backOfficeAppDetailsPage.IDMaualVerification();
            backOfficeAppDetailsPage.BusinessOwnerMaualVerification();
            backOfficeAppDetailsPage.RentMaualVerification();
            backOfficeAppDetailsPage.GeerateAgreementMaualVerification();
            backOfficeAppDetailsPage.SendAgreementSteps();
            backOfficeAppDetailsPage.UploadSignedAgreementDocs();
            backOfficeAppDetailsPage.ContractVerification();
            backOfficeAppDetailsPage.FundingVerification();
            Assert.assertEquals("Approved For Funding", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_DefaultContent() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_DefaultContent";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_Fraud() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_Fraud";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_Slowpay() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_Slowpay";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_SplitPayer() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_SplitPayer";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
		
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_StackingHistory() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_StackingHistory";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_Other() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_Other";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void merchantVerificationDeclinedFlow_CriminalHistory() throws Exception {
        String sTestID = "merchantVerificationDeclinedFlow_CriminalHistory";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    @Test(description = "", groups = {"PortalTests","Sanity","Sprint1"},enabled = true)
    public void whitePages_BusinessIdentityVerification() throws Exception {
        String sTestID = "whitePages- BusinessIdentityVerification";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String bankModification = "no";
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            wait.until(ExpectedConditions.visibilityOfElementLocated(portalFuncUtils.getLocator(uiObjParam.homepage_newApplicationLabel)));
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.submitApplication(driver,bankModification);
            appId = (driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_dashboard_appIdLabel)).getText()).split(":")[1];
            BorrowerHomePage borrowerHomePage1 = borrowerDashboardPage.logout(driver);
            System.out.println("---------------App ID----------> "+appId);
        	
            driver.get(PortalParam.backOfficeUrl);
            logger.info(PortalParam.backOfficeUrl +" loaded successfully.");
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(appId);             
            Assert.assertEquals("Declined ", backOfficeAppDetailsPage.BackOfficeGetStatus());
            result = "Passed";
        } catch (Exception e) {
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,Thread.currentThread().getStackTrace()[1].getMethodName(), result);
        }
    }   
    
    
    
}
