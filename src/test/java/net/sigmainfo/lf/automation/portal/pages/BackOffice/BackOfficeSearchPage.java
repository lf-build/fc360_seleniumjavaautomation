package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 19-01-2018.
 */
public class BackOfficeSearchPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BackOfficeSearchPage.class);

    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeSearchPage(WebDriver driver) throws Exception {
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown));
      //  PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_productIdTextBox));
        logger.info("=========== BackOfficeSearchPage is loaded============");
    }
    public BackOfficeSearchPage(){}

    public BackOfficeLoginPage logout(WebDriver driver) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_profileDropdown),"Profile");
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink)),"Logout link not clickable");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_header_logoutLink),"Log out");
        return new BackOfficeLoginPage(driver);
    }

    public static BackOfficeAppDetailsPage searchApp(String appId) throws Exception {
    	
    	System.out.println("----------@Test------------");
        PortalFuncUtils.waitForPageToLoad(driver);
       // assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Timed out waiting for application number textbox.");
       // assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Application number textbox not clickable within given time");
       // PortalFuncUtils.waitForTextToBePresent(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_productIdTextBox),"system");
        Thread.sleep(2000);
        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox), appId);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton), "Search");
        Thread.sleep(2000);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_SelectApp),"Select application details button.");
        Thread.sleep(20000);
        return new BackOfficeAppDetailsPage(driver);
    }
    
public static BackOfficeAppDetailsPage searchAppLOCApp(String appId) throws Exception {
    	
    	System.out.println("----------@Test------------");
        PortalFuncUtils.waitForPageToLoad(driver);
       // assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Timed out waiting for application number textbox.");
       // assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox)),"Application number textbox not clickable within given time");
       // PortalFuncUtils.waitForTextToBePresent(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_productIdTextBox),"system");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchMenu),"Select search menu.");
        
        
        Thread.sleep(2000);
        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_applicationNumberTextBox), appId);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_searchButton), "Search");
        Thread.sleep(2000);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_searchpage_SelectApp),"Select application details button.");
        Thread.sleep(20000);
        return new BackOfficeAppDetailsPage(driver);
    }

   

}

