package net.sigmainfo.lf.automation.common;

import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.api.config.ApiConfig;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.portal.config.PortalConfig;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalPropertiesReader;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import net.sigmainfo.lf.automation.portal.function.UIObjPropertiesReader;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.test.provider.BoolObj;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.testng.Assert;
import org.testng.annotations.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.fail;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : AbstractTests.java
 * Description          : Drives automation suite and delegates testng annotations
 * Includes             : 1. Setup and quit method for browser opening and closing for portal cases
 *                        2. Initializes test data and test property files
 *                        3. Custom Reporting methods
 */

@ContextConfiguration(classes = {PortalConfig.class, ApiConfig.class})
@TestExecutionListeners(inheritListeners = false, listeners = {
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@WebAppConfiguration
//@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {

    private Logger logger = LoggerFactory.getLogger(AbstractTests.class);

    static Server automationServer = new Server(9096);

    static boolean setupDone = false;
    public static boolean ifFileExist=false;
    public static long auto_start = 0;
    public static String Execution_start_time = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss").format(new Date());
    public static long auto_finish = 0;
    public static String sResBackUp = "res/TestReport.txt";
    public enum browser_list {
        IE, FF, Chrome;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver driver;
    public static int profile_delay = 20;
    public static int ShortSleep = 20;
    public static File casefile,portal_scenfile,portal_feafile,api_scenfile,api_feafile,feafile;
    String months[] = {"Jan", "Feb", "Mar", "Apr","May", "Jun", "Jul", "Aug", "Sep","Oct", "Nov", "Dec"};

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    public
    PortalParam portalParam;

    @Autowired
    public
    UIObjParam uiObjParam;

    @Autowired
    UIObjPropertiesReader uiPropertiesReader;

    @Autowired
    public
    PortalFuncUtils portalFuncUtils;

    @Autowired
    PortalPropertiesReader portalPropertiesReader;

    @BeforeSuite(alwaysRun = true)
    public void setUpOnce() throws Exception {
        logger.info("======================== Before Suite Invoked ==============================");
        setupJetty("FC360-automation", automationServer);
        logger.info("Jetty Server Started..........");
    }



    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("======================== After Suite Invoked. ==============================");
        automationServer.getServer().stop();
        logger.info("Jetty Server Stopped..........");
    }

    @PostConstruct
    private void postConstruct() throws SQLException,Exception {
        if (!setupDone) {
            logger.info("======================== Post Construct Invoked. ==============================");
            setupDBParams();
            setupUIObjects();
            setupDone = true;
        }
    }

    public void setupUIObjects() {
        logger.info("--------------- READING UI OBJECTS ----------------");
        uiObjParam.borrower_loginpage_loginLabel= uiPropertiesReader.getBorrower_loginpage_loginLabel();
        logger.info("uiObjParam.borrower_loginpage_loginLabel :" + uiObjParam.borrower_loginpage_loginLabel);
        uiObjParam.borrower_loginpage_emailTextBox= uiPropertiesReader.getBorrower_loginpage_emailTextBox();
        logger.info("uiObjParam.borrower_loginpage_emailTextBox :" + uiObjParam.borrower_loginpage_emailTextBox);
        uiObjParam.borrower_loginpage_passwordTextBox= uiPropertiesReader.getBorrower_loginpage_passwordTextBox();
        logger.info("uiObjParam.borrower_loginpage_passwordTextBox :" + uiObjParam.borrower_loginpage_passwordTextBox);
        uiObjParam.borrower_loginpage_submitButton= uiPropertiesReader.getBorrower_loginpage_submitButton();
        logger.info("uiObjParam.borrower_loginpage_submitButton :" + uiObjParam.borrower_loginpage_submitButton);
        uiObjParam.homepage_newApplicationLabel= uiPropertiesReader.getHomepage_newApplicationLabel();
        logger.info("uiObjParam.homepage_newApplicationLabel :" + uiObjParam.homepage_newApplicationLabel);
        uiObjParam.homepage_signinButton= uiPropertiesReader.getHomepage_signinButton();
        logger.info("uiObjParam.homepage_signinButton :" + uiObjParam.homepage_signinButton);
        uiObjParam.homepage_basicTab= uiPropertiesReader.getHomepage_basicTab();
        logger.info("uiObjParam.homepage_basicTab :" + uiObjParam.homepage_basicTab);
        uiObjParam.homepage_yourBusinessTab= uiPropertiesReader.getHomepage_yourBusinessTab();
        logger.info("uiObjParam.homepage_yourBusinessTab :" + uiObjParam.homepage_yourBusinessTab);
        uiObjParam.homepage_aboutYouTab= uiPropertiesReader.getHomepage_aboutYouTab();
        logger.info("uiObjParam.homepage_aboutYouTab :" + uiObjParam.homepage_aboutYouTab);
        uiObjParam.homepage_financialReviewTab= uiPropertiesReader.getHomepage_financialReviewTab();
        logger.info("uiObjParam.homepage_financialReviewTab :" + uiObjParam.homepage_financialReviewTab);
        uiObjParam.homepage_basicTab_businessNameTextBox= uiPropertiesReader.getHomepage_basicTab_businessNameTextBox();
        logger.info("uiObjParam.homepage_basicTab_businessNameTextBox :" + uiObjParam.homepage_basicTab_businessNameTextBox);
        uiObjParam.homepage_basicTab_amountSeekingTextBox= uiPropertiesReader.getHomepage_basicTab_amountSeekingTextBox();
        logger.info("uiObjParam.homepage_basicTab_amountSeekingTextBox :" + uiObjParam.homepage_basicTab_amountSeekingTextBox);
        uiObjParam.homepage_basicTab_useOfFundsDropdown= uiPropertiesReader.getHomepage_basicTab_useOfFundsDropdown();
        logger.info("uiObjParam.homepage_basicTab_useOfFundsDropdown :" + uiObjParam.homepage_basicTab_useOfFundsDropdown);
        uiObjParam.homepage_basicTab_annualBusinessRevenueTextBox= uiPropertiesReader.getHomepage_basicTab_annualBusinessRevenueTextBox();
        logger.info("uiObjParam.homepage_basicTab_annualBusinessRevenueTextBox :" + uiObjParam.homepage_basicTab_annualBusinessRevenueTextBox);
        uiObjParam.homepage_basicTab_firstNameTextBox= uiPropertiesReader.getHomepage_basicTab_firstNameTextBox();
        logger.info("uiObjParam.homepage_basicTab_firstNameTextBox :" + uiObjParam.homepage_basicTab_firstNameTextBox);
        uiObjParam.homepage_basicTab_lastNameTextBox= uiPropertiesReader.getHomepage_basicTab_lastNameTextBox();
        logger.info("uiObjParam.homepage_basicTab_lastNameTextBox :" + uiObjParam.homepage_basicTab_lastNameTextBox);
        uiObjParam.homepage_basicTab_phoneTextBox= uiPropertiesReader.getHomepage_basicTab_phoneTextBox();
        logger.info("uiObjParam.homepage_basicTab_phoneTextBox :" + uiObjParam.homepage_basicTab_phoneTextBox);
        uiObjParam.homepage_basicTab_emailTextBox= uiPropertiesReader.getHomepage_basicTab_emailTextBox();
        logger.info("uiObjParam.homepage_basicTab_emailTextBox :" + uiObjParam.homepage_basicTab_emailTextBox);
        uiObjParam.homepage_basicTab_confirmPasswordTextBox= uiPropertiesReader.getHomepage_basicTab_confirmPasswordTextBox();
        logger.info("uiObjParam.homepage_basicTab_confirmPasswordTextBox :" + uiObjParam.homepage_basicTab_confirmPasswordTextBox);
        uiObjParam.homepage_basicTab_passwordTextBox= uiPropertiesReader.getHomepage_basicTab_passwordTextBox();
        logger.info("uiObjParam.homepage_basicTab_passwordTextBox :" + uiObjParam.homepage_basicTab_passwordTextBox);
        uiObjParam.homepage_basicTab_signInButton= uiPropertiesReader.getHomepage_basicTab_signInButton();
        logger.info("uiObjParam.homepage_basicTab_signInButton :" + uiObjParam.homepage_basicTab_signInButton);
        uiObjParam.homepage_basicTab_startedApplicationLabel= uiPropertiesReader.getHomepage_basicTab_startedApplicationLabel();
        logger.info("uiObjParam.homepage_basicTab_startedApplicationLabel :" + uiObjParam.homepage_basicTab_startedApplicationLabel);
        uiObjParam.homepage_yourBusinessTab_businessLabel= uiPropertiesReader.getHomepage_yourBusinessTab_businessLabel();
        logger.info("uiObjParam.homepage_yourBusinessTab_businessLabel :" + uiObjParam.homepage_yourBusinessTab_businessLabel);
        uiObjParam.homepage_yourBusinessTab_businessAddressTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_businessAddressTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_businessAddressTextBox :" + uiObjParam.homepage_yourBusinessTab_businessAddressTextBox);
        uiObjParam.homepage_yourBusinessTab_dbaTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_dbaTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_dbaTextBox :" + uiObjParam.homepage_yourBusinessTab_dbaTextBox);
        uiObjParam.homepage_yourBusinessTab_cityTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_cityTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_cityTextBox :" + uiObjParam.homepage_yourBusinessTab_cityTextBox);
        uiObjParam.homepage_yourBusinessTab_stateDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_stateDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_stateDropdown :" + uiObjParam.homepage_yourBusinessTab_stateDropdown);
        uiObjParam.homepage_yourBusinessTab_zipCodeTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_zipCodeTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_zipCodeTextBox :" + uiObjParam.homepage_yourBusinessTab_zipCodeTextBox);
        uiObjParam.homepage_yourBusinessTab_businessPhoneTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_businessPhoneTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_businessPhoneTextBox :" + uiObjParam.homepage_yourBusinessTab_businessPhoneTextBox);
        uiObjParam.homepage_yourBusinessTab_bstMonthDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_bstMonthDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_bstMonthDropdown :" + uiObjParam.homepage_yourBusinessTab_bstMonthDropdown);
        uiObjParam.homepage_yourBusinessTab_bstDayDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_bstDayDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_bstDayDropdown :" + uiObjParam.homepage_yourBusinessTab_bstDayDropdown);
        uiObjParam.homepage_yourBusinessTab_bstYearDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_bstYearDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_bstYearDropdown :" + uiObjParam.homepage_yourBusinessTab_bstYearDropdown);
        uiObjParam.homepage_yourBusinessTab_businessWebsiteTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_businessWebsiteTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_businessWebsiteTextBox :" + uiObjParam.homepage_yourBusinessTab_businessWebsiteTextBox);
        uiObjParam.homepage_yourBusinessTab_legalEntityDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_legalEntityDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_legalEntityDropdown :" + uiObjParam.homepage_yourBusinessTab_legalEntityDropdown);
        uiObjParam.homepage_yourBusinessTab_businessLocationDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_businessLocationDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_businessLocationDropdown :" + uiObjParam.homepage_yourBusinessTab_businessLocationDropdown);
        uiObjParam.homepage_yourBusinessTab_federalSalesTaxTextBox= uiPropertiesReader.getHomepage_yourBusinessTab_federalSalesTaxTextBox();
        logger.info("uiObjParam.homepage_yourBusinessTab_federalSalesTaxTextBox :" + uiObjParam.homepage_yourBusinessTab_federalSalesTaxTextBox);
        uiObjParam.homepage_yourBusinessTab_industryDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_industryDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_industryDropdown :" + uiObjParam.homepage_yourBusinessTab_industryDropdown);
        uiObjParam.homepage_yourBusinessTab_importanceDropdown= uiPropertiesReader.getHomepage_yourBusinessTab_importanceDropdown();
        logger.info("uiObjParam.homepage_yourBusinessTab_importanceDropdown :" + uiObjParam.homepage_yourBusinessTab_importanceDropdown);
        uiObjParam.homepage_yourBusinessTab_continueButton= uiPropertiesReader.getHomepage_yourBusinessTab_continueButton();
        logger.info("uiObjParam.homepage_yourBusinessTab_continueButton :" + uiObjParam.homepage_yourBusinessTab_continueButton);
        uiObjParam.homepage_aboutYouTab_getToKnowLabel= uiPropertiesReader.getHomepage_aboutYouTab_getToKnowLabel();
        logger.info("uiObjParam.homepage_aboutYouTab_getToKnowLabel :" + uiObjParam.homepage_aboutYouTab_getToKnowLabel);
        uiObjParam.homepage_aboutYouTab_emailAddressTextBox= uiPropertiesReader.getHomepage_aboutYouTab_emailAddressTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_emailAddressTextBox :" + uiObjParam.homepage_aboutYouTab_emailAddressTextBox);
        uiObjParam.homepage_aboutYouTab_firstNameTextBox= uiPropertiesReader.getHomepage_aboutYouTab_firstNameTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_firstNameTextBox :" + uiObjParam.homepage_aboutYouTab_firstNameTextBox);
        uiObjParam.homepage_aboutYouTab_lastNameTextBox= uiPropertiesReader.getHomepage_aboutYouTab_lastNameTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_lastNameTextBox :" + uiObjParam.homepage_aboutYouTab_lastNameTextBox);
        uiObjParam.homepage_aboutYouTab_addressTextBox= uiPropertiesReader.getHomepage_aboutYouTab_addressTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_addressTextBox :" + uiObjParam.homepage_aboutYouTab_addressTextBox);
        uiObjParam.homepage_aboutYouTab_homeCityTextBox= uiPropertiesReader.getHomepage_aboutYouTab_homeCityTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_homeCityTextBox :" + uiObjParam.homepage_aboutYouTab_homeCityTextBox);
        uiObjParam.homepage_aboutYouTab_homeStateDropdown= uiPropertiesReader.getHomepage_aboutYouTab_homeStateDropdown();
        logger.info("uiObjParam.homepage_aboutYouTab_homeStateDropdown :" + uiObjParam.homepage_aboutYouTab_homeStateDropdown);
        uiObjParam.homepage_aboutYouTab_zipCodeTextBox= uiPropertiesReader.getHomepage_aboutYouTab_zipCodeTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_zipCodeTextBox :" + uiObjParam.homepage_aboutYouTab_zipCodeTextBox);
        uiObjParam.homepage_aboutYouTab_phoneNumberTextBox= uiPropertiesReader.getHomepage_aboutYouTab_phoneNumberTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_phoneNumberTextBox :" + uiObjParam.homepage_aboutYouTab_phoneNumberTextBox);
        uiObjParam.homepage_aboutYouTab_mobileNumberTextBox= uiPropertiesReader.getHomepage_aboutYouTab_mobileNumberTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_mobileNumberTextBox :" + uiObjParam.homepage_aboutYouTab_mobileNumberTextBox);
        uiObjParam.homepage_aboutYouTab_ownershipTextBox= uiPropertiesReader.getHomepage_aboutYouTab_ownershipTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_ownershipTextBox :" + uiObjParam.homepage_aboutYouTab_ownershipTextBox);
        uiObjParam.homepage_aboutYouTab_ssnTextBox= uiPropertiesReader.getHomepage_aboutYouTab_ssnTextBox();
        logger.info("uiObjParam.homepage_aboutYouTab_ssnTextBox :" + uiObjParam.homepage_aboutYouTab_ssnTextBox);
        uiObjParam.homepage_aboutYouTab_mobDropdown= uiPropertiesReader.getHomepage_aboutYouTab_mobDropdown();
        logger.info("uiObjParam.homepage_aboutYouTab_mobDropdown :" + uiObjParam.homepage_aboutYouTab_mobDropdown);
        uiObjParam.homepage_aboutYouTab_dobDropdown= uiPropertiesReader.getHomepage_aboutYouTab_dobDropdown();
        logger.info("uiObjParam.homepage_aboutYouTab_dobDropdown :" + uiObjParam.homepage_aboutYouTab_dobDropdown);
        uiObjParam.homepage_aboutYouTab_yobDropdown= uiPropertiesReader.getHomepage_aboutYouTab_yobDropdown();
        logger.info("uiObjParam.homepage_aboutYouTab_yobDropdown :" + uiObjParam.homepage_aboutYouTab_yobDropdown);
        uiObjParam.homepage_aboutYouTab_AcceptTC= uiPropertiesReader.getHomepage_aboutYouTab_AcceptTC();
        logger.info("uiObjParam.homepage_aboutYouTab_AcceptTC :" + uiObjParam.homepage_aboutYouTab_AcceptTC);      
        uiObjParam.homepage_aboutYouTab_continueButton= uiPropertiesReader.getHomepage_aboutYouTab_continueButton();
        logger.info("uiObjParam.homepage_aboutYouTab_continueButton :" + uiObjParam.homepage_aboutYouTab_continueButton);
        uiObjParam.homepage_financialReviewTab_selectOptionsLabel= uiPropertiesReader.getHomepage_financialReviewTab_selectOptionsLabel();
        logger.info("uiObjParam.homepage_financialReviewTab_selectOptionsLabel :" + uiObjParam.homepage_financialReviewTab_selectOptionsLabel);
        uiObjParam.homepage_financialReviewTab_linkBankAccountButton= uiPropertiesReader.getHomepage_financialReviewTab_linkBankAccountButton();
        logger.info("uiObjParam.homepage_financialReviewTab_linkBankAccountButton :" + uiObjParam.homepage_financialReviewTab_linkBankAccountButton);
        uiObjParam.homepage_financialReviewTab_skipButton= uiPropertiesReader.getHomepage_financialReviewTab_skipButton();
        logger.info("uiObjParam.homepage_financialReviewTab_skipButton :" + uiObjParam.homepage_financialReviewTab_skipButton);
        uiObjParam.homepage_basicTab_continueButton= uiPropertiesReader.getHomepage_basicTab_continueButton();
        logger.info("uiObjParam.homepage_basicTab_continueButton :" + uiObjParam.homepage_basicTab_continueButton);
        uiObjParam.selectBank_selectBankLabel= uiPropertiesReader.getSelectBank_selectBankLabel();
        logger.info("uiObjParam.selectBank_selectBankLabel :" + uiObjParam.selectBank_selectBankLabel);
        uiObjParam.selectBank_searchBankTextBox= uiPropertiesReader.getSelectBank_searchBankTextBox();
        logger.info("uiObjParam.selectBank_searchBankTextBox :" + uiObjParam.selectBank_searchBankTextBox);
        uiObjParam.selectBank_selectedBank= uiPropertiesReader.getSelectBank_selectedBank();
        logger.info("uiObjParam.selectBank_selectedBank :" + uiObjParam.selectBank_selectedBank);
        uiObjParam.selectBank_plaidUsernameTextBox= uiPropertiesReader.getSelectBank_plaidUsernameTextBox();
        logger.info("uiObjParam.selectBank_plaidUsernameTextBox :" + uiObjParam.selectBank_plaidUsernameTextBox);
        uiObjParam.selectBank_plaidPasswordTextBox= uiPropertiesReader.getSelectBank_plaidPasswordTextBox();
        logger.info("uiObjParam.selectBank_plaidPasswordTextBox :" + uiObjParam.selectBank_plaidPasswordTextBox);
        uiObjParam.selectBank_submitPlaidButton= uiPropertiesReader.getSelectBank_submitPlaidButton();
        logger.info("uiObjParam.selectBank_submitPlaidButton :" + uiObjParam.selectBank_submitPlaidButton);
        uiObjParam.selectBank_resultantBank= uiPropertiesReader.getSelectBank_resultantBank();
        logger.info("uiObjParam.selectBank_resultantBank :" + uiObjParam.selectBank_resultantBank);
        uiObjParam.selectBank_selectYourAccountLabel= uiPropertiesReader.getSelectBank_selectYourAccountLabel();
        logger.info("uiObjParam.selectBank_selectYourAccountLabel :" + uiObjParam.selectBank_selectYourAccountLabel);
        uiObjParam.selectBank_continueButton= uiPropertiesReader.getSelectBank_continueButton();
        logger.info("uiObjParam.selectBank_continueButton :" + uiObjParam.selectBank_continueButton);
        uiObjParam.selectBank_successfulConnectionLabel= uiPropertiesReader.getSelectBank_successfulConnectionLabel();
        logger.info("uiObjParam.selectBank_successfulConnectionLabel :" + uiObjParam.selectBank_successfulConnectionLabel);
        uiObjParam.homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel= uiPropertiesReader.getHomepage_financialReviewTab_bankLinkingSuccessfulMessageLabel();
        logger.info("uiObjParam.homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel :" + uiObjParam.homepage_financialReviewTab_bankLinkingSuccessfulMessageLabel);
        uiObjParam.homepage_financialReviewTab_finishButton= uiPropertiesReader.getHomepage_financialReviewTab_finishButton();
        logger.info("uiObjParam.homepage_financialReviewTab_finishButton :" + uiObjParam.homepage_financialReviewTab_finishButton);
        uiObjParam.homepage_financialReviewTab_reviewDocusignDocLabel= uiPropertiesReader.getHomepage_financialReviewTab_reviewDocusignDocLabel();
        logger.info("uiObjParam.homepage_financialReviewTab_reviewDocusignDocLabel :" + uiObjParam.homepage_financialReviewTab_reviewDocusignDocLabel);
        uiObjParam.homepage_financialReviewTab_agreementCheckBox= uiPropertiesReader.getHomepage_financialReviewTab_agreementCheckBox();
        logger.info("uiObjParam.homepage_financialReviewTab_agreementCheckBox :" + uiObjParam.homepage_financialReviewTab_agreementCheckBox);
        uiObjParam.homepage_financialReviewTab_continueButton= uiPropertiesReader.getHomepage_financialReviewTab_continueButton();
        logger.info("uiObjParam.homepage_financialReviewTab_continueButton :" + uiObjParam.homepage_financialReviewTab_continueButton);
        uiObjParam.dashboard_profileDropdown= uiPropertiesReader.getDashboard_profileDropdown();
        logger.info("uiObjParam.dashboard_profileDropdown :" + uiObjParam.dashboard_profileDropdown);
        uiObjParam.dashboard_logoutLink= uiPropertiesReader.getDashboard_logoutLink();
        logger.info("uiObjParam.dashboard_logoutLink :" + uiObjParam.dashboard_logoutLink);
        uiObjParam.homepage_financialReviewTab_docusign_okButton= uiPropertiesReader.getHomepage_financialReviewTab_docusign_okButton();
        logger.info("uiObjParam.homepage_financialReviewTab_docusign_okButton :" + uiObjParam.homepage_financialReviewTab_docusign_okButton);
        uiObjParam.homepage_financialReviewTab_docusign_startDocusignButton= uiPropertiesReader.getHomepage_financialReviewTab_docusign_startDocusignButton();
        logger.info("uiObjParam.homepage_financialReviewTab_docusign_startDocusignButton :" + uiObjParam.homepage_financialReviewTab_docusign_startDocusignButton);
        uiObjParam.homepage_financialReviewTab_docusign_signHereButton= uiPropertiesReader.getHomepage_financialReviewTab_docusign_signHereButton();
        logger.info("uiObjParam.homepage_financialReviewTab_docusign_signHereButton :" + uiObjParam.homepage_financialReviewTab_docusign_signHereButton);
        uiObjParam.homepage_financialReviewTab_docusign_adoptSignButton= uiPropertiesReader.getHomepage_financialReviewTab_docusign_adoptSignButton();
        logger.info("uiObjParam.homepage_financialReviewTab_docusign_adoptSignButton :" + uiObjParam.homepage_financialReviewTab_docusign_adoptSignButton);
        uiObjParam.homepage_financialReviewTab_docusign_finishButton= uiPropertiesReader.getHomepage_financialReviewTab_docusign_finishButton();
        logger.info("uiObjParam.homepage_financialReviewTab_docusign_finishButton :" + uiObjParam.homepage_financialReviewTab_docusign_finishButton);
        uiObjParam.borrower_accountLinkedMessageLabel= uiPropertiesReader.getBorrower_accountLinkedMessageLabel();
        logger.info("uiObjParam.borrower_accountLinkedMessageLabel :" + uiObjParam.borrower_accountLinkedMessageLabel);
        uiObjParam.borrower_purposeOfFundsTitle= uiPropertiesReader.getBorrower_purposeOfFundsTitle();
        logger.info("uiObjParam.borrower_purposeOfFundsTitle :" + uiObjParam.borrower_purposeOfFundsTitle);
        uiObjParam.borrower_dashboard_appIdLabel= uiPropertiesReader.getBorrower_dashboard_appIdLabel();
        logger.info("uiObjParam.borrower_dashboard_appIdLabel :" + uiObjParam.borrower_dashboard_appIdLabel);
        uiObjParam.backoffice_loginPage_usernameTextBox= uiPropertiesReader.getBackoffice_loginPage_usernameTextBox();
        logger.info("uiObjParam.backoffice_loginPage_usernameTextBox :" + uiObjParam.backoffice_loginPage_usernameTextBox);
        uiObjParam.backoffice_loginPage_passwordTextBox= uiPropertiesReader.getBackoffice_loginPage_passwordTextBox();
        logger.info("uiObjParam.backoffice_loginPage_passwordTextBox :" + uiObjParam.backoffice_loginPage_passwordTextBox);
        uiObjParam.backoffice_loginPage_agreementCheckbox= uiPropertiesReader.getBackoffice_loginPage_agreementCheckbox();
        logger.info("uiObjParam.backoffice_loginPage_agreementCheckbox :" + uiObjParam.backoffice_loginPage_agreementCheckbox);
        uiObjParam.backoffice_loginPage_forgetPasswordLink= uiPropertiesReader.getBackoffice_loginPage_forgetPasswordLink();
        logger.info("uiObjParam.backoffice_loginPage_forgetPasswordLink :" + uiObjParam.backoffice_loginPage_forgetPasswordLink);
        uiObjParam.backoffice_loginPage_loginButton= uiPropertiesReader.getBackoffice_loginPage_loginButton();
        logger.info("uiObjParam.backoffice_loginPage_loginButton :" + uiObjParam.backoffice_loginPage_loginButton);
        uiObjParam.backoffice_searchpage_SelectApp= uiPropertiesReader.getBackoffice_searchpage_SelectApp();
        logger.info("uiObjParam.backoffice_searchpage_SelectApp :" + uiObjParam.backoffice_searchpage_SelectApp);
        
        uiObjParam.backoffice_header_profileDropdown= uiPropertiesReader.getBackoffice_header_profileDropdown();
        logger.info("uiObjParam.backoffice_header_profileDropdown :" + uiObjParam.backoffice_header_profileDropdown);
        uiObjParam.backoffice_header_logoutLink= uiPropertiesReader.getBackoffice_header_logoutLink();
        logger.info("uiObjParam.backoffice_header_logoutLink :" + uiObjParam.backoffice_header_logoutLink);
        uiObjParam.backoffice_header_logoutLink= uiPropertiesReader.getBackoffice_header_logoutLink();
        logger.info("uiObjParam.backoffice_searchpage_productIdTextBox :" + uiObjParam.backoffice_searchpage_productIdTextBox);
        uiObjParam.backoffice_searchpage_applicationNumberTextBox= uiPropertiesReader.getBackoffice_searchpage_applicationNumberTextBox();
        logger.info("uiObjParam.backoffice_searchpage_applicationNumberTextBox :" + uiObjParam.backoffice_searchpage_applicationNumberTextBox);
        uiObjParam.backoffice_searchpage_searchButton= uiPropertiesReader.getBackoffice_searchpage_searchButton();
        logger.info("uiObjParam.backoffice_searchpage_searchButton :" + uiObjParam.backoffice_searchpage_searchButton);
        uiObjParam.homepage_financialReviewTab_completeApplicationButton= uiPropertiesReader.getHomepage_financialReviewTab_completeApplicationButton();
        logger.info("uiObjParam.homepage_financialReviewTab_completeApplicationButton :" + uiObjParam.homepage_financialReviewTab_completeApplicationButton);

        uiObjParam.backoffice_homepage_actionsButton= uiPropertiesReader.getBackoffice_homepage_actionsButton();
        logger.info("uiObjParam.backoffice_homepage_actionsButton :" + uiObjParam.backoffice_homepage_actionsButton);
        uiObjParam.backoffice_homepage_actions_uploadBankStatementButton= uiPropertiesReader.getBackoffice_homepage_actions_uploadBankStatementButton();
        logger.info("uiObjParam.backoffice_homepage_actions_uploadBankStatementButton :" + uiObjParam.backoffice_homepage_actions_uploadBankStatementButton);
        uiObjParam.backoffice_homepage_actions_bankNameTextBox= uiPropertiesReader.getBackoffice_homepage_actions_bankNameTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_bankNameTextBox :" + uiObjParam.backoffice_homepage_actions_bankNameTextBox);
       
        uiObjParam.backoffice_homepage_actions_bankACNumTextBox= uiPropertiesReader.getBackoffice_homepage_actions_bankACNumTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_bankACNumTextBox :" + uiObjParam.backoffice_homepage_actions_bankACNumTextBox);
        uiObjParam.backoffice_homepage_actions_bankACTypeTextBox= uiPropertiesReader.getBackoffice_homepage_actions_bankACTypeTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_bankACTypeTextBox :" + uiObjParam.backoffice_homepage_actions_bankACTypeTextBox);
        uiObjParam.backoffice_homepage_actions_bankStatementUpload= uiPropertiesReader.getBackoffice_homepage_actions_bankStatementUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_bankStatementUpload :" + uiObjParam.backoffice_homepage_actions_bankStatementUpload);
        uiObjParam.backoffice_homepage_actions_bankStatementSubmit= uiPropertiesReader.getBackoffice_homepage_actions_bankStatementSubmit();
        logger.info("uiObjParam.backoffice_homepage_actions_bankStatementSubmit :" + uiObjParam.backoffice_homepage_actions_bankStatementSubmit);
        uiObjParam.backoffice_homepage_actions_CashFlowTab= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowTab();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowTab :" + uiObjParam.backoffice_homepage_actions_CashFlowTab);
        uiObjParam.backoffice_homepage_actions_CashFlowSelectAC= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowSelectAC();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowSelectAC :" + uiObjParam.backoffice_homepage_actions_CashFlowSelectAC);
      
        uiObjParam.backoffice_homepage_actions_SelectCashFlowAC= uiPropertiesReader.getBackoffice_homepage_actions_SelectCashFlowAC();
        logger.info("uiObjParam.backoffice_homepage_actions_SelectCashFlowAC :" + uiObjParam.backoffice_homepage_actions_SelectCashFlowAC);
        uiObjParam.backoffice_homepage_actions_SelectFundingAC= uiPropertiesReader.getBackoffice_homepage_actions_SelectFundingAC();
        logger.info("uiObjParam.backoffice_homepage_actions_SelectFundingAC :" + uiObjParam.backoffice_homepage_actions_SelectFundingAC);
        uiObjParam.backoffice_homepage_actions_DashBoardTab= uiPropertiesReader.getBackoffice_homepage_actions_DashBoardTab();
        logger.info("uiObjParam.backoffice_homepage_actions_DashBoardTab :" + uiObjParam.backoffice_homepage_actions_DashBoardTab);
        uiObjParam.backoffice_homepage_actions_CashFlowDetailsButton= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowDetailsButton();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowDetailsButton :" + uiObjParam.backoffice_homepage_actions_CashFlowDetailsButton);
        uiObjParam.backoffice_homepage_actions_CashFlowSelectProduct= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowSelectProduct();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowSelectProduct :" + uiObjParam.backoffice_homepage_actions_CashFlowSelectProduct);
        uiObjParam.backoffice_homepage_actions_verifyCheckbox= uiPropertiesReader.getBackoffice_homepage_actions_verifyCheckbox();
        logger.info("uiObjParam.backoffice_homepage_actions_verifyCheckbox :" + uiObjParam.backoffice_homepage_actions_verifyCheckbox);
        uiObjParam.backoffice_homepage_actions_CashFlowVerifySubmit= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowVerifySubmit();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowVerifySubmit :" + uiObjParam.backoffice_homepage_actions_CashFlowVerifySubmit);
        uiObjParam.backoffice_homepage_actions_CashFlowCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_CashFlowCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_CashFlowCloseButton :" + uiObjParam.backoffice_homepage_actions_CashFlowCloseButton);
       
        uiObjParam.backoffice_homepage_actions_decideProduct= uiPropertiesReader.getBackoffice_homepage_actions_decideProduct();
        logger.info("uiObjParam.backoffice_homepage_actions_decideProduct :" + uiObjParam.backoffice_homepage_actions_decideProduct);
        uiObjParam.backoffice_homepage_actions_moveProduct= uiPropertiesReader.getBackoffice_homepage_actions_moveProduct();
        logger.info("uiObjParam.backoffice_homepage_actions_moveProduct :" + uiObjParam.backoffice_homepage_actions_moveProduct);
        uiObjParam.backoffice_homepage_actions_selectListproduct= uiPropertiesReader.getBackoffice_homepage_actions_selectListproduct();
        logger.info("uiObjParam.backoffice_homepage_actions_selectListproduct :" + uiObjParam.backoffice_homepage_actions_selectListproduct);
      
        uiObjParam.backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_PaymentFrequencyTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_ProgramTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_ProgramTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_ProgramTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_ProgramTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_FactorTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_FactorTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_FactorTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_FactorTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_TermsTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_TermsTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_TermsTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_TermsTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_PaymentTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_PaymentTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_PaymentTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_PaymentTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_ApprovedAmtTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedAmtTextBox);
       
        uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_ApprovedDateTimeTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_SubmitedDateTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_SubmitedDateTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_SubmitedDateTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_SubmitedDateTextBox);
        
        
        uiObjParam.backoffice_homepage_actions_OfferGen_PSFpercentageTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_PSFpercentageTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_PSFpercentageTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_PSFpercentageTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_FunderFeeTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_FunderFeeTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_FunderFeeTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_FunderFeeTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_CompPercentageTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_CompPercentageTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_CompPercentageTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_CompPercentageTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_MaxCrossPersentageTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_ProductTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_ProductTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_ProductTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_ProductTextBox);
        uiObjParam.backoffice_homepage_actions_OfferGen_CommentsTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OfferGen_CommentsTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferGen_CommentsTextBox :" + uiObjParam.backoffice_homepage_actions_OfferGen_CommentsTextBox);   
        uiObjParam.backoffice_homepage_actions_OfferSubmit= uiPropertiesReader.getBackoffice_homepage_actions_OfferSubmit();
        logger.info("uiObjParam.backoffice_homepage_actions_OfferSubmit :" + uiObjParam.backoffice_homepage_actions_OfferSubmit);
        uiObjParam.backoffice_homepage_actions_UploadDocs= uiPropertiesReader.getBackoffice_homepage_actions_UploadDocs();
        logger.info("uiObjParam.backoffice_homepage_actions_UploadDocs :" + uiObjParam.backoffice_homepage_actions_UploadDocs);      
        uiObjParam.backoffice_homepage_actions_PhotoIDUpload= uiPropertiesReader.getBackoffice_homepage_actions_PhotoIDUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_PhotoIDUpload :" + uiObjParam.backoffice_homepage_actions_PhotoIDUpload);        
        uiObjParam.backoffice_homepage_actions_OwnershipUpload= uiPropertiesReader.getBackoffice_homepage_actions_OwnershipUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnershipUpload :" + uiObjParam.backoffice_homepage_actions_OwnershipUpload);        
        uiObjParam.backoffice_homepage_actions_ProofOfRentUpload= uiPropertiesReader.getBackoffice_homepage_actions_ProofOfRentUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_ProofOfRentUpload :" + uiObjParam.backoffice_homepage_actions_ProofOfRentUpload);        
        uiObjParam.backoffice_homepage_actions_VoidedCheckUpload= uiPropertiesReader.getBackoffice_homepage_actions_VoidedCheckUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_VoidedCheckUpload :" + uiObjParam.backoffice_homepage_actions_VoidedCheckUpload);
     
        uiObjParam.backoffice_homepage_actions_UploadDocsClose= uiPropertiesReader.getBackoffice_homepage_actions_UploadDocsClose();
        logger.info("uiObjParam.backoffice_homepage_actions_UploadDocsClose :" + uiObjParam.backoffice_homepage_actions_UploadDocsClose);
     
        
        uiObjParam.backoffice_homepage_actions_BankDetails= uiPropertiesReader.getBackoffice_homepage_actions_BankDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_BankDetails :" + uiObjParam.backoffice_homepage_actions_BankDetails);
        
        uiObjParam.backoffice_homepage_actions_BankAccNumTextBox= uiPropertiesReader.getBackoffice_homepage_actions_BankAccNumTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_BankAccNumTextBox :" + uiObjParam.backoffice_homepage_actions_BankAccNumTextBox);
        
        uiObjParam.backoffice_homepage_actions_BankRoutingNumTextBox= uiPropertiesReader.getBackoffice_homepage_actions_BankRoutingNumTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_BankRoutingNumTextBox :" + uiObjParam.backoffice_homepage_actions_BankRoutingNumTextBox);
        
        uiObjParam.backoffice_homepage_actions_BankverifyCheckbox= uiPropertiesReader.getBackoffice_homepage_actions_BankverifyCheckbox();
        logger.info("uiObjParam.backoffice_homepage_actions_BankverifyCheckbox :" + uiObjParam.backoffice_homepage_actions_BankverifyCheckbox);
     
        uiObjParam.backoffice_homepage_actions_BankVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_BankVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_BankVerifyButton :" + uiObjParam.backoffice_homepage_actions_BankVerifyButton);
     
        uiObjParam.backoffice_homepage_actions_BankVerifyCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_BankVerifyCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_BankVerifyCloseButton :" + uiObjParam.backoffice_homepage_actions_BankVerifyCloseButton);
        
        uiObjParam.backoffice_homepage_actions_IDVerifyDetails= uiPropertiesReader.getBackoffice_homepage_actions_IDVerifyDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_IDVerifyDetails :" + uiObjParam.backoffice_homepage_actions_IDVerifyDetails);
        
        uiObjParam.backoffice_homepage_actions_IDverifyCheckbox1= uiPropertiesReader.getBackoffice_homepage_actions_IDverifyCheckbox1();
        logger.info("uiObjParam.backoffice_homepage_actions_IDverifyCheckbox1 :" + uiObjParam.backoffice_homepage_actions_IDverifyCheckbox1);
        uiObjParam.backoffice_homepage_actions_IDverifyCheckbox2= uiPropertiesReader.getBackoffice_homepage_actions_IDverifyCheckbox2();
        logger.info("uiObjParam.backoffice_homepage_actions_IDverifyCheckbox2 :" + uiObjParam.backoffice_homepage_actions_IDverifyCheckbox2);
        uiObjParam.backoffice_homepage_actions_IDverifyCheckbox3= uiPropertiesReader.getBackoffice_homepage_actions_IDverifyCheckbox3();
        logger.info("uiObjParam.backoffice_homepage_actions_IDverifyCheckbox3 :" + uiObjParam.backoffice_homepage_actions_IDverifyCheckbox3);
        
        uiObjParam.backoffice_homepage_actions_IDVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_IDVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_IDVerifyButton :" + uiObjParam.backoffice_homepage_actions_IDVerifyButton);
        
        uiObjParam.backoffice_homepage_actions_IDVerifyCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_IDVerifyCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_IDVerifyCloseButton :" + uiObjParam.backoffice_homepage_actions_IDVerifyCloseButton);
     
        uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetails= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerVerifyDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetails :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetails);
     
        uiObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerverifyCheckbox1();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1 :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerverifyCheckbox1);

        uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyButton :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyButton);
       
        uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerVerifyCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButton :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButton);
     
        uiObjParam.backoffice_homepage_actions_RentDetails= uiPropertiesReader.getBackoffice_homepage_actions_RentDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_RentDetails :" + uiObjParam.backoffice_homepage_actions_RentDetails);
     
        uiObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OwnerPhoneNumTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox :" + uiObjParam.backoffice_homepage_actions_OwnerPhoneNumTextBox);
     
        uiObjParam.backoffice_homepage_actions_OwnerNameTextBox= uiPropertiesReader.getBackoffice_homepage_actions_OwnerNameTextBox();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnerNameTextBox :" + uiObjParam.backoffice_homepage_actions_OwnerNameTextBox);
        
        uiObjParam.backoffice_homepage_actions_OwnerVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_OwnerVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnerVerifyButton :" + uiObjParam.backoffice_homepage_actions_OwnerVerifyButton);
       
        uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_OwnerVerifyCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButton :" + uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButton);
     
        uiObjParam.backoffice_homepage_actions_GenerateAgreement= uiPropertiesReader.getBackoffice_homepage_actions_GenerateAgreement();
        logger.info("uiObjParam.backoffice_homepage_actions_GenerateAgreement :" + uiObjParam.backoffice_homepage_actions_GenerateAgreement);
       
        uiObjParam.backoffice_homepage_actions_GenAgreePrepayCheck= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreePrepayCheck();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreePrepayCheck :" + uiObjParam.backoffice_homepage_actions_GenAgreePrepayCheck);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeFirstRepurchasePrice= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeFirstRepurchasePrice();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeFirstRepurchasePrice :" + uiObjParam.backoffice_homepage_actions_GenAgreeFirstRepurchasePrice);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeSecondRepurchasePrice= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeSecondRepurchasePrice();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeSecondRepurchasePrice :" + uiObjParam.backoffice_homepage_actions_GenAgreeSecondRepurchasePrice);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeThirdRepurchasePrice= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeThirdRepurchasePrice();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeThirdRepurchasePrice :" + uiObjParam.backoffice_homepage_actions_GenAgreeThirdRepurchasePrice);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeFourthRepurchasePrice= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeFourthRepurchasePrice();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeFourthRepurchasePrice :" + uiObjParam.backoffice_homepage_actions_GenAgreeFourthRepurchasePrice);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeFifthRepurchasePrice= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeFifthRepurchasePrice();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeFifthRepurchasePrice :" + uiObjParam.backoffice_homepage_actions_GenAgreeFifthRepurchasePrice);
     
        uiObjParam.backoffice_homepage_actions_GenAgreeNoStackCheck= uiPropertiesReader.getBackoffice_homepage_actions_GenAgreeNoStackCheck();
        logger.info("uiObjParam.backoffice_homepage_actions_GenAgreeNoStackCheck :" + uiObjParam.backoffice_homepage_actions_GenAgreeNoStackCheck);
     
        uiObjParam.backoffice_homepage_GenAgreeButton= uiPropertiesReader.getBackoffice_homepage_GenAgreeButton();
        logger.info("uiObjParam.backoffice_homepage_GenAgreeButton :" + uiObjParam.backoffice_homepage_GenAgreeButton);
     
        uiObjParam.backoffice_homepage_actions_SendAgreement= uiPropertiesReader.getBackoffice_homepage_actions_SendAgreement();
        logger.info("uiObjParam.backoffice_homepage_actions_SendAgreement :" + uiObjParam.backoffice_homepage_actions_SendAgreement);
     
        uiObjParam.backoffice_homepage_actions_VerifySendAgreement= uiPropertiesReader.getBackoffice_homepage_actions_VerifySendAgreement();
        logger.info("uiObjParam.backoffice_homepage_actions_VerifySendAgreement :" + uiObjParam.backoffice_homepage_actions_VerifySendAgreement);
     
        uiObjParam.backoffice_homepage_actions_SignedAgreementUpload= uiPropertiesReader.getBackoffice_homepage_actions_SignedAgreementUpload();
        logger.info("uiObjParam.backoffice_homepage_actions_SignedAgreementUpload :" + uiObjParam.backoffice_homepage_actions_SignedAgreementUpload);
     
        uiObjParam.backoffice_homepage_actions_ContractVerifyDetails= uiPropertiesReader.getBackoffice_homepage_actions_ContractVerifyDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractVerifyDetails :" + uiObjParam.backoffice_homepage_actions_ContractVerifyDetails);
     
        uiObjParam.backoffice_homepage_actions_ContractOwnerverifyCheckbox1= uiPropertiesReader.getBackoffice_homepage_actions_ContractOwnerverifyCheckbox1();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractOwnerverifyCheckbox1 :" + uiObjParam.backoffice_homepage_actions_ContractOwnerverifyCheckbox1);
        
        uiObjParam.backoffice_homepage_actions_ContractOwnerVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_ContractOwnerVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractOwnerVerifyButton :" + uiObjParam.backoffice_homepage_actions_ContractOwnerVerifyButton);
        
        uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_ContractOwnerCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButton :" + uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButton);
        
        uiObjParam.backoffice_homepage_actions_FundingVerifyDetails= uiPropertiesReader.getBackoffice_homepage_actions_FundingVerifyDetails();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingVerifyDetails :" + uiObjParam.backoffice_homepage_actions_FundingVerifyDetails);
     
        uiObjParam.backoffice_homepage_actions_FundingOwnerverifyCheckbox1= uiPropertiesReader.getBackoffice_homepage_actions_FundingOwnerverifyCheckbox1();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingOwnerverifyCheckbox1 :" + uiObjParam.backoffice_homepage_actions_FundingOwnerverifyCheckbox1);
        
        uiObjParam.backoffice_homepage_actions_FundingOwnerVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_FundingOwnerVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingOwnerVerifyButton :" + uiObjParam.backoffice_homepage_actions_FundingOwnerVerifyButton);
        
        uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButton= uiPropertiesReader.getBackoffice_homepage_actions_FundingOwnerCloseButton();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButton :" + uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButton);
        
        uiObjParam.backoffice_searchpage_searchMenu= uiPropertiesReader.getBackoffice_searchpage_searchMenu();
        logger.info("uiObjParam.backoffice_searchpage_searchMenu :" + uiObjParam.backoffice_searchpage_searchMenu);
        
        uiObjParam.backoffice_homepage_actions_IDVerifyDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_IDVerifyDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_IDVerifyDetailsLoc :" + uiObjParam.backoffice_homepage_actions_IDVerifyDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_RentDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_RentDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_RentDetailsLoc :" + uiObjParam.backoffice_homepage_actions_RentDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_BankDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_BankDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_BankDetailsLoc :" + uiObjParam.backoffice_homepage_actions_BankDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_FundingVerifyDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_FundingVerifyDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingVerifyDetailsLoc :" + uiObjParam.backoffice_homepage_actions_FundingVerifyDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_ContractVerifyDetailsLoc= uiPropertiesReader.getBackoffice_homepage_actions_ContractVerifyDetailsLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractVerifyDetailsLoc :" + uiObjParam.backoffice_homepage_actions_ContractVerifyDetailsLoc);
        
        uiObjParam.backoffice_homepage_actions_BankVerifyCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_BankVerifyCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_BankVerifyCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_BankVerifyCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_IDVerifyCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_IDVerifyCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_IDVerifyCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_IDVerifyCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_BusinessOwnerVerifyCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_OwnerVerifyCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_OwnerVerifyCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_ContractOwnerCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_ContractOwnerCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButtonLoc= uiPropertiesReader.getBackoffice_homepage_actions_FundingOwnerCloseButtonLoc();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButtonLoc :" + uiObjParam.backoffice_homepage_actions_FundingOwnerCloseButtonLoc);
       
        uiObjParam.backoffice_homepage_actions_FundingOwnerInitiateButton= uiPropertiesReader.getBackoffice_homepage_actions_FundingOwnerInitiateButton();
        logger.info("uiObjParam.backoffice_homepage_actions_FundingOwnerInitiateButton :" + uiObjParam.backoffice_homepage_actions_FundingOwnerInitiateButton);
      
        uiObjParam.backoffice_homepage_actions_applicationDataVerification= uiPropertiesReader.getBackoffice_homepage_actions_applicationDataVerification();
        logger.info("uiObjParam.backoffice_homepage_actions_applicationDataVerification :" + uiObjParam.backoffice_homepage_actions_applicationDataVerification);
      
        uiObjParam.backoffice_homepage_actions_applicationDataCheck= uiPropertiesReader.getBackoffice_homepage_actions_applicationDataCheck();
        logger.info("uiObjParam.backoffice_homepage_actions_applicationDataCheck :" + uiObjParam.backoffice_homepage_actions_applicationDataCheck);
      
        uiObjParam.backoffice_homepage_actions_applicationDataVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_applicationDataVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_applicationDataVerifyButton :" + uiObjParam.backoffice_homepage_actions_applicationDataVerifyButton);
      
        uiObjParam.backoffice_homepage_actions_businessSocialVerification= uiPropertiesReader.getBackoffice_homepage_actions_businessSocialVerification();
        logger.info("uiObjParam.backoffice_homepage_actions_businessSocialVerification :" + uiObjParam.backoffice_homepage_actions_businessSocialVerification);
      
        uiObjParam.backoffice_homepage_actions_businessSocialVerifyCheck= uiPropertiesReader.getBackoffice_homepage_actions_businessSocialVerifyCheck();
        logger.info("uiObjParam.backoffice_homepage_actions_businessSocialVerifyCheck :" + uiObjParam.backoffice_homepage_actions_businessSocialVerifyCheck);
      
        uiObjParam.backoffice_homepage_actions_businessSocialVerifyButton= uiPropertiesReader.getBackoffice_homepage_actions_businessSocialVerifyButton();
        logger.info("uiObjParam.backoffice_homepage_actions_businessSocialVerifyButton :" + uiObjParam.backoffice_homepage_actions_businessSocialVerifyButton);
        
        uiObjParam.backoffice_homepage_actions_ApplicationStatus= uiPropertiesReader.getBackoffice_homepage_actions_ApplicationStatus();
        logger.info("uiObjParam.backoffice_homepage_actions_ApplicationStatus :" + uiObjParam.backoffice_homepage_actions_ApplicationStatus);
            
    }

    public void setupDBParams() {

        logger.info("--------------- READING PORTAL PROPERTIES FILE ----------------");

        portalParam.borrowerUrl = portalPropertiesReader.getBorrowerUrl();
        logger.info("portalParam.borrowerUrl :" + portalParam.borrowerUrl);
        portalParam.backOfficeUrl = portalPropertiesReader.getBackOfficeUrl();
        logger.info("portalParam.backOfficeUrl :" + portalParam.backOfficeUrl);
        portalParam.backofficeUsername = portalPropertiesReader.getBackOfficeUsername();
        logger.info("portalParam.backofficeUsername :" + portalParam.backofficeUsername);
        portalParam.backofficePassword = portalPropertiesReader.getBackOfficePassword();
        logger.info("portalParam.backofficePassword :" + portalParam.backofficePassword);
        portalParam.custom_report_location = portalPropertiesReader.getCustom_report_location();
        logger.info("portalParam.custom_report_location :" + portalParam.custom_report_location);
        portalParam.browser = portalPropertiesReader.getBrowser();
        logger.info("portalParam.browser :" + portalParam.browser);
        portalParam.gmailId = portalPropertiesReader.getGmailId();
        logger.info("portalParam.gmailId :" + portalParam.gmailId);
        portalParam.gmailPassword = portalPropertiesReader.getGmailPassword();
        logger.info("portalParam.gmailPassword :" + portalParam.gmailPassword);
        portalParam.plaid_user = portalPropertiesReader.getPlaid_user();
        logger.info("portalParam.plaid_user :" + portalParam.plaid_user);
        portalParam.plaid_password = portalPropertiesReader.getPlaid_password();
        logger.info("portalParam.plaid_password :" + portalParam.plaid_password);
        portalParam.upload_file_location = portalPropertiesReader.getUpload_file_location();
        logger.info("portalParam.upload_file_location :" + portalParam.upload_file_location);
        portalParam.iframeSource = portalPropertiesReader.getIframeSource();
        logger.info("portalParam.iframeSource :" + portalParam.iframeSource);
        portalParam.bankToConnect = portalPropertiesReader.getBankToConnect();
        logger.info("portalParam.bankToConnect :" + portalParam.bankToConnect);

    }

    private void setupJetty(final String contextRoot, Server server) throws Exception {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(ApiConfig.class,PortalConfig.class);
//        applicationContext.register(SmppConfig.class);
        final ServletHolder servletHolder = new ServletHolder(new DispatcherServlet(applicationContext));
        final ServletContextHandler context = new ServletContextHandler();
        context.setErrorHandler(null);
        context.setContextPath("/" + contextRoot);
        context.addServlet(servletHolder, "/*");

        server.setHandler(context);
        server.start();
    }

    @Test(groups = { "Result", "", "" }, description = "Results file creation")
    public static void AAResultsBackUp() throws Exception {
        System.out.println("*****Begining of ResultsBackUp *********************");

        try {
            TestResults.CreateResultTxt();
            //TestResults.ExportResultToTxt("Portal Url:=",GlobalVariables.portalUrl);
            Assert.assertTrue(true, "ResultsBackUp test case has been Passed.");
            System.out.println("*****Ending of ResultsBackUp *********************");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception happened in executing test case ResultsBackUp");
            fail("ResultsBackUp test case has been failed.");
        }
    }

    protected void initializeData(String FuncMod, String Test_ID) throws Exception {

        try {

            logger.info("Reading test data from " + FuncMod + " worksheet.");
            logger.info("-----------------------------------------------");

            if (FuncMod.equalsIgnoreCase("FC360")) {
                portalParam.testId = portalFuncUtils.getTestData(FuncMod, Test_ID, "Test_ID");
                logger.info("portalParam.testId: " + portalParam.testId);
                portalParam.businessName = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessName");
                logger.info("portalParam.businessName: " + portalParam.businessName);
                portalParam.amountSeeking = portalFuncUtils.getTestData(FuncMod, Test_ID, "amountSeeking");
                logger.info("portalParam.amountSeeking: " + portalParam.amountSeeking);
                portalParam.useOfFunds = portalFuncUtils.getTestData(FuncMod, Test_ID, "useOfFunds");
                logger.info("portalParam.useOfFunds: " + portalParam.useOfFunds);
                portalParam.annualBusinessRevenue = portalFuncUtils.getTestData(FuncMod, Test_ID, "annualBusinessRevenue");
                logger.info("portalParam.annualBusinessRevenue: " + portalParam.annualBusinessRevenue);
                portalParam.firstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstName");
                logger.info("portalParam.firstName: " + portalParam.firstName);
                portalParam.lastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "lastName");
                logger.info("portalParam.lastName: " + portalParam.lastName);
                String inviteId = new SimpleDateFormat("ddMMHHmmss").format(Calendar.getInstance().getTime());
                portalParam.username = "shaishav.sigma"+"+"+inviteId+"@gmail.com";
                portalParam.password = portalPropertiesReader.getEncryptedPassword();
                logger.info("portalParam.password: " + portalParam.password);
                portalParam.phone = portalFuncUtils.getTestData(FuncMod, Test_ID, "phone");
                logger.info("portalParam.phone: " + portalParam.phone);
                portalParam.password = portalFuncUtils.getTestData(FuncMod, Test_ID, "password");
                logger.info("portalParam.password: " + portalParam.password);
                portalParam.dba = portalFuncUtils.getTestData(FuncMod, Test_ID, "dba");
                logger.info("portalParam.dba: " + portalParam.dba);
                portalParam.businessAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessAddress");
                logger.info("portalParam.businessAddress: " + portalParam.businessAddress);
                portalParam.city = portalFuncUtils.getTestData(FuncMod, Test_ID, "city");
                logger.info("portalParam.city: " + portalParam.city);
                portalParam.state = portalFuncUtils.getTestData(FuncMod, Test_ID, "state");
                logger.info("portalParam.state: " + portalParam.state);
                portalParam.zipCode = portalFuncUtils.getTestData(FuncMod, Test_ID, "zipCode");
                logger.info("portalParam.zipCode: " + portalParam.zipCode);
                portalParam.businessPhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "businessPhone");
                logger.info("portalParam.businessPhone: " + portalParam.businessPhone);
                portalParam.bsMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "bsMonth");
                logger.info("portalParam.bsMonth: " + portalParam.bsMonth);
                portalParam.bsDay = portalFuncUtils.getTestData(FuncMod, Test_ID, "bsDay");
                logger.info("portalParam.bsDay: " + portalParam.bsDay);
                portalParam.bsYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "bsYear");
                logger.info("portalParam.bsYear: " + portalParam.bsYear);
                portalParam.website = portalFuncUtils.getTestData(FuncMod, Test_ID, "website");
                logger.info("portalParam.website: " + portalParam.website);
                portalParam.legalEntity = portalFuncUtils.getTestData(FuncMod, Test_ID, "legalEntity");
                logger.info("portalParam.legalEntity: " + portalParam.legalEntity);
                portalParam.location = portalFuncUtils.getTestData(FuncMod, Test_ID, "location");
                logger.info("portalParam.location: " + portalParam.location);
                portalParam.taxId = portalFuncUtils.getTestData(FuncMod, Test_ID, "taxId");
                logger.info("portalParam.taxId: " + portalParam.taxId);
                portalParam.industry = portalFuncUtils.getTestData(FuncMod, Test_ID, "industry");
                logger.info("portalParam.industry: " + portalParam.industry);
                portalParam.importance = portalFuncUtils.getTestData(FuncMod, Test_ID, "importance");
                logger.info("portalParam.importance: " + portalParam.importance);
                portalParam.homeAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeAddress");
                logger.info("portalParam.homeAddress: " + portalParam.homeAddress);
                portalParam.homeCity = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeCity");
                logger.info("portalParam.homeCity: " + portalParam.homeCity);
                portalParam.homeState = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeState");
                logger.info("portalParam.homeState: " + portalParam.homeState);
                portalParam.homeZipcode = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeZipcode");
                logger.info("portalParam.homeZipcode: " + portalParam.homeZipcode);
                portalParam.homePhone = portalFuncUtils.getTestData(FuncMod, Test_ID, "homePhone");
                logger.info("portalParam.homePhone: " + portalParam.homePhone);
                portalParam.homeMobile = portalFuncUtils.getTestData(FuncMod, Test_ID, "homeMobile");
                logger.info("portalParam.homeMobile: " + portalParam.homeMobile);
                portalParam.ownership = portalFuncUtils.getTestData(FuncMod, Test_ID, "ownership");
                logger.info("portalParam.ownership: " + portalParam.ownership);
                portalParam.ssn = portalFuncUtils.getTestData(FuncMod, Test_ID, "ssn");
                logger.info("portalParam.ssn: " + portalParam.ssn);
                portalParam.doMonth = portalFuncUtils.getTestData(FuncMod, Test_ID, "doMonth");
                logger.info("portalParam.doMonth: " + portalParam.doMonth);
                portalParam.doDay = portalFuncUtils.getTestData(FuncMod, Test_ID, "doDay");
                logger.info("portalParam.doDay: " + portalParam.doDay);
                portalParam.doYear = portalFuncUtils.getTestData(FuncMod, Test_ID, "doYear");
                logger.info("portalParam.doYear: " + portalParam.doYear);


            }
            logger.info("-----------------------------------------------");
        } catch (Exception e) {
            logger.info("    *****    Field not present in \"" + FuncMod + "\" worksheet.");
        }
    }

    public void writeToReport(String funcMod,String sTestID, String result) throws IOException {

        String base64String=null;
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", sTestID);
            obj.put("status", result);
            if((funcMod.contains("Portal") && (result.equalsIgnoreCase("Failed"))))
            {
                //File file = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\images\\"+sTestID+".png");
                File file = new File("./"+portalParam.custom_report_location+sTestID+".png");
                System.out.println(file.exists());
                byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
                base64String = new String(encoded, StandardCharsets.US_ASCII);
                obj.put("screenshot", "data:image/png;base64,"+base64String);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            File file = new File(PortalParam.custom_report_location + "test.json");
            ifFileExist = file.createNewFile();
            if (ifFileExist) {
                FileWriter f = new FileWriter(file);
                f.write("cases : [" + obj.toString());
                f.flush();
                f.close();
            } else {
                FileWriter f = new FileWriter(file, true);
                f.write("," + obj.toString());
                f.flush();
                f.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod(groups = {"PortalTests","verifyHappyFlowWithManualUpload","Sprint1"})
    @Parameters("browser")
    public void setUp() throws Exception {
        //String ibrowser = GlobalVariables.browser;

        browser_list browser = browser_list.valueOf(portalPropertiesReader.getBrowser());
        String nodeUrl;
        switch (browser) {

            case IE:

                // IEDriverServer available @
                // http://code.google.com/p/selenium/downloads/list
                System.setProperty("webdriver.ie.driver", "./webdriver/IEDriverServer.exe");
                DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                driver = new InternetExplorerDriver(capabilities);
                break;

            case Chrome:
                // chromedriver available @http://code.google.com/p/chromedriver/downloads/list
                System.setProperty("webdriver.chrome.driver", "./webdriver/chromedriver.exe");
                System.setProperty("java.awt.headless", "false");
                driver = new ChromeDriver();

                break;

            case FF:
                System.setProperty("webdriver.gecko.driver" ,"./webdriver/geckodriver.exe");
                driver = new FirefoxDriver();
        }

        Thread.sleep(profile_delay);
        driver.manage().timeouts().implicitlyWait(ShortSleep, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        logger.info("Loading url :" + portalPropertiesReader.getBorrowerUrl());
        driver.get(portalPropertiesReader.getBorrowerUrl());

    }



    @AfterMethod(groups = {"PortalTests","verifyHappyFlowWithManualUpload","Sprint1"})
    public void quit()
    {
        driver.quit();
    }
    /**
     * Waits for the expected condition to complete
     *
     * @param e the expected condition to wait until it becomes true
     * @param timeout  how long to wait for the expected condition to turn true
     * @return true if the expected condition returned true and false if not
     */
    public boolean waitForCondition(ExpectedCondition<Boolean> e, int timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        boolean returnValue = true;
        try {
            w.until(e);
        } catch (TimeoutException te) {
            logger.error("Failed to find the expected condition", te);
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Wait for the element to disappear from the screen
     *
     * @param finder the element to wait to disappear
     * @param timeout  how long to wait for the item to disappear (in seconds)
     * @return true if the element disappeared and false if it did not
     */
    protected boolean waitForElementRemoval(final By finder, int timeout) {
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() == 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Wait for the element to appear on the screen
     * @param finder  the element to wait to appear
     * @param timeout  how long to wait for the item to appear (in seconds)
     * @return true if the element appeared and false if it did not
     */
    protected boolean waitForElement(final By finder, int timeout) {
        //logger.info("Waiting for element to load");
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() > 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Sleeps for the desired amount of time
     *
     * @param time
     *            the amount of time to sleep in ms
     */
    protected void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ie) {
        }
    }

    public void waitForPageLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();
        wait.until(pageLoadCondition);
    }

    @BeforeSuite(alwaysRun=true)
    public void  startCaseReport() throws Exception {
        deletePreviousScreenshots();
        logger.info("Initializing reporting framework");
        logger.info("---------------------------------");
        auto_start = System.currentTimeMillis();

        logger.info("portalParam.custom_report_location:"+portalParam.custom_report_location);

        FileUtils.copyFile(new File(portalParam.custom_report_location + "header.txt"),new File(portalParam.custom_report_location + "header_template.txt"));
        FileUtils.copyFile(new File(portalParam.custom_report_location + "trailor.txt"),new File(portalParam.custom_report_location + "trailor_template.txt"));

        logger.info("Calling Before Suite for generating reporting files.");

        casefile = new File(portalParam.custom_report_location+ "test.json");
        portal_scenfile = new File(portalParam.custom_report_location + "portal_scenario.json");
        portal_feafile = new File(portalParam.custom_report_location + "portal_feature.json");
        api_scenfile = new File(portalParam.custom_report_location + "api_scenario.json");
        api_feafile = new File(portalParam.custom_report_location + "api_feature.json");

        feafile = new File(portalParam.custom_report_location + "feature.json");

        casefile.createNewFile();
        FileWriter casef = new FileWriter(casefile);
        portal_scenfile.createNewFile();
        FileWriter portal_scenf = new FileWriter(portal_scenfile);
        portal_feafile.createNewFile();
        FileWriter portal_feaf = new FileWriter(portal_feafile);
        api_scenfile.createNewFile();
        FileWriter api_scenf = new FileWriter(api_scenfile);
        api_feafile.createNewFile();
        FileWriter api_feaf = new FileWriter(api_feafile);

        feafile.createNewFile();
        FileWriter featurefw = new FileWriter(feafile);
        portal_feaf.write("features: [");
        portal_scenf.write("scenarios: [");
        casef.write("cases : [");
        casef.flush();
        casef.close();
        portal_scenf.flush();
        portal_scenf.close();
        portal_feaf.flush();
        portal_feaf.close();
        featurefw.write("features: [");
        api_feaf.write("features: [");
        api_scenf.write("scenarios: [");
        api_scenf.flush();
        api_scenf.close();
        api_feaf.flush();
        api_feaf.close();
        featurefw.flush();
        featurefw.close();



    }

    private void deletePreviousScreenshots() {
        File folder = new File(PortalParam.upload_file_location);
        File fList[] = folder.listFiles();

        for (File f : fList) {
            if (f.getName().endsWith(".png")) {
                f.delete();
            }}
    }

    public void generateReport(String className, String description,String funcModule) throws IOException, JSONException {

        String classStatus = "";
        File scenFile = null;

        if(funcModule.equalsIgnoreCase("ProjectTest_Api"))
        {
            scenFile = api_scenfile;
        }
        else if(funcModule.equalsIgnoreCase("FC360"))
        {
            scenFile = portal_scenfile;
        }

        logger.info("scenFile:"+scenFile);
        logger.info("casefile:"+casefile);
        BufferedReader scenreader = new BufferedReader(new FileReader(scenFile));
        BufferedReader reader = new BufferedReader(new FileReader(casefile));
        String line = "", oldtext = "";
        while ((line = reader.readLine()) != null) {
            oldtext += line + "\r";
        }

        reader.close();
        String newtext = oldtext.replaceAll("\\[\\,", "[");
        newtext = newtext.replaceAll("\n", "");
        newtext = newtext + "]";
        FileWriter f = new FileWriter(casefile, true);
        f.write(newtext);
        f.flush();
        f.close();

        JSONObject jobj = new JSONObject();
        jobj.put("name", className);
        jobj.put("description", description);
        jobj.put("tags", "");
        if (newtext.contains("Fail")) {
            classStatus = "Fail";
        } else {
            classStatus = "Pass";
        }
        jobj.put("status", classStatus);
        jobj.put("automated", "true");

        line = "";
        oldtext = "";
        while ((line = scenreader.readLine()) != null) {
            oldtext += line + "\r";
        }
        FileWriter scenf = new FileWriter(scenFile, true);
        if (!oldtext.contains("scenarios: [")) {
            scenf.write("scenarios: [\n");
        }
        scenf.write(jobj.toString().replaceAll("\\}", "") + "," + newtext + "\n},");
        scenf.flush();
        scenf.close();
        //casefile.delete();
    }

    @AfterSuite (alwaysRun = true)
    public void prepareFinalReport() throws IOException, JSONException,java.lang.Exception {
        auto_finish = System.currentTimeMillis();

        String testEnvtString =  setTestEnvtDetails(auto_start,auto_finish,Execution_start_time);
        testEnvtString = testEnvtString.toString().replaceAll("\"values\"","values").replaceAll("\\[\\{","\\[").replaceAll("\\}\\]","\\]").replaceAll("\":\"", ":");

        UpdateEnvtParams(testEnvtString, portalParam.custom_report_location+ "header_template.txt");
        System.out.println(testEnvtString);
        logger.info("Calling After Suite for preparing html report.");

        if((portal_feafile.isFile()) && (portal_scenfile.isFile()))
        {
            BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
            BufferedReader portal_scenreader = new BufferedReader(new FileReader(portal_scenfile));

            String line = "", oldtext = "";
            while((line = portal_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","Portal");
            fobj.put("description","CA Portal Test Cases");

            FileWriter feaf = new FileWriter(portal_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = portal_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(portal_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }

        /*if((api_feafile.isFile()) && (api_scenfile.isFile()))
        {
            BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));
            BufferedReader api_scenreader = new BufferedReader(new FileReader(api_scenfile));

            String line = "", oldtext = "";
            while((line = api_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","ProjectTest_Api");
            fobj.put("description","ProjectTest API Test Cases");

            FileWriter feaf = new FileWriter(api_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = api_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(api_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }
*/
        //casefile.delete();

        BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
        BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));

        String oldtext="",line="";
        while((line = api_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }
        while((line = portal_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        portal_feareader.close();
        api_feareader.close();
        String[] parts = oldtext.split("features: \\[", 2);
        oldtext = parts[0] + parts[1].replaceAll("features: \\[", "");

        FileWriter finalfeaf = new FileWriter(feafile,true);
        finalfeaf.write(oldtext);
        finalfeaf.flush();
        finalfeaf.close();

        File htmlReportFile = new File(portalParam.custom_report_location + "FC360_Automation.html");
        File reportHeader = new File(portalParam.custom_report_location + "header_template.txt");
        File reportFeature = new File(portalParam.custom_report_location + "feature.json");
        File reportTrailer = new File(portalParam.custom_report_location + "trailor_template.txt");
        oldtext = "";line="";
        BufferedReader reportReader = new BufferedReader(new FileReader(reportHeader));
        while((line = reportReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader featureReader = new BufferedReader(new FileReader(reportFeature));
        while((line = featureReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader trailerReader = new BufferedReader(new FileReader(reportTrailer));
        while((line = trailerReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        htmlReportFile.createNewFile();

        FileWriter htmlf = new FileWriter(htmlReportFile);
        htmlf.write(oldtext);
        htmlf.flush();
        htmlf.close();
        UpdateEnvtParams("ENVT_DETAILS", portalParam.custom_report_location + "header_template.txt");
    }

    public void UpdateEnvtParams(String testEnvtString,String fileName){
        try
        {
            String newtext=null;
            File file = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "", oldtext = "";
            while((line = reader.readLine()) != null)
            {
                oldtext += line + "";
            }
            reader.close();
            // replace a word in a file
            //String newtext = oldtext.replaceAll("drink", "Love");

            //To replace a line in a file
            if(!testEnvtString.equalsIgnoreCase("ENVT_DETAILS")) {
                newtext = oldtext.replaceAll("ENVT_DETAILS", testEnvtString);
            }
            else {
                newtext = oldtext.replaceAll(testEnvtString, "ENVT_DETAILS");
            }
            FileWriter writer = new FileWriter(fileName);
            writer.write(newtext);writer.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

    }

    public String setTestEnvtDetails(long startTime,long endTime,String execution_start_time) throws java.lang.Exception{
        JSONObject jo = new JSONObject();
        jo.put("ProjectName","CA-Automation");
        jo.put("Operating System",System.getProperty("os.name").toLowerCase());
        jo.put("Testing environment",PortalFuncUtils.removeSpecialChar(System.getProperty("envParam")).replaceAll("\\\\",""));
        jo.put("Date",execution_start_time);
        jo.put("Total Execution Time",toHHMMDD(endTime - startTime));

        JSONArray ja = new JSONArray();
        ja.put(jo);

        JSONObject mainObj = new JSONObject();
        mainObj.put("values", ja);
        return mainObj.toString();
    }

    public String toHHMMDD(long time){

        String hms = String.format("%02d"+ " hrs" +":%02d"+ " mins" + ":%02d"+ " sec", TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        return hms;
    }

    public boolean isPopedUp() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }   // catch
    }

    public String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String str = matcher.replaceAll("");
        return str;
    }

    /*public DocusignPage readGmailAndDocusign(WebDriver driver, String mainTab) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.gmailId,portalParam.gmailPassword);
        if(driver.getCurrentUrl().equalsIgnoreCase("https://myaccount.google.com/?pli=1"))
        {
            driver.get("https://mail.google.com/mail/u/0/#inbox");
        }
        /*driver.findElement(By.xpath("/*//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();
        List<WebElement> unreademail = driver.findElements(By.xpath("//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Sigma via DocuSign";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")));
                    driver.findElement(By.xpath(".//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.xpath("//a[contains(@href,'https://demo.docusign.net')]")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new DocusignPage(driver);
    }*/

    private void loginToGmail(String id, String password) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        boolean newGmail = driver.findElements( By.xpath("//div[contains(text(),'More options')]") ).size() != 0;
        if(newGmail)
        {
            driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(id);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Forgot password?')]"))).isDisplayed();
            driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
        }
        else {
            driver.findElement(By.id("Email")).sendKeys(id);
            driver.findElement(By.id("next")).click();
            driver.findElement(By.id("Passwd")).sendKeys(password);
            driver.findElement(By.id("signIn")).click();

        }
    }

}
